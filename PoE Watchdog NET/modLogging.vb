﻿Module modLogging

    ''' <summary>
    ''' This is filled the first time an entry is written to the logfile, and is used when right click on the debug button
    ''' to show the log file in te default browser. Also Shift Leftclick on the main form shows the log in a own browser
    ''' </summary>
    Public LOGFILE As String
    Public RecycleLogfiles As Boolean
    Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
    (ByVal hWnd As Long, _
    ByVal lpOperation As String, _
    ByVal lpFile As String, _
    ByVal lpParameters As String, _
    ByVal lpDirectory As String, _
    ByVal nShowCmd As Long) As Long

    Sub ShowLogFile(VanWelkeForm As Form)
        Dim iRet As Long
        iRet = ShellExecute(VanWelkeForm.Handle, vbNullString, LOGFILE & Format(Now, "dd") & ".html", vbNullString, "C:\", 1)
        Debug.Print("ShellExecute return value = " & iRet)
    End Sub
    ''' <summary>
    ''' Log schrijft (in HTML) log gegevens weg naar een bestand. LOGFILE wordt in de init van de applicatie bepaald, zonder extentie. Zorg ook daar dat de gebruiker schrijf rechten daartoe heeft
    ''' </summary>
    ''' <param name="Wat">De text in de logfile</param>
    ''' <param name="Opmaak">Opmaak is een kleine letter b(old), i(talic). Geen letter  = normaal</param>
    ''' <param name="Color"> Kleur is een klein letter r(ood), b(lauw), g(roen). Geen letter = zwart</param>
    ''' <param name="DateAndTime">Lege string wordt de Now tijd neergezet, een min teken "-" wordt geen tijd/datum info neergezet. Elke andere string wordt letterlijk overgenomen in de logfile gezet.</param>
    ''' <param name="NoCRLF">Als deze true is dan wordt geen enter aan het einde van deze regel gezet. Handig om in meerdere calls naar Log() op een regel te krijgen.</param>
    ''' <param name="LogFileBaseName">is de eerste deel van het bestand, wordt hier aangevuld met dag/datum tenzij dit een index.htm is.</param>
    ''' <param name="RecycleLog">als die true is, zal oude logs wissen</param>
    ''' <remarks></remarks>
    Sub HTMLog _
            (ByVal Wat As String, _
            Opmaak As FontStyle, _
            Color As Color, _
            Optional DateAndTime As String = "", _
            Optional NoCRLF As Boolean = False, _
            Optional LogFileBaseName As String = "", _
            Optional RecycleLog As Boolean = False)


        Dim HtmlLogFile$
        On Error GoTo Fout
        '1 maand aan logfiles worden bewaard, logfiles heten dan ook LOGFILE_dd.htm
        If InStr(1, LogFileBaseName, "index.htm") = 0 Then  'Als ik een index maak, dan is dit een log voor webserver

            HtmlLogFile = LogFileBaseName & "_" & Format(Now, "dd")
        Else
            ' Mag ik natuurlijk de naam niet veranderen
            HtmlLogFile = LOGFILE
        End If

        Dim FN As Integer
        FN = FreeFile()

        'De aller eerste keer zal dit bestand niet bestaan, dus aan maken en Verdana aan zetten
        If Dir(HtmlLogFile & ".htm") = "" Then
            FileOpen(FN, HtmlLogFile & ".htm", OpenMode.Append)
            Print(FN, "<BODY bgColor=#ffffff><FONT face=Verdana size=2>")
            FileClose(FN)
        End If

        FileOpen(FN, HtmlLogFile & ".htm", OpenMode.Append)

        If Wat <> "" Then
            If DateAndTime = "" Then
                Print(FN, "<FONT size=1>" & Format(Now, "dd MMM HH:mm:ss") & "&nbsp</FONT>")
            ElseIf DateAndTime = "-" Then
                'Do niks - geen datum tijd
            Else
                Print(FN, "<FONT size=1>" & DateAndTime & "&nbsp</FONT>")
            End If
        End If


        If Opmaak = FontStyle.Bold Then Print(FN, "<STRONG>")
        If Opmaak = FontStyle.Italic Then Print(FN, "<EM>")

        Dim HTMColor As String = System.Drawing.ColorTranslator.ToHtml(Color)

        Print(FN, "<FONT color=" & HTMColor & ">")

        'If InStr(1, Color, "r", vbTextCompare) <> 0 Then Print(FN, "<FONT color=#FF0000>")
        'If InStr(1, Color, "g", vbTextCompare) <> 0 Then Print(FN, "<FONT color=#00BB00>")
        'If InStr(1, Color, "b", vbTextCompare) <> 0 Then Print(FN, "<FONT color=#0000FF>")

        'zorg dat enters in een string (zoals die vanaf de poe switches komen) ook in de HTM goed zijn
        Wat = Replace(Wat, vbLf, "<br>")

        If Wat = "LINE" Then
            Print(FN, "<HR>")
        Else
            If NoCRLF Then
                Print(FN, Wat)
            Else
                Print(FN, Wat & "<br>")
            End If
        End If
        
        If Opmaak = FontStyle.Bold Then Print(FN, "</STRONG>")
        If Opmaak = FontStyle.Italic Then Print(FN, "</EM>")
        Print(FN, "</FONT>")

        'If InStr(1, Color, "r", vbTextCompare) <> 0 Then Print(FN, "</FONT>")
        'If InStr(1, Color, "g", vbTextCompare) <> 0 Then Print(FN, "</FONT>")
        'If InStr(1, Color, "b", vbTextCompare) <> 0 Then Print(FN, "</FONT>")
        FileClose(FN)
        Exit Sub
Fout:
        Bug(Err.Description & " tijdens het wegschrijven van log data in " & HtmlLogFile, , True, True)

    End Sub

End Module
