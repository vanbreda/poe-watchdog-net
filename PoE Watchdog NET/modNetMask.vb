﻿Imports System.Net

Public Module modNetMask

    Public Structure structIPAddressRange
        Dim startIPAddress As String
        Dim endIPAddress As String
        Dim FromIPInt As IPAddress
        Dim ToIPInt As IPAddress
    End Structure

    Public MSK(32) As String
    ''' <summary>
    ''' This inits the /mask in the dotted MSK() array, making "/24" into 255.255.255.0 for example 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InitMSK()
        MSK(32) = "255.255.255.255"
        MSK(31) = "255.255.255.254"
        MSK(30) = "255.255.255.252"
        MSK(29) = "255.255.255.248"
        MSK(28) = "255.255.255.240"
        MSK(27) = "255.255.255.224"
        MSK(26) = "255.255.255.192"
        MSK(25) = "255.255.255.128"
        MSK(24) = "255.255.255.0"
        MSK(23) = "255.255.254.0"
        MSK(22) = "255.255.252.0"
        MSK(21) = "255.255.248.0"
        MSK(20) = "255.255.240.0"
        MSK(19) = "255.255.224.0"
        MSK(18) = "255.255.192.0"
        MSK(17) = "255.255.128.0"
        MSK(16) = "255.255.0.0"
        MSK(15) = "255.254.0.0"
        MSK(14) = "255.252.0.0"
        MSK(13) = "255.248.0.0"
        MSK(12) = "255.240.0.0"
        MSK(11) = "255.224.0.0"
        MSK(10) = "255.192.0.0"
        MSK(9) = "255.128.0.0"
        MSK(8) = "255.0.0.0"
        MSK(7) = "254.0.0.0"
        MSK(6) = "252.0.0.0"
        MSK(5) = "248.0.0.0"
        MSK(4) = "240.0.0.0"
        MSK(3) = "224.0.0.0"
        MSK(2) = "192.0.0.0"
        MSK(1) = "128.0.0.0"
        MSK(0) = "0.0.0.0"
    End Sub

    Sub Test()
        For i% = 1 To 32
            Dim S = GetIPRange("10.0.0.0/" & i)
            Debug.Print("(" & i & ") " & S.startIPAddress & " - " & S.endIPAddress)
        Next

    End Sub



    Public Function GetIPRange(cidr As String) As structIPAddressRange
        Dim parts() As String = cidr.Split("/"c)
        Dim baseIP As String = parts(0)
        Dim subnetMaskLength As Integer = Integer.Parse(parts(1))



        Dim baseIPBytes() As Byte = IPAddress.Parse(baseIP).GetAddressBytes()
        Dim startIPBytes(3) As Byte
        Array.Copy(baseIPBytes, startIPBytes, 4)

        Dim subnetMaskBytes() As Byte = GetSubnetMask(subnetMaskLength)

        Dim endIPBytes(3) As Byte
        For i As Integer = 0 To 3
            startIPBytes(i) = baseIPBytes(i) And subnetMaskBytes(i)
            endIPBytes(i) = baseIPBytes(i) Or (subnetMaskBytes(i) Xor 255)
        Next


        Dim startIP As New IPAddress(startIPBytes)
        Dim endIP As New IPAddress(endIPBytes)
        Dim Range As structIPAddressRange


        Range.startIPAddress = startIP.ToString()
        Range.endIPAddress = endIP.ToString
        Range.FromIPInt = startIP
        Range.ToIPInt = endIP

        Return Range
    End Function

    Function GetSubnetMask(subnetMaskLength As Integer) As Byte()
        Dim subnetMask(3) As Byte
        For i As Integer = 0 To 3
            If subnetMaskLength >= 8 Then
                subnetMask(i) = 255
                subnetMaskLength -= 8
            ElseIf subnetMaskLength > 0 Then
                subnetMask(i) = Convert.ToByte(255 << (8 - subnetMaskLength) And 255)
                subnetMaskLength = 0
            End If
        Next
        Return subnetMask
    End Function

    Function IPAddressToUInt32(ip As IPAddress) As UInt32
        Dim bytes() As Byte = ip.GetAddressBytes()
        Array.Reverse(bytes) ' Convert to big-endian
        Return BitConverter.ToUInt32(bytes, 0)
    End Function

    Function UInt32ToIPAddress(ip As UInt32) As IPAddress
        Dim bytes() As Byte = BitConverter.GetBytes(ip)
        Array.Reverse(bytes) ' Convert to little-endian
        Return New IPAddress(bytes)
    End Function

End Module
