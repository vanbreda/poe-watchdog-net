﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPoE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPoE))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tbUnits = New System.Windows.Forms.TabPage()
        Me.chkAutoVervangUnits = New System.Windows.Forms.CheckBox()
        Me.cmdDBResync = New System.Windows.Forms.Button()
        Me.cmdToevoegen = New System.Windows.Forms.Button()
        Me.cmdResetPoE = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdDisableRestartTmp = New System.Windows.Forms.Button()
        Me.lstDisableAutorestartTime = New System.Windows.Forms.ListBox()
        Me.chkEnableRestart = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbActiveUnits = New System.Windows.Forms.TabPage()
        Me.lblFoundAlbireos = New System.Windows.Forms.Label()
        Me.lstFoundAlbireos = New System.Windows.Forms.ListBox()
        Me.lblNumberOfAIPs = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lstAIPDood = New System.Windows.Forms.ListBox()
        Me.lstAIP = New System.Windows.Forms.ListBox()
        Me.lstKomtWelBinnen = New System.Windows.Forms.ListBox()
        Me.tbInstellingen = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cmdAlbireoZoekHelp = New System.Windows.Forms.Button()
        Me.cmdINEX500iniSturen = New System.Windows.Forms.Button()
        Me.cmdFindAIP = New System.Windows.Forms.Button()
        Me.txtfindAlbireoTo = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFindAlbireoFrom = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblSelectedNetworkAdapter = New System.Windows.Forms.Label()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lstPoESwitches = New System.Windows.Forms.ListBox()
        Me.mnuSwitches = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ResetAllePoortenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetPoortToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TelnetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuttyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToevoegenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerwijderenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Command14 = New System.Windows.Forms.Button()
        Me.cmdDoTheMagic = New System.Windows.Forms.Button()
        Me.cmdZoekPoE = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtResetTime = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtTotaalAantalVideoServers = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtSNMPCommunity = New System.Windows.Forms.TextBox()
        Me.txtSwitchInlogWachtwoord2 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAlbireoVLAN = New System.Windows.Forms.TextBox()
        Me.txtSwitchNetwerk = New System.Windows.Forms.TextBox()
        Me.txtSwitchInlogWachtwoord1 = New System.Windows.Forms.TextBox()
        Me.txtSwitchInlogNaam = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtWiFiPassword = New System.Windows.Forms.TextBox()
        Me.txtWiFiSSID = New System.Windows.Forms.TextBox()
        Me.chkMACuitDatabase = New System.Windows.Forms.CheckBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtNegeerAIPAdressen = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtLogPath = New System.Windows.Forms.TextBox()
        Me.txtEVALogIPAdres = New System.Windows.Forms.TextBox()
        Me.txtTFTPIPAdres = New System.Windows.Forms.TextBox()
        Me.txtDatabase = New System.Windows.Forms.TextBox()
        Me.lstAdapters = New System.Windows.Forms.ListBox()
        Me.tbDebug = New System.Windows.Forms.TabPage()
        Me.chkKomtWelBinnen = New System.Windows.Forms.CheckBox()
        Me.txtUDPNaarDeZaak = New System.Windows.Forms.TextBox()
        Me.chkDetail = New System.Windows.Forms.CheckBox()
        Me.chkDebugNaarDeZaak = New System.Windows.Forms.CheckBox()
        Me.tbOud = New System.Windows.Forms.TabPage()
        Me.txtSwitchNetworkMask = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.List5 = New System.Windows.Forms.ListBox()
        Me.chkAlbireoIPOnly = New System.Windows.Forms.CheckBox()
        Me.Text9 = New System.Windows.Forms.TextBox()
        Me.Text8 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Command5 = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lstEVAVideoServer = New System.Windows.Forms.ListBox()
        Me.lstBellatrix = New System.Windows.Forms.ListBox()
        Me.lstActiveVideoServers = New System.Windows.Forms.ListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lstActiveATVS = New System.Windows.Forms.ListBox()
        Me.mnuAIP = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuToevoegen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAanpassenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVerwijderen2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFactoryReset = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBooloaderUpdate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuResetPoEPoort = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPoortUit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuWiFiInstellingenSturenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuINEXIniSturenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScriptNaarAlbireoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TelnetToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuttyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblAantalFouten = New System.Windows.Forms.Label()
        Me.tmrgTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.tmrAIPlstADD = New System.Windows.Forms.Timer(Me.components)
        Me.mnuTray = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShutdown = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblStatus2 = New System.Windows.Forms.Label()
        Me.tmrDelayBinding = New System.Windows.Forms.Timer(Me.components)
        Me.tmrCheckUnits = New System.Windows.Forms.Timer(Me.components)
        Me.tmrDelayCheckUnits = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tmrStatusUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.tmrEnableAutorestart = New System.Windows.Forms.Timer(Me.components)
        Me.tmrAIPDood = New System.Windows.Forms.Timer(Me.components)
        Me.btnTab0 = New System.Windows.Forms.Button()
        Me.btnTab1 = New System.Windows.Forms.Button()
        Me.btnTab2 = New System.Windows.Forms.Button()
        Me.btnTab3 = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.tbUnits.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.tbActiveUnits.SuspendLayout()
        Me.tbInstellingen.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.mnuSwitches.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tbDebug.SuspendLayout()
        Me.tbOud.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.mnuAIP.SuspendLayout()
        Me.mnuTray.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(12, 547)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 29)
        Me.Button1.TabIndex = 1
        Me.Button1.TabStop = False
        Me.Button1.Text = "Stop"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tbUnits)
        Me.TabControl1.Controls.Add(Me.tbActiveUnits)
        Me.TabControl1.Controls.Add(Me.tbInstellingen)
        Me.TabControl1.Controls.Add(Me.tbDebug)
        Me.TabControl1.Controls.Add(Me.tbOud)
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.ItemSize = New System.Drawing.Size(200, 20)
        Me.TabControl1.Location = New System.Drawing.Point(136, 3)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(989, 621)
        Me.TabControl1.TabIndex = 3
        Me.TabControl1.TabStop = False
        '
        'tbUnits
        '
        Me.tbUnits.Controls.Add(Me.chkAutoVervangUnits)
        Me.tbUnits.Controls.Add(Me.cmdDBResync)
        Me.tbUnits.Controls.Add(Me.cmdToevoegen)
        Me.tbUnits.Controls.Add(Me.cmdResetPoE)
        Me.tbUnits.Controls.Add(Me.GroupBox3)
        Me.tbUnits.Controls.Add(Me.Label6)
        Me.tbUnits.Location = New System.Drawing.Point(4, 24)
        Me.tbUnits.Name = "tbUnits"
        Me.tbUnits.Size = New System.Drawing.Size(981, 593)
        Me.tbUnits.TabIndex = 2
        Me.tbUnits.Text = "Units"
        Me.tbUnits.UseVisualStyleBackColor = True
        '
        'chkAutoVervangUnits
        '
        Me.chkAutoVervangUnits.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkAutoVervangUnits.AutoSize = True
        Me.chkAutoVervangUnits.Location = New System.Drawing.Point(1197, 12)
        Me.chkAutoVervangUnits.Name = "chkAutoVervangUnits"
        Me.chkAutoVervangUnits.Size = New System.Drawing.Size(143, 22)
        Me.chkAutoVervangUnits.TabIndex = 7
        Me.chkAutoVervangUnits.Text = "Auto vervang units"
        Me.chkAutoVervangUnits.UseVisualStyleBackColor = True
        Me.chkAutoVervangUnits.Visible = False
        '
        'cmdDBResync
        '
        Me.cmdDBResync.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDBResync.Location = New System.Drawing.Point(1773, 117)
        Me.cmdDBResync.Name = "cmdDBResync"
        Me.cmdDBResync.Size = New System.Drawing.Size(91, 27)
        Me.cmdDBResync.TabIndex = 6
        Me.cmdDBResync.Text = "DB. ReSync"
        Me.cmdDBResync.UseVisualStyleBackColor = True
        '
        'cmdToevoegen
        '
        Me.cmdToevoegen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdToevoegen.Location = New System.Drawing.Point(1773, 41)
        Me.cmdToevoegen.Name = "cmdToevoegen"
        Me.cmdToevoegen.Size = New System.Drawing.Size(91, 27)
        Me.cmdToevoegen.TabIndex = 6
        Me.cmdToevoegen.Text = "Toevoegen"
        Me.cmdToevoegen.UseVisualStyleBackColor = True
        '
        'cmdResetPoE
        '
        Me.cmdResetPoE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdResetPoE.Location = New System.Drawing.Point(1773, 84)
        Me.cmdResetPoE.Name = "cmdResetPoE"
        Me.cmdResetPoE.Size = New System.Drawing.Size(91, 27)
        Me.cmdResetPoE.TabIndex = 6
        Me.cmdResetPoE.Text = "Reset PoE"
        Me.cmdResetPoE.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.cmdDisableRestartTmp)
        Me.GroupBox3.Controls.Add(Me.lstDisableAutorestartTime)
        Me.GroupBox3.Controls.Add(Me.chkEnableRestart)
        Me.GroupBox3.Location = New System.Drawing.Point(447, -4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(531, 39)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        '
        'cmdDisableRestartTmp
        '
        Me.cmdDisableRestartTmp.Location = New System.Drawing.Point(206, 11)
        Me.cmdDisableRestartTmp.Name = "cmdDisableRestartTmp"
        Me.cmdDisableRestartTmp.Size = New System.Drawing.Size(203, 25)
        Me.cmdDisableRestartTmp.TabIndex = 4
        Me.cmdDisableRestartTmp.Text = "Uitzetten voor:"
        Me.cmdDisableRestartTmp.UseVisualStyleBackColor = True
        '
        'lstDisableAutorestartTime
        '
        Me.lstDisableAutorestartTime.FormattingEnabled = True
        Me.lstDisableAutorestartTime.ItemHeight = 18
        Me.lstDisableAutorestartTime.Items.AddRange(New Object() {"30 min", "1 uur", "1 uur 30 min", "2 uur"})
        Me.lstDisableAutorestartTime.Location = New System.Drawing.Point(415, 13)
        Me.lstDisableAutorestartTime.Name = "lstDisableAutorestartTime"
        Me.lstDisableAutorestartTime.Size = New System.Drawing.Size(110, 22)
        Me.lstDisableAutorestartTime.TabIndex = 3
        '
        'chkEnableRestart
        '
        Me.chkEnableRestart.AutoSize = True
        Me.chkEnableRestart.Location = New System.Drawing.Point(6, 14)
        Me.chkEnableRestart.Name = "chkEnableRestart"
        Me.chkEnableRestart.Size = New System.Drawing.Size(194, 22)
        Me.chkEnableRestart.TabIndex = 1
        Me.chkEnableRestart.Text = "Herstart units bij vastlopen"
        Me.chkEnableRestart.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 18)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Units:"
        '
        'tbActiveUnits
        '
        Me.tbActiveUnits.Controls.Add(Me.lblFoundAlbireos)
        Me.tbActiveUnits.Controls.Add(Me.lstFoundAlbireos)
        Me.tbActiveUnits.Controls.Add(Me.lblNumberOfAIPs)
        Me.tbActiveUnits.Controls.Add(Me.Label17)
        Me.tbActiveUnits.Controls.Add(Me.Label16)
        Me.tbActiveUnits.Controls.Add(Me.lstAIPDood)
        Me.tbActiveUnits.Controls.Add(Me.lstAIP)
        Me.tbActiveUnits.Controls.Add(Me.lstKomtWelBinnen)
        Me.tbActiveUnits.Location = New System.Drawing.Point(4, 24)
        Me.tbActiveUnits.Name = "tbActiveUnits"
        Me.tbActiveUnits.Size = New System.Drawing.Size(981, 593)
        Me.tbActiveUnits.TabIndex = 3
        Me.tbActiveUnits.Text = "Active Units"
        Me.tbActiveUnits.UseVisualStyleBackColor = True
        '
        'lblFoundAlbireos
        '
        Me.lblFoundAlbireos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFoundAlbireos.AutoSize = True
        Me.lblFoundAlbireos.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFoundAlbireos.Location = New System.Drawing.Point(691, 5)
        Me.lblFoundAlbireos.Name = "lblFoundAlbireos"
        Me.lblFoundAlbireos.Size = New System.Drawing.Size(129, 18)
        Me.lblFoundAlbireos.TabIndex = 3
        Me.lblFoundAlbireos.Text = "Gevonden Albireo's"
        '
        'lstFoundAlbireos
        '
        Me.lstFoundAlbireos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstFoundAlbireos.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFoundAlbireos.FormattingEnabled = True
        Me.lstFoundAlbireos.ItemHeight = 16
        Me.lstFoundAlbireos.Location = New System.Drawing.Point(691, 23)
        Me.lstFoundAlbireos.Name = "lstFoundAlbireos"
        Me.lstFoundAlbireos.Size = New System.Drawing.Size(287, 564)
        Me.lstFoundAlbireos.Sorted = True
        Me.lstFoundAlbireos.TabIndex = 2
        '
        'lblNumberOfAIPs
        '
        Me.lblNumberOfAIPs.AutoSize = True
        Me.lblNumberOfAIPs.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumberOfAIPs.Location = New System.Drawing.Point(0, 5)
        Me.lblNumberOfAIPs.Name = "lblNumberOfAIPs"
        Me.lblNumberOfAIPs.Size = New System.Drawing.Size(104, 18)
        Me.lblNumberOfAIPs.TabIndex = 1
        Me.lblNumberOfAIPs.Text = "Active AIP units"
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(489, 5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(112, 18)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Komt wel binnen"
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(486, 372)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(199, 18)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Ooit actief geweest AIP units: 0"
        '
        'lstAIPDood
        '
        Me.lstAIPDood.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstAIPDood.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAIPDood.FormattingEnabled = True
        Me.lstAIPDood.ItemHeight = 16
        Me.lstAIPDood.Location = New System.Drawing.Point(489, 391)
        Me.lstAIPDood.Name = "lstAIPDood"
        Me.lstAIPDood.Size = New System.Drawing.Size(196, 196)
        Me.lstAIPDood.TabIndex = 0
        '
        'lstAIP
        '
        Me.lstAIP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstAIP.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAIP.FormattingEnabled = True
        Me.lstAIP.ItemHeight = 16
        Me.lstAIP.Location = New System.Drawing.Point(3, 23)
        Me.lstAIP.Name = "lstAIP"
        Me.lstAIP.Size = New System.Drawing.Size(480, 564)
        Me.lstAIP.Sorted = True
        Me.lstAIP.TabIndex = 0
        '
        'lstKomtWelBinnen
        '
        Me.lstKomtWelBinnen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstKomtWelBinnen.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstKomtWelBinnen.FormattingEnabled = True
        Me.lstKomtWelBinnen.ItemHeight = 16
        Me.lstKomtWelBinnen.Location = New System.Drawing.Point(489, 23)
        Me.lstKomtWelBinnen.Name = "lstKomtWelBinnen"
        Me.lstKomtWelBinnen.Size = New System.Drawing.Size(196, 340)
        Me.lstKomtWelBinnen.TabIndex = 0
        '
        'tbInstellingen
        '
        Me.tbInstellingen.BackColor = System.Drawing.Color.White
        Me.tbInstellingen.Controls.Add(Me.GroupBox6)
        Me.tbInstellingen.Controls.Add(Me.lblSelectedNetworkAdapter)
        Me.tbInstellingen.Controls.Add(Me.lblInfo)
        Me.tbInstellingen.Controls.Add(Me.lstPoESwitches)
        Me.tbInstellingen.Controls.Add(Me.Command14)
        Me.tbInstellingen.Controls.Add(Me.cmdDoTheMagic)
        Me.tbInstellingen.Controls.Add(Me.cmdZoekPoE)
        Me.tbInstellingen.Controls.Add(Me.GroupBox4)
        Me.tbInstellingen.Controls.Add(Me.GroupBox2)
        Me.tbInstellingen.Controls.Add(Me.GroupBox1)
        Me.tbInstellingen.Controls.Add(Me.lstAdapters)
        Me.tbInstellingen.Location = New System.Drawing.Point(4, 24)
        Me.tbInstellingen.Name = "tbInstellingen"
        Me.tbInstellingen.Padding = New System.Windows.Forms.Padding(3)
        Me.tbInstellingen.Size = New System.Drawing.Size(981, 593)
        Me.tbInstellingen.TabIndex = 0
        Me.tbInstellingen.Text = "Instellingen"
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.cmdAlbireoZoekHelp)
        Me.GroupBox6.Controls.Add(Me.cmdINEX500iniSturen)
        Me.GroupBox6.Controls.Add(Me.cmdFindAIP)
        Me.GroupBox6.Controls.Add(Me.txtfindAlbireoTo)
        Me.GroupBox6.Controls.Add(Me.Label13)
        Me.GroupBox6.Controls.Add(Me.txtFindAlbireoFrom)
        Me.GroupBox6.Controls.Add(Me.Label4)
        Me.GroupBox6.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(413, 168)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(391, 112)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Albireo's"
        '
        'cmdAlbireoZoekHelp
        '
        Me.cmdAlbireoZoekHelp.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.cmdAlbireoZoekHelp.BackColor = System.Drawing.Color.Transparent
        Me.cmdAlbireoZoekHelp.BackgroundImage = CType(resources.GetObject("cmdAlbireoZoekHelp.BackgroundImage"), System.Drawing.Image)
        Me.cmdAlbireoZoekHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdAlbireoZoekHelp.ForeColor = System.Drawing.Color.White
        Me.cmdAlbireoZoekHelp.Location = New System.Drawing.Point(185, 80)
        Me.cmdAlbireoZoekHelp.Name = "cmdAlbireoZoekHelp"
        Me.cmdAlbireoZoekHelp.Size = New System.Drawing.Size(32, 26)
        Me.cmdAlbireoZoekHelp.TabIndex = 3
        Me.cmdAlbireoZoekHelp.TabStop = False
        Me.cmdAlbireoZoekHelp.UseVisualStyleBackColor = False
        '
        'cmdINEX500iniSturen
        '
        Me.cmdINEX500iniSturen.Location = New System.Drawing.Point(14, 80)
        Me.cmdINEX500iniSturen.Name = "cmdINEX500iniSturen"
        Me.cmdINEX500iniSturen.Size = New System.Drawing.Size(126, 26)
        Me.cmdINEX500iniSturen.TabIndex = 2
        Me.cmdINEX500iniSturen.TabStop = False
        Me.cmdINEX500iniSturen.Text = "INEX 500 ini ->"
        Me.cmdINEX500iniSturen.UseVisualStyleBackColor = True
        '
        'cmdFindAIP
        '
        Me.cmdFindAIP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdFindAIP.Location = New System.Drawing.Point(261, 80)
        Me.cmdFindAIP.Name = "cmdFindAIP"
        Me.cmdFindAIP.Size = New System.Drawing.Size(124, 26)
        Me.cmdFindAIP.TabIndex = 4
        Me.cmdFindAIP.TabStop = False
        Me.cmdFindAIP.Text = "Zoeken..."
        Me.cmdFindAIP.UseVisualStyleBackColor = True
        '
        'txtfindAlbireoTo
        '
        Me.txtfindAlbireoTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtfindAlbireoTo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfindAlbireoTo.Location = New System.Drawing.Point(53, 51)
        Me.txtfindAlbireoTo.Name = "txtfindAlbireoTo"
        Me.txtfindAlbireoTo.Size = New System.Drawing.Size(332, 23)
        Me.txtfindAlbireoTo.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(16, 56)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 18)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Tot:"
        '
        'txtFindAlbireoFrom
        '
        Me.txtFindAlbireoFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFindAlbireoFrom.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFindAlbireoFrom.Location = New System.Drawing.Point(53, 22)
        Me.txtFindAlbireoFrom.Name = "txtFindAlbireoFrom"
        Me.txtFindAlbireoFrom.Size = New System.Drawing.Size(332, 23)
        Me.txtFindAlbireoFrom.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(11, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 18)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Van:"
        '
        'lblSelectedNetworkAdapter
        '
        Me.lblSelectedNetworkAdapter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSelectedNetworkAdapter.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectedNetworkAdapter.Location = New System.Drawing.Point(416, 384)
        Me.lblSelectedNetworkAdapter.Name = "lblSelectedNetworkAdapter"
        Me.lblSelectedNetworkAdapter.Size = New System.Drawing.Size(550, 40)
        Me.lblSelectedNetworkAdapter.TabIndex = 13
        Me.lblSelectedNetworkAdapter.Text = "         "
        '
        'lblInfo
        '
        Me.lblInfo.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.Location = New System.Drawing.Point(105, 327)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblInfo.Size = New System.Drawing.Size(298, 18)
        Me.lblInfo.TabIndex = 13
        Me.lblInfo.Text = "lblInfo "
        '
        'lstPoESwitches
        '
        Me.lstPoESwitches.ContextMenuStrip = Me.mnuSwitches
        Me.lstPoESwitches.FormattingEnabled = True
        Me.lstPoESwitches.ItemHeight = 18
        Me.lstPoESwitches.Location = New System.Drawing.Point(7, 355)
        Me.lstPoESwitches.Name = "lstPoESwitches"
        Me.lstPoESwitches.Size = New System.Drawing.Size(400, 220)
        Me.lstPoESwitches.Sorted = True
        Me.lstPoESwitches.TabIndex = 3
        Me.lstPoESwitches.TabStop = False
        '
        'mnuSwitches
        '
        Me.mnuSwitches.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.mnuSwitches.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ResetAllePoortenToolStripMenuItem, Me.ResetPoortToolStripMenuItem, Me.ToolStripSeparator2, Me.TelnetToolStripMenuItem, Me.PuttyToolStripMenuItem, Me.ToolStripMenuItem2, Me.ToevoegenToolStripMenuItem, Me.VerwijderenToolStripMenuItem})
        Me.mnuSwitches.Name = "mnuSwitches"
        Me.mnuSwitches.Size = New System.Drawing.Size(169, 148)
        '
        'ResetAllePoortenToolStripMenuItem
        '
        Me.ResetAllePoortenToolStripMenuItem.Name = "ResetAllePoortenToolStripMenuItem"
        Me.ResetAllePoortenToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ResetAllePoortenToolStripMenuItem.Text = "Reset alle poorten"
        '
        'ResetPoortToolStripMenuItem
        '
        Me.ResetPoortToolStripMenuItem.Name = "ResetPoortToolStripMenuItem"
        Me.ResetPoortToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ResetPoortToolStripMenuItem.Text = "Reset poort:"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(165, 6)
        '
        'TelnetToolStripMenuItem
        '
        Me.TelnetToolStripMenuItem.Name = "TelnetToolStripMenuItem"
        Me.TelnetToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.TelnetToolStripMenuItem.Text = "Telnet"
        '
        'PuttyToolStripMenuItem
        '
        Me.PuttyToolStripMenuItem.Name = "PuttyToolStripMenuItem"
        Me.PuttyToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.PuttyToolStripMenuItem.Text = "Putty (SSH)"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(165, 6)
        '
        'ToevoegenToolStripMenuItem
        '
        Me.ToevoegenToolStripMenuItem.Name = "ToevoegenToolStripMenuItem"
        Me.ToevoegenToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ToevoegenToolStripMenuItem.Text = "Toevoegen"
        '
        'VerwijderenToolStripMenuItem
        '
        Me.VerwijderenToolStripMenuItem.Name = "VerwijderenToolStripMenuItem"
        Me.VerwijderenToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.VerwijderenToolStripMenuItem.Text = "Verwijderen"
        '
        'Command14
        '
        Me.Command14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Command14.Location = New System.Drawing.Point(810, 49)
        Me.Command14.Name = "Command14"
        Me.Command14.Size = New System.Drawing.Size(156, 25)
        Me.Command14.TabIndex = 1
        Me.Command14.TabStop = False
        Me.Command14.Text = "Instellingen opslaan"
        Me.Command14.UseVisualStyleBackColor = True
        '
        'cmdDoTheMagic
        '
        Me.cmdDoTheMagic.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDoTheMagic.Location = New System.Drawing.Point(810, 18)
        Me.cmdDoTheMagic.Name = "cmdDoTheMagic"
        Me.cmdDoTheMagic.Size = New System.Drawing.Size(156, 25)
        Me.cmdDoTheMagic.TabIndex = 0
        Me.cmdDoTheMagic.TabStop = False
        Me.cmdDoTheMagic.Text = "Do the Magic"
        Me.cmdDoTheMagic.UseVisualStyleBackColor = True
        '
        'cmdZoekPoE
        '
        Me.cmdZoekPoE.Location = New System.Drawing.Point(6, 324)
        Me.cmdZoekPoE.Name = "cmdZoekPoE"
        Me.cmdZoekPoE.Size = New System.Drawing.Size(93, 25)
        Me.cmdZoekPoE.TabIndex = 2
        Me.cmdZoekPoE.TabStop = False
        Me.cmdZoekPoE.Text = "Zoek PoE"
        Me.cmdZoekPoE.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.txtResetTime)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.txtTotaalAantalVideoServers)
        Me.GroupBox4.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(1029, 337)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(268, 77)
        Me.GroupBox4.TabIndex = 10
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Videoservers"
        Me.GroupBox4.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(11, 47)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(172, 18)
        Me.Label20.TabIndex = 6
        Me.Label20.Text = "Totaal aantal videoservers:"
        '
        'txtResetTime
        '
        Me.txtResetTime.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResetTime.Location = New System.Drawing.Point(186, 19)
        Me.txtResetTime.Name = "txtResetTime"
        Me.txtResetTime.Size = New System.Drawing.Size(71, 23)
        Me.txtResetTime.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(11, 22)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(157, 18)
        Me.Label21.TabIndex = 5
        Me.Label21.Text = "Videoserver reset tijs (s):"
        '
        'txtTotaalAantalVideoServers
        '
        Me.txtTotaalAantalVideoServers.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotaalAantalVideoServers.Location = New System.Drawing.Point(186, 44)
        Me.txtTotaalAantalVideoServers.Name = "txtTotaalAantalVideoServers"
        Me.txtTotaalAantalVideoServers.Size = New System.Drawing.Size(71, 23)
        Me.txtTotaalAantalVideoServers.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtSNMPCommunity)
        Me.GroupBox2.Controls.Add(Me.txtSwitchInlogWachtwoord2)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtAlbireoVLAN)
        Me.GroupBox2.Controls.Add(Me.txtSwitchNetwerk)
        Me.GroupBox2.Controls.Add(Me.txtSwitchInlogWachtwoord1)
        Me.GroupBox2.Controls.Add(Me.txtSwitchInlogNaam)
        Me.GroupBox2.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(413, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(391, 162)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "PoE Switches"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 123)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 18)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "SNMP Community:"
        '
        'txtSNMPCommunity
        '
        Me.txtSNMPCommunity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSNMPCommunity.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSNMPCommunity.Location = New System.Drawing.Point(167, 120)
        Me.txtSNMPCommunity.Name = "txtSNMPCommunity"
        Me.txtSNMPCommunity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSNMPCommunity.Size = New System.Drawing.Size(218, 23)
        Me.txtSNMPCommunity.TabIndex = 5
        Me.txtSNMPCommunity.Text = "iPvgdJn6B4d6mK2vWITmlupsx"
        '
        'txtSwitchInlogWachtwoord2
        '
        Me.txtSwitchInlogWachtwoord2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSwitchInlogWachtwoord2.Location = New System.Drawing.Point(280, 44)
        Me.txtSwitchInlogWachtwoord2.Name = "txtSwitchInlogWachtwoord2"
        Me.txtSwitchInlogWachtwoord2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSwitchInlogWachtwoord2.Size = New System.Drawing.Size(105, 23)
        Me.txtSwitchInlogWachtwoord2.TabIndex = 2
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(11, 100)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(111, 18)
        Me.Label26.TabIndex = 7
        Me.Label26.Text = "VLAN (Albireo's):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(11, 73)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 18)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Netwerk:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(11, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(149, 18)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Inlog wachtwoord(en):"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(11, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(82, 18)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Inlog naam:"
        '
        'txtAlbireoVLAN
        '
        Me.txtAlbireoVLAN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAlbireoVLAN.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlbireoVLAN.Location = New System.Drawing.Point(167, 95)
        Me.txtAlbireoVLAN.Name = "txtAlbireoVLAN"
        Me.txtAlbireoVLAN.Size = New System.Drawing.Size(218, 23)
        Me.txtAlbireoVLAN.TabIndex = 4
        Me.txtAlbireoVLAN.Text = "2"
        '
        'txtSwitchNetwerk
        '
        Me.txtSwitchNetwerk.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSwitchNetwerk.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSwitchNetwerk.Location = New System.Drawing.Point(167, 70)
        Me.txtSwitchNetwerk.Name = "txtSwitchNetwerk"
        Me.txtSwitchNetwerk.Size = New System.Drawing.Size(218, 23)
        Me.txtSwitchNetwerk.TabIndex = 3
        '
        'txtSwitchInlogWachtwoord1
        '
        Me.txtSwitchInlogWachtwoord1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSwitchInlogWachtwoord1.Location = New System.Drawing.Point(167, 44)
        Me.txtSwitchInlogWachtwoord1.Name = "txtSwitchInlogWachtwoord1"
        Me.txtSwitchInlogWachtwoord1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSwitchInlogWachtwoord1.Size = New System.Drawing.Size(107, 23)
        Me.txtSwitchInlogWachtwoord1.TabIndex = 1
        '
        'txtSwitchInlogNaam
        '
        Me.txtSwitchInlogNaam.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSwitchInlogNaam.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSwitchInlogNaam.Location = New System.Drawing.Point(167, 18)
        Me.txtSwitchInlogNaam.Name = "txtSwitchInlogNaam"
        Me.txtSwitchInlogNaam.Size = New System.Drawing.Size(218, 23)
        Me.txtSwitchInlogNaam.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.txtWiFiPassword)
        Me.GroupBox1.Controls.Add(Me.txtWiFiSSID)
        Me.GroupBox1.Controls.Add(Me.chkMACuitDatabase)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.txtNegeerAIPAdressen)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtLogPath)
        Me.GroupBox1.Controls.Add(Me.txtEVALogIPAdres)
        Me.GroupBox1.Controls.Add(Me.txtTFTPIPAdres)
        Me.GroupBox1.Controls.Add(Me.txtDatabase)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(400, 274)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Algemeen"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(11, 242)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(119, 18)
        Me.Label19.TabIndex = 16
        Me.Label19.Text = "WiFi wachtwoord:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(11, 217)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(68, 18)
        Me.Label25.TabIndex = 15
        Me.Label25.Text = "WiFi SSID:"
        '
        'txtWiFiPassword
        '
        Me.txtWiFiPassword.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWiFiPassword.Location = New System.Drawing.Point(162, 238)
        Me.txtWiFiPassword.Name = "txtWiFiPassword"
        Me.txtWiFiPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtWiFiPassword.Size = New System.Drawing.Size(230, 23)
        Me.txtWiFiPassword.TabIndex = 7
        '
        'txtWiFiSSID
        '
        Me.txtWiFiSSID.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWiFiSSID.Location = New System.Drawing.Point(162, 213)
        Me.txtWiFiSSID.Name = "txtWiFiSSID"
        Me.txtWiFiSSID.Size = New System.Drawing.Size(230, 23)
        Me.txtWiFiSSID.TabIndex = 6
        '
        'chkMACuitDatabase
        '
        Me.chkMACuitDatabase.AutoSize = True
        Me.chkMACuitDatabase.Location = New System.Drawing.Point(14, 162)
        Me.chkMACuitDatabase.Name = "chkMACuitDatabase"
        Me.chkMACuitDatabase.Size = New System.Drawing.Size(176, 22)
        Me.chkMACuitDatabase.TabIndex = 4
        Me.chkMACuitDatabase.Text = "MAC Adres uit database"
        Me.chkMACuitDatabase.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(11, 189)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(137, 18)
        Me.Label18.TabIndex = 11
        Me.Label18.Text = "Negeer IP addressen:"
        '
        'txtNegeerAIPAdressen
        '
        Me.txtNegeerAIPAdressen.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNegeerAIPAdressen.Location = New System.Drawing.Point(162, 188)
        Me.txtNegeerAIPAdressen.Name = "txtNegeerAIPAdressen"
        Me.txtNegeerAIPAdressen.Size = New System.Drawing.Size(230, 23)
        Me.txtNegeerAIPAdressen.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 97)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 18)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Path logbestanden:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(11, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(148, 18)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Host adres deze server:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Host adres TFTP server:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "INEX 500 Database:"
        '
        'txtLogPath
        '
        Me.txtLogPath.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLogPath.Location = New System.Drawing.Point(162, 94)
        Me.txtLogPath.Name = "txtLogPath"
        Me.txtLogPath.Size = New System.Drawing.Size(230, 23)
        Me.txtLogPath.TabIndex = 3
        '
        'txtEVALogIPAdres
        '
        Me.txtEVALogIPAdres.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEVALogIPAdres.Location = New System.Drawing.Point(162, 68)
        Me.txtEVALogIPAdres.Name = "txtEVALogIPAdres"
        Me.txtEVALogIPAdres.Size = New System.Drawing.Size(230, 23)
        Me.txtEVALogIPAdres.TabIndex = 2
        '
        'txtTFTPIPAdres
        '
        Me.txtTFTPIPAdres.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTFTPIPAdres.Location = New System.Drawing.Point(162, 43)
        Me.txtTFTPIPAdres.Name = "txtTFTPIPAdres"
        Me.txtTFTPIPAdres.Size = New System.Drawing.Size(230, 23)
        Me.txtTFTPIPAdres.TabIndex = 1
        '
        'txtDatabase
        '
        Me.txtDatabase.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDatabase.Location = New System.Drawing.Point(162, 18)
        Me.txtDatabase.Name = "txtDatabase"
        Me.txtDatabase.Size = New System.Drawing.Size(230, 23)
        Me.txtDatabase.TabIndex = 0
        '
        'lstAdapters
        '
        Me.lstAdapters.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstAdapters.FormattingEnabled = True
        Me.lstAdapters.ItemHeight = 18
        Me.lstAdapters.Location = New System.Drawing.Point(413, 427)
        Me.lstAdapters.Name = "lstAdapters"
        Me.lstAdapters.Size = New System.Drawing.Size(553, 148)
        Me.lstAdapters.TabIndex = 5
        Me.lstAdapters.TabStop = False
        '
        'tbDebug
        '
        Me.tbDebug.BackColor = System.Drawing.Color.White
        Me.tbDebug.Controls.Add(Me.chkKomtWelBinnen)
        Me.tbDebug.Controls.Add(Me.txtUDPNaarDeZaak)
        Me.tbDebug.Controls.Add(Me.chkDetail)
        Me.tbDebug.Controls.Add(Me.chkDebugNaarDeZaak)
        Me.tbDebug.Location = New System.Drawing.Point(4, 24)
        Me.tbDebug.Name = "tbDebug"
        Me.tbDebug.Padding = New System.Windows.Forms.Padding(3)
        Me.tbDebug.Size = New System.Drawing.Size(981, 593)
        Me.tbDebug.TabIndex = 1
        Me.tbDebug.Text = "Debug"
        '
        'chkKomtWelBinnen
        '
        Me.chkKomtWelBinnen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkKomtWelBinnen.AutoSize = True
        Me.chkKomtWelBinnen.Location = New System.Drawing.Point(486, 6)
        Me.chkKomtWelBinnen.Name = "chkKomtWelBinnen"
        Me.chkKomtWelBinnen.Size = New System.Drawing.Size(133, 22)
        Me.chkKomtWelBinnen.TabIndex = 7
        Me.chkKomtWelBinnen.Text = "Komt wel binnen"
        Me.chkKomtWelBinnen.UseVisualStyleBackColor = True
        '
        'txtUDPNaarDeZaak
        '
        Me.txtUDPNaarDeZaak.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUDPNaarDeZaak.Location = New System.Drawing.Point(733, 3)
        Me.txtUDPNaarDeZaak.Name = "txtUDPNaarDeZaak"
        Me.txtUDPNaarDeZaak.Size = New System.Drawing.Size(158, 26)
        Me.txtUDPNaarDeZaak.TabIndex = 5
        '
        'chkDetail
        '
        Me.chkDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkDetail.AutoSize = True
        Me.chkDetail.Location = New System.Drawing.Point(897, 6)
        Me.chkDetail.Name = "chkDetail"
        Me.chkDetail.Size = New System.Drawing.Size(70, 22)
        Me.chkDetail.TabIndex = 4
        Me.chkDetail.Text = "Details"
        Me.chkDetail.UseVisualStyleBackColor = True
        '
        'chkDebugNaarDeZaak
        '
        Me.chkDebugNaarDeZaak.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkDebugNaarDeZaak.AutoSize = True
        Me.chkDebugNaarDeZaak.Location = New System.Drawing.Point(631, 6)
        Me.chkDebugNaarDeZaak.Name = "chkDebugNaarDeZaak"
        Me.chkDebugNaarDeZaak.Size = New System.Drawing.Size(96, 22)
        Me.chkDebugNaarDeZaak.TabIndex = 4
        Me.chkDebugNaarDeZaak.Text = "UDP Export"
        Me.chkDebugNaarDeZaak.UseVisualStyleBackColor = True
        '
        'tbOud
        '
        Me.tbOud.Controls.Add(Me.txtSwitchNetworkMask)
        Me.tbOud.Controls.Add(Me.GroupBox5)
        Me.tbOud.Controls.Add(Me.Label15)
        Me.tbOud.Controls.Add(Me.Label14)
        Me.tbOud.Controls.Add(Me.Label12)
        Me.tbOud.Controls.Add(Me.lstEVAVideoServer)
        Me.tbOud.Controls.Add(Me.lstBellatrix)
        Me.tbOud.Controls.Add(Me.lstActiveVideoServers)
        Me.tbOud.Controls.Add(Me.Label11)
        Me.tbOud.Controls.Add(Me.lstActiveATVS)
        Me.tbOud.Location = New System.Drawing.Point(4, 24)
        Me.tbOud.Name = "tbOud"
        Me.tbOud.Size = New System.Drawing.Size(981, 593)
        Me.tbOud.TabIndex = 4
        Me.tbOud.Text = "Oud"
        Me.tbOud.UseVisualStyleBackColor = True
        '
        'txtSwitchNetworkMask
        '
        Me.txtSwitchNetworkMask.Location = New System.Drawing.Point(522, 237)
        Me.txtSwitchNetworkMask.Name = "txtSwitchNetworkMask"
        Me.txtSwitchNetworkMask.Size = New System.Drawing.Size(206, 26)
        Me.txtSwitchNetworkMask.TabIndex = 18
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.List5)
        Me.GroupBox5.Controls.Add(Me.chkAlbireoIPOnly)
        Me.GroupBox5.Controls.Add(Me.Text9)
        Me.GroupBox5.Controls.Add(Me.Text8)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label22)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.Command5)
        Me.GroupBox5.Location = New System.Drawing.Point(470, 27)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(346, 164)
        Me.GroupBox5.TabIndex = 17
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "GroupBox5"
        Me.GroupBox5.Visible = False
        '
        'List5
        '
        Me.List5.FormattingEnabled = True
        Me.List5.ItemHeight = 18
        Me.List5.Location = New System.Drawing.Point(15, 95)
        Me.List5.Name = "List5"
        Me.List5.Size = New System.Drawing.Size(303, 58)
        Me.List5.TabIndex = 2
        '
        'chkAlbireoIPOnly
        '
        Me.chkAlbireoIPOnly.AutoSize = True
        Me.chkAlbireoIPOnly.Location = New System.Drawing.Point(158, 67)
        Me.chkAlbireoIPOnly.Name = "chkAlbireoIPOnly"
        Me.chkAlbireoIPOnly.Size = New System.Drawing.Size(212, 22)
        Me.chkAlbireoIPOnly.TabIndex = 16
        Me.chkAlbireoIPOnly.Text = "Alleen Albireo IP units vinden"
        Me.chkAlbireoIPOnly.UseVisualStyleBackColor = True
        '
        'Text9
        '
        Me.Text9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text9.Location = New System.Drawing.Point(206, 39)
        Me.Text9.Name = "Text9"
        Me.Text9.Size = New System.Drawing.Size(112, 23)
        Me.Text9.TabIndex = 1
        '
        'Text8
        '
        Me.Text8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text8.Location = New System.Drawing.Point(206, 16)
        Me.Text8.Name = "Text8"
        Me.Text8.Size = New System.Drawing.Size(112, 23)
        Me.Text8.TabIndex = 0
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(155, 40)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(31, 18)
        Me.Label23.TabIndex = 18
        Me.Label23.Text = "Tot:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(155, 17)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(36, 18)
        Me.Label22.TabIndex = 19
        Me.Label22.Text = "Van:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(12, 73)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(155, 18)
        Me.Label24.TabIndex = 20
        Me.Label24.Text = "Gevonden overige units:"
        '
        'Command5
        '
        Me.Command5.Location = New System.Drawing.Point(6, 20)
        Me.Command5.Name = "Command5"
        Me.Command5.Size = New System.Drawing.Size(140, 25)
        Me.Command5.TabIndex = 17
        Me.Command5.Text = "Zoek Overige IP"
        Me.Command5.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Enabled = False
        Me.Label15.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(151, 261)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(163, 18)
        Me.Label15.TabIndex = 7
        Me.Label15.Text = "Active EVA Videoserver: 0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Enabled = False
        Me.Label14.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(151, 146)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(115, 18)
        Me.Label14.TabIndex = 8
        Me.Label14.Text = "Active Bellatrix: 0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(151, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(142, 18)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Active Videoservers: 0"
        '
        'lstEVAVideoServer
        '
        Me.lstEVAVideoServer.Enabled = False
        Me.lstEVAVideoServer.FormattingEnabled = True
        Me.lstEVAVideoServer.ItemHeight = 18
        Me.lstEVAVideoServer.Location = New System.Drawing.Point(154, 279)
        Me.lstEVAVideoServer.Name = "lstEVAVideoServer"
        Me.lstEVAVideoServer.Size = New System.Drawing.Size(310, 76)
        Me.lstEVAVideoServer.TabIndex = 4
        '
        'lstBellatrix
        '
        Me.lstBellatrix.Enabled = False
        Me.lstBellatrix.FormattingEnabled = True
        Me.lstBellatrix.ItemHeight = 18
        Me.lstBellatrix.Location = New System.Drawing.Point(154, 164)
        Me.lstBellatrix.Name = "lstBellatrix"
        Me.lstBellatrix.Size = New System.Drawing.Size(310, 76)
        Me.lstBellatrix.TabIndex = 5
        '
        'lstActiveVideoServers
        '
        Me.lstActiveVideoServers.Enabled = False
        Me.lstActiveVideoServers.FormattingEnabled = True
        Me.lstActiveVideoServers.ItemHeight = 18
        Me.lstActiveVideoServers.Location = New System.Drawing.Point(154, 27)
        Me.lstActiveVideoServers.Name = "lstActiveVideoServers"
        Me.lstActiveVideoServers.Size = New System.Drawing.Size(310, 112)
        Me.lstActiveVideoServers.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Enabled = False
        Me.Label11.Font = New System.Drawing.Font("Calibri", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(3, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(133, 18)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Active Albireo Touch"
        '
        'lstActiveATVS
        '
        Me.lstActiveATVS.Enabled = False
        Me.lstActiveATVS.FormattingEnabled = True
        Me.lstActiveATVS.ItemHeight = 18
        Me.lstActiveATVS.Location = New System.Drawing.Point(3, 27)
        Me.lstActiveATVS.Name = "lstActiveATVS"
        Me.lstActiveATVS.Size = New System.Drawing.Size(145, 130)
        Me.lstActiveATVS.TabIndex = 2
        '
        'mnuAIP
        '
        Me.mnuAIP.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.mnuAIP.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToevoegen, Me.mnuAanpassenToolStripMenuItem, Me.mnuVerwijderen2, Me.ToolStripSeparator1, Me.mnuFactoryReset, Me.mnuBooloaderUpdate, Me.ToolStripSeparator4, Me.mnuResetPoEPoort, Me.mnuPoortUit, Me.ToolStripMenuItem1, Me.mnuWiFiInstellingenSturenToolStripMenuItem, Me.mnuINEXIniSturenToolStripMenuItem, Me.mnuScriptNaarAlbireoToolStripMenuItem, Me.ToolStripSeparator3, Me.TelnetToolStripMenuItem1, Me.PuttyToolStripMenuItem1})
        Me.mnuAIP.Name = "mnuAIP"
        Me.mnuAIP.Size = New System.Drawing.Size(198, 292)
        '
        'mnuToevoegen
        '
        Me.mnuToevoegen.Name = "mnuToevoegen"
        Me.mnuToevoegen.Size = New System.Drawing.Size(197, 22)
        Me.mnuToevoegen.Text = "Toevoegen"
        '
        'mnuAanpassenToolStripMenuItem
        '
        Me.mnuAanpassenToolStripMenuItem.Name = "mnuAanpassenToolStripMenuItem"
        Me.mnuAanpassenToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.mnuAanpassenToolStripMenuItem.Text = "Aanpassen"
        '
        'mnuVerwijderen2
        '
        Me.mnuVerwijderen2.Name = "mnuVerwijderen2"
        Me.mnuVerwijderen2.Size = New System.Drawing.Size(197, 22)
        Me.mnuVerwijderen2.Text = "Verwijderen"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(194, 6)
        '
        'mnuFactoryReset
        '
        Me.mnuFactoryReset.Name = "mnuFactoryReset"
        Me.mnuFactoryReset.Size = New System.Drawing.Size(197, 22)
        Me.mnuFactoryReset.Text = "Factory reset"
        '
        'mnuBooloaderUpdate
        '
        Me.mnuBooloaderUpdate.Name = "mnuBooloaderUpdate"
        Me.mnuBooloaderUpdate.Size = New System.Drawing.Size(197, 22)
        Me.mnuBooloaderUpdate.Text = "Bootloader update"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(194, 6)
        '
        'mnuResetPoEPoort
        '
        Me.mnuResetPoEPoort.Name = "mnuResetPoEPoort"
        Me.mnuResetPoEPoort.Size = New System.Drawing.Size(197, 22)
        Me.mnuResetPoEPoort.Text = "Reset PoE poort"
        '
        'mnuPoortUit
        '
        Me.mnuPoortUit.Name = "mnuPoortUit"
        Me.mnuPoortUit.Size = New System.Drawing.Size(197, 22)
        Me.mnuPoortUit.Text = "PoE Poort uit"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(194, 6)
        '
        'mnuWiFiInstellingenSturenToolStripMenuItem
        '
        Me.mnuWiFiInstellingenSturenToolStripMenuItem.Name = "mnuWiFiInstellingenSturenToolStripMenuItem"
        Me.mnuWiFiInstellingenSturenToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.mnuWiFiInstellingenSturenToolStripMenuItem.Text = "WiFi Instellingen sturen"
        '
        'mnuINEXIniSturenToolStripMenuItem
        '
        Me.mnuINEXIniSturenToolStripMenuItem.Name = "mnuINEXIniSturenToolStripMenuItem"
        Me.mnuINEXIniSturenToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.mnuINEXIniSturenToolStripMenuItem.Text = "INEX ini sturen"
        '
        'mnuScriptNaarAlbireoToolStripMenuItem
        '
        Me.mnuScriptNaarAlbireoToolStripMenuItem.Name = "mnuScriptNaarAlbireoToolStripMenuItem"
        Me.mnuScriptNaarAlbireoToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.mnuScriptNaarAlbireoToolStripMenuItem.Text = "Script naar Albireo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(194, 6)
        '
        'TelnetToolStripMenuItem1
        '
        Me.TelnetToolStripMenuItem1.Name = "TelnetToolStripMenuItem1"
        Me.TelnetToolStripMenuItem1.Size = New System.Drawing.Size(197, 22)
        Me.TelnetToolStripMenuItem1.Text = "Telnet"
        '
        'PuttyToolStripMenuItem1
        '
        Me.PuttyToolStripMenuItem1.Name = "PuttyToolStripMenuItem1"
        Me.PuttyToolStripMenuItem1.Size = New System.Drawing.Size(197, 22)
        Me.PuttyToolStripMenuItem1.Text = "Putty (SSH)"
        '
        'lblAantalFouten
        '
        Me.lblAantalFouten.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAantalFouten.AutoSize = True
        Me.lblAantalFouten.BackColor = System.Drawing.Color.White
        Me.lblAantalFouten.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAantalFouten.ForeColor = System.Drawing.Color.Red
        Me.lblAantalFouten.Location = New System.Drawing.Point(620, 629)
        Me.lblAantalFouten.Name = "lblAantalFouten"
        Me.lblAantalFouten.Size = New System.Drawing.Size(108, 18)
        Me.lblAantalFouten.TabIndex = 4
        Me.lblAantalFouten.Text = "lblAantalFouten"
        '
        'tmrAIPlstADD
        '
        Me.tmrAIPlstADD.Interval = 1000
        '
        'mnuTray
        '
        Me.mnuTray.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.mnuTray.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShutdown})
        Me.mnuTray.Name = "ContextMenuStrip1"
        Me.mnuTray.Size = New System.Drawing.Size(122, 26)
        '
        'mnuShutdown
        '
        Me.mnuShutdown.Name = "mnuShutdown"
        Me.mnuShutdown.Size = New System.Drawing.Size(121, 22)
        Me.mnuShutdown.Text = "Afsluiten"
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(143, 627)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(471, 26)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.Text = "lblStatus"
        '
        'lblStatus2
        '
        Me.lblStatus2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus2.Location = New System.Drawing.Point(729, 629)
        Me.lblStatus2.Name = "lblStatus2"
        Me.lblStatus2.Size = New System.Drawing.Size(396, 18)
        Me.lblStatus2.TabIndex = 4
        Me.lblStatus2.Text = "lblStatus2"
        Me.lblStatus2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmrDelayBinding
        '
        Me.tmrDelayBinding.Interval = 1000
        '
        'tmrCheckUnits
        '
        Me.tmrCheckUnits.Interval = 1000
        '
        'tmrDelayCheckUnits
        '
        Me.tmrDelayCheckUnits.Enabled = True
        Me.tmrDelayCheckUnits.Interval = 1000
        '
        'tmrStatusUpdate
        '
        Me.tmrStatusUpdate.Enabled = True
        Me.tmrStatusUpdate.Interval = 1000
        '
        'tmrEnableAutorestart
        '
        Me.tmrEnableAutorestart.Enabled = True
        Me.tmrEnableAutorestart.Interval = 1000
        '
        'tmrAIPDood
        '
        Me.tmrAIPDood.Enabled = True
        Me.tmrAIPDood.Interval = 5000
        '
        'btnTab0
        '
        Me.btnTab0.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnTab0.FlatAppearance.BorderSize = 0
        Me.btnTab0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTab0.Location = New System.Drawing.Point(0, 36)
        Me.btnTab0.Name = "btnTab0"
        Me.btnTab0.Size = New System.Drawing.Size(140, 35)
        Me.btnTab0.TabIndex = 8
        Me.btnTab0.TabStop = False
        Me.btnTab0.Text = "Units"
        Me.btnTab0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTab0.UseVisualStyleBackColor = True
        '
        'btnTab1
        '
        Me.btnTab1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnTab1.FlatAppearance.BorderSize = 0
        Me.btnTab1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTab1.Location = New System.Drawing.Point(0, 70)
        Me.btnTab1.Name = "btnTab1"
        Me.btnTab1.Size = New System.Drawing.Size(140, 35)
        Me.btnTab1.TabIndex = 8
        Me.btnTab1.TabStop = False
        Me.btnTab1.Text = "Actieve Units"
        Me.btnTab1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTab1.UseVisualStyleBackColor = True
        '
        'btnTab2
        '
        Me.btnTab2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnTab2.FlatAppearance.BorderSize = 0
        Me.btnTab2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTab2.Location = New System.Drawing.Point(0, 104)
        Me.btnTab2.Name = "btnTab2"
        Me.btnTab2.Size = New System.Drawing.Size(140, 35)
        Me.btnTab2.TabIndex = 8
        Me.btnTab2.TabStop = False
        Me.btnTab2.Text = "Instellingen"
        Me.btnTab2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTab2.UseVisualStyleBackColor = True
        '
        'btnTab3
        '
        Me.btnTab3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnTab3.FlatAppearance.BorderSize = 0
        Me.btnTab3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTab3.Location = New System.Drawing.Point(0, 138)
        Me.btnTab3.Name = "btnTab3"
        Me.btnTab3.Size = New System.Drawing.Size(140, 35)
        Me.btnTab3.TabIndex = 8
        Me.btnTab3.TabStop = False
        Me.btnTab3.Text = "Debug"
        Me.btnTab3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTab3.UseVisualStyleBackColor = True
        '
        'frmPoE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1131, 659)
        Me.Controls.Add(Me.btnTab3)
        Me.Controls.Add(Me.btnTab2)
        Me.Controls.Add(Me.btnTab1)
        Me.Controls.Add(Me.btnTab0)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lblStatus2)
        Me.Controls.Add(Me.lblAantalFouten)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button1)
        Me.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(-2, 0)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmPoE"
        Me.Text = "PoE Watchdog"
        Me.TabControl1.ResumeLayout(False)
        Me.tbUnits.ResumeLayout(False)
        Me.tbUnits.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.tbActiveUnits.ResumeLayout(False)
        Me.tbActiveUnits.PerformLayout()
        Me.tbInstellingen.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.mnuSwitches.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tbDebug.ResumeLayout(False)
        Me.tbDebug.PerformLayout()
        Me.tbOud.ResumeLayout(False)
        Me.tbOud.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.mnuAIP.ResumeLayout(False)
        Me.mnuTray.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tbInstellingen As System.Windows.Forms.TabPage
    Friend WithEvents tbDebug As System.Windows.Forms.TabPage
    Friend WithEvents tbUnits As System.Windows.Forms.TabPage
    Friend WithEvents tbActiveUnits As System.Windows.Forms.TabPage
    Friend WithEvents lstAdapters As System.Windows.Forms.ListBox
    'Friend WithEvents lvUnits As ffListView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLogPath As System.Windows.Forms.TextBox
    Friend WithEvents txtEVALogIPAdres As System.Windows.Forms.TextBox
    Friend WithEvents txtTFTPIPAdres As System.Windows.Forms.TextBox
    Friend WithEvents txtDatabase As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtSwitchNetwerk As System.Windows.Forms.TextBox
    Friend WithEvents txtSwitchInlogWachtwoord1 As System.Windows.Forms.TextBox
    Friend WithEvents txtSwitchInlogNaam As System.Windows.Forms.TextBox
    Friend WithEvents txtSwitchInlogWachtwoord2 As System.Windows.Forms.TextBox
    Friend WithEvents txtUDPNaarDeZaak As System.Windows.Forms.TextBox
    Friend WithEvents chkDetail As System.Windows.Forms.CheckBox
    Friend WithEvents chkDebugNaarDeZaak As System.Windows.Forms.CheckBox
    Friend WithEvents lblAantalFouten As System.Windows.Forms.Label
    Friend WithEvents tmrgTimeOut As System.Windows.Forms.Timer
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEnableRestart As System.Windows.Forms.CheckBox
    Friend WithEvents lblNumberOfAIPs As System.Windows.Forms.Label
    Friend WithEvents lstAIPDood As System.Windows.Forms.ListBox
    Friend WithEvents lstAIP As System.Windows.Forms.ListBox
    Friend WithEvents lstKomtWelBinnen As System.Windows.Forms.ListBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lstPoESwitches As System.Windows.Forms.ListBox
    Friend WithEvents cmdZoekPoE As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtResetTime As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtTotaalAantalVideoServers As System.Windows.Forms.TextBox
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents chkKomtWelBinnen As System.Windows.Forms.CheckBox
    Friend WithEvents tmrAIPlstADD As System.Windows.Forms.Timer
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblStatus2 As System.Windows.Forms.Label
    Friend WithEvents lstDisableAutorestartTime As System.Windows.Forms.ListBox
    Friend WithEvents tmrDelayBinding As System.Windows.Forms.Timer
    Friend WithEvents mnuTray As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShutdown As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Command14 As System.Windows.Forms.Button
    Friend WithEvents cmdDoTheMagic As System.Windows.Forms.Button
    Friend WithEvents tmrCheckUnits As System.Windows.Forms.Timer
    Friend WithEvents tmrDelayCheckUnits As System.Windows.Forms.Timer
    Friend WithEvents lblSelectedNetworkAdapter As System.Windows.Forms.Label
    Friend WithEvents mnuSwitches As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ResetAllePoortenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetPoortToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tmrStatusUpdate As System.Windows.Forms.Timer
    Friend WithEvents cmdResetPoE As System.Windows.Forms.Button
    Friend WithEvents mnuAIP As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuFactoryReset As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBooloaderUpdate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuResetPoEPoort As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPoortUit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdDBResync As System.Windows.Forms.Button
    Friend WithEvents chkAutoVervangUnits As System.Windows.Forms.CheckBox
    Friend WithEvents cmdDisableRestartTmp As System.Windows.Forms.Button
    Friend WithEvents tmrEnableAutorestart As System.Windows.Forms.Timer
    Friend WithEvents chkMACuitDatabase As System.Windows.Forms.CheckBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtNegeerAIPAdressen As System.Windows.Forms.TextBox
    Friend WithEvents cmdToevoegen As System.Windows.Forms.Button
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuAanpassenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmrAIPDood As System.Windows.Forms.Timer
    Friend WithEvents btnTab0 As System.Windows.Forms.Button
    Friend WithEvents btnTab1 As System.Windows.Forms.Button
    Friend WithEvents btnTab2 As System.Windows.Forms.Button
    Friend WithEvents btnTab3 As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdFindAIP As System.Windows.Forms.Button
    Friend WithEvents txtfindAlbireoTo As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtFindAlbireoFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbOud As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents List5 As System.Windows.Forms.ListBox
    Friend WithEvents chkAlbireoIPOnly As System.Windows.Forms.CheckBox
    Friend WithEvents Text9 As System.Windows.Forms.TextBox
    Friend WithEvents Text8 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Command5 As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lstEVAVideoServer As System.Windows.Forms.ListBox
    Friend WithEvents lstBellatrix As System.Windows.Forms.ListBox
    Friend WithEvents lstActiveVideoServers As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lstActiveATVS As System.Windows.Forms.ListBox
    Friend WithEvents lblFoundAlbireos As System.Windows.Forms.Label
    Friend WithEvents lstFoundAlbireos As System.Windows.Forms.ListBox
    Friend WithEvents mnuWiFiInstellingenSturenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuINEXIniSturenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuScriptNaarAlbireoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtWiFiPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtWiFiSSID As System.Windows.Forms.TextBox
    Friend WithEvents cmdINEX500iniSturen As System.Windows.Forms.Button
    Friend WithEvents txtSwitchNetworkMask As System.Windows.Forms.TextBox
    Friend WithEvents cmdAlbireoZoekHelp As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSNMPCommunity As System.Windows.Forms.TextBox
    Friend WithEvents mnuToevoegen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuVerwijderen2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TelnetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuttyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TelnetToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuttyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents VerwijderenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label26 As Label
    Friend WithEvents txtAlbireoVLAN As TextBox
    Friend WithEvents ToevoegenToolStripMenuItem As ToolStripMenuItem
End Class
