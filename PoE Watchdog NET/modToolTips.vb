﻿'Dit laat ik even hier als geheugensteun
'Module modToolTip
'    '========================================Create ToolTips
'    Dim E
'        E = vbCrLf
'        With PoEToolTip
'            Call .Create(frmPoE)
'            .MaxTipWidth = 240
'            .DelayTime(ttDelayShow) = 20000
'            .DelayTime(ttDelayInitial) = 1000
'            .ToolTipHeader = ""
'            cmdOpnieuwInlezen.ToolTipText = ""
'    Dim ctrl As Control
'            For Each ctrl In Controls
'                Call .AddTool(ctrl)
'            Next ctrl

'            .ToolTipHeaderShow = False
'            .ToolText(cmdUpdateFirmware) = "Hiermee wordt een nieuwe applicatie naar " & vbCrLf & _
'                                                                    "óf de geselecteerde unit " & vbCrLf & _
'                                                                    "óf alle units gestuurd." & vbCrLf & vbCrLf & _
'                                                                    "De unit wordt, na het onvangen van de " & vbCrLf & _
'                                                                    "applicatie, opnieuw gestart."
'            .ToolText(Command9) = "Hiermee wordt een herstart commando " & vbCrLf & _
'                                                    "naar de de geselecteerde unit gestuurd." & vbCrLf & vbCrLf & _
'                                                    "Als de unit hierop reageert zal die opnieuw" & vbCrLf & _
'                                                    "opstarten. Zoniet: gebruik dan Reset"

'            .ToolText(Command6) = "Hiermee wordt de PoE switch waar de" & vbCrLf & _
'                                                    "geselecteerde unit op aan gesloten is" & vbCrLf & _
'                                                    "vezoekt de desbetreffende poort uit en " & vbCrLf & _
'                                                    "weer aan te zetten."

'            .ToolText(cmdToevoegen) = "Hiermee wordt een unit toegevoegd" & vbCrLf & _
'                                                    "aan de lijst van ATVS / AIP."

'            .ToolText(cmdToevoegen) = "Hiermee wordt een unit verwijderd" & vbCrLf & _
'                                                    "uit de lijst van ATVS / AIP."

'            .ToolText(cmdPoortUit) = "Een noodmaatregel: Ja, hier zet je de " & vbCrLf & _
'                                                    "PoE poort van de geselecteerde unit UIT." & vbCrLf & _
'                                                    "Werkt alleen bij AIP units, ATVS units gaan" & vbCrLf & _
'                                                    "vanzelf weer aan." & vbCrLf & _
'                                                    "Wil je een ATVS uit zetten dan moet je de poort" & vbCrLf & _
'                                                    "uitzetten en gelijk de unit verwijderen."

'            .ToolText(cmdOpnieuwInlezen) = "Als de gegevens van een unit de eerste keer " & vbCrLf & _
'                                                    "niet goed zijn ingezlezen (van de switch) dan " & vbCrLf & _
'                                                    "kan je hiermee de geselecteerde unit opnieuw" & vbCrLf & _
'                                                    "inlezen."

'            .ToolText(cmdTelnet) = "Maakt een Telnet verbinding met de  geselecteerde unit." & vbCrLf & _
'                                                    "Gebeurt trouwens ook als je op de unit (in " & vbCrLf & _
'                                                    "de units lijst) dubbelklikt"

'            .ToolText(Command8) = "Programmeer de geselecteerde ATVS om het" & vbCrLf & _
'                                                    "Scherm Actief vinkje te activeren." & vbCrLf & _
'                                                    vbCrLf & _
'                                                    "Dit zal, via FTP, de ATVS.INI vervangen in de ATVS unit."

'            .ToolText(chkScreenActive) = "Als dit is aangevinkt kan je video beelden opvragen" & vbCrLf & _
'                                                             "d.m.v. het scherm aan te raken." & vbCrLf & _
'                                                             vbCrLf & _
'                                                             "Deze feature wordt pas geactiveerd in ATVS versie 4."

'            .ToolText(cmdZoekPoE) = "Hiermee wordt een discover losgelaten op " & vbCrLf & _
'                                                    "alle IP-adressen die op deze PC aanwezig zijn." & vbCrLf & _
'                                                    "Alle PoE switches van 3Com komen dan in de" & vbCrLf & _
'                                                    "lijst hieronder te staan."

'            .ToolText(Command5) = "Hiermee wordt een ping gestuurd naar alle" & vbCrLf & _
'                                                    "IP-adressen tussen de twee adressen hieronder." & vbCrLf & _
'                                                    "Als er een reactie op de ping komt kijk ik of het " & vbCrLf & _
'                                                    "om een Albireo IP gaat (MAC adres met 00256E)." & vbCrLf & _
'                                                    "Zo ja stop ik het in de lijst hieronder."

'            .ToolText(cmdDoTheMagic) = "Hiermee wis ik de lijst met units. Die stel ik dan" & vbCrLf & _
'                                    "opnieuw stamen met de IP-adressen uit de lijsten: " & E & _
'                                    "'Active Albireo Touch'" & E & _
'                                    "'Active Video servers'" & E & _
'                                    "'Active Bellatrixen'" & E & _
'                                    "'Gevonden AIP units'" & E & _
'                                    "'Gevonden overige units'." & vbCrLf & _
'                                    "Vervolgens vraag ik de switches welke MACadressen op " & E & _
'                                    "welke poorten hangen en stel ik de nieuwe lijst samen."

'            .ToolText(Command10) = "Hiermee wordt de software afgesloten." & vbCrLf & _
'                                    "Als een ATVS unit vast loopt wordt ie niet meer" & vbCrLf & _
'                                    "gereset als deze software niet draait. " & vbCrLf & vbCrLf & _
'                                    "Maar ja, het kan zijn dat je het toch wil sluiten," & vbCrLf & _
'                                    "maar dan moet je wel het dagwachtwoord invoeren."
'            .ToolText(chkDebugNaarDeZaak) = "Hiermee wordt de debug informatie in het scherm" & vbCrLf & _
'                                            "hieronder doorgestuurd naar het IP-adres rechts." & vbCrLf & _
'                                             vbCrLf & _
'                                            "Dit gebruik ik om op de zaak mee te kunnen kijken" & vbCrLf & _
'                                            "naar de debug info om evt. problemen op te sporen." & vbCrLf & _
'                                            vbCrLf & _
'                                            "Debug poort is 44445"
'            .ToolText(chkDetail) = "Hiermee worden meer details in de debug informatie" & vbCrLf & _
'                                                   "weergegeven."

'            .ToolText(List1) = "Deze lijst bevat de IP adressen van ATVS" & E & _
'                               "units. Elke ATVS stuurt een 'Alive' bericht" & E & _
'                               "via UDP. De afzender IP adres van elke alive" & E & _
'                               "bericht wordt hier bijgehouden. Achter het" & E & _
'                               "IP adres staat de tijd dat het bericht ontvangen" & E & _
'                               "werd." & E & _
'                               "Als er xxx staat in plaats van de tijd, dat wordt" & E & _
'                               "deze unit door de PoE watchdog herstart."

'            .ToolText(List2) = "Deze lijst bevat de IP adressen van video" & E & _
'                               "servers. Elke server stuurt een bericht" & E & _
'                               "via UDP. De afzender IP adres van elke" & E & _
'                               "bericht wordt hier bijgehouden. Achter het" & E & _
'                               "IP adres staat de tijd dat het bericht ontvangen" & E & _
'                               "werd." & E & _
'                               "Als er xxx staat in plaats van de tijd, dat wordt" & E & _
'                               "deze unit door de PoE watchdog herstart."

'            .ToolText(lstBellatrix) = "Deze lijst bevat de IP adressen van Bellatrixen." & E & _
'                               "Elke Bellatrix stuurt een bericht" & E & _
'                               "via UDP (994). De afzender IP adres van elke" & E & _
'                               "bericht wordt hier bijgehouden. Achter het" & E & _
'                               "IP adres staat de tijd dat het bericht ontvangen" & E & _
'                               "werd." & E & _
'                               "Als er xxx staat in plaats van de tijd, dat wordt" & E & _
'                               "deze unit door de PoE watchdog herstart."

'            .ToolText(List3) = "Deze lijst bevat de IP adressen van de gevonden" & E & _
'                               "switches. Klik een keer op een IP adres om het" & E & _
'                               "MAC adres te tonen. Dubbelklik op een IPadres" & E & _
'                               "om een telnet sessie met de desbetreffende switch" & E & _
'                               "te openen" & E & E & _
'                               "Met een rechtermuisklik kan je het IP adres van" & E & _
'                               "een switch handmatig invoeren."

'            .ToolText(List4) = "Deze lijst bevat de IP adressen van de gevonden" & E & _
'                               "Albireo IP units. Klik een keer op een IP adres om het" & E & _
'                               "MAC adres te tonen." & E & E & _
'                               "Dubbelklik op een IP adres om deze toe te voegen" & E & _
'                               "aan de lijst met units op de eerste tabblad."

'            .ToolText(List5) = "Deze lijst bevat de IP adressen van apparaten die" & E & _
'                               "op een ping gereageerd hebben. Ze worden met 'de magic' " & E & _
'                               "toegevoegd aan de Unit lijst als type 'div(ersen)'."

'            .ToolText(lstAIP) = "Deze lijst bevat de IP adressen van AIP units." & E & _
'                               "Elke AIP unit stuurt een bericht via UDP. " & E & _
'                               "De afzender IP adres van elke bericht wordt " & E & _
'                               "hier bijgehouden. Achter het IP adres staat " & E & _
'                               "de tijd dat het bericht ontvangen werd." & E & _
'                               "Als er xxx staat in plaats van de tijd, dat wordt" & E & _
'                               "deze unit door de PoE watchdog herstart."

'            .ToolText(chkEnableRestart) = _
'                               "Als dit aangevinkt is dan zullen units, waarvan een " & E & _
'                               "vast ingestelde tijd geen bericht ontvangen is, " & E & _
'                               "herstart worden. Dit doe ik door de PoE poort waaraan " & E & _
'                               "die hangt uit en aan te zetten. Geld alleen voor" & E & _
'                               "ATVS clients / servers en AIP units"

'            .ToolText(Command11) = _
'                               "Als er een aantal units toegevoegd zijn, en je niet heel " & E & _
'                               "de 'Magic' weer door wilt lopen, dan kan je hier mee" & E & _
'                               "even de namen van de datrabase ophalen."
'            .ToolText(cmdKillStart) = _
'                               "Hiermee wordt het Apploader van de ATVS client verwijderd " & E & _
'                               "waardoor de ATVS client software niet meer opstart. Dit moet " & E & _
'                               "omdat de video dlls niet overschreven worden als ze nog in gebruik zijn."
'            .ToolText(cmdVerwijderen) = _
'                               "Hiermee wordt de geselecterde unit uit de lijst verwijderd."

'            .ToolText(cmdExportSettings) = _
'                               "Hiermee wordt de lijst van units (eerste tab) geëxporteerd" & E & _
'                               "in CSV formaat t.b.v. documentatie."

'            .ToolText(chkAlbireoIPOnly) = _
'                               "Als dit aangevinkt is worden, tijdens het 'Zoek IPReo's' " & E & _
'                               "opdracht, alleen  IP adressen van AIP units in de lijst(en) " & E & _
'                               "gezet. Vinkje uit - dan wordt elke IP adres die op een ping" & E & _
'                               "reageert in de lijsten gezet (AIP units links, overige rechts)"

'            .ToolText(txtTotaalAantalVideoServers) = _
'                               "Hiermee geef je aan hoeveel videoservers er totaal aanwezig " & E & _
'                               "zijn in dit systeem. Dit is van belang vanaf ATVS versie 4.8"

'            .ToolText(txtSwitchInlogNaam) = _
'                               "Dit is de inlognaam die je gebruikt om in de switches in te loggen. "

'            .ToolText(txtSwitchInlogWachtwoord) = _
'                               "Het wachtwoord om in de switches te loggen."

'            .ToolText(txtSwitchNetwerk) = _
'                               "Vul hier het IP-adres van een van de switches in met als laatste cijfer een 0. " & E & _
'                               "Als je niets invuld zal ik - zoals vroeger - kijken in mijn huidige netwerk."

'            .ToolText(txtSwitchNetworkMask) = _
'                               "Vul hier het subnetmasker van de switches in. Dit bepaald in hoeveel reeksen ik ga zoeken naar switches." & E & _
'                               "Als je niets invuld zal ik - zoals vroeger - kijken in mijn huidige netwerk."
'            .ToolText(lstAdapters) = _
'                               "Hier staan de netwerk kaarten in de PC. PacketX kan op een van deze " & E & _
'                               "kaarten mee kijken naar DHCP pakketten als de DHCP poort al bezet is." & E & E & _
'                               "Als de DHCP poort nog vrij is (dus een DHCP server draait NIET op deze" & E & _
'                               "PC) dan geef ik daar de voorkeur aan en gebruik ik deze netwerk kaarten " & E & _
'                               "of PacketX NIET."


'        End With
'End Module
