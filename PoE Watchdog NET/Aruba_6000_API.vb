﻿Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web
Imports Newtonsoft.Json.Linq
Imports System.Xml.Serialization

Module Aruba_6000_API
    Public CookieJar As New CookieContainer
    'Public Const Host As String = "10.26.1.165"
    ''' <summary>
    ''' When enabling or disabling a PoE port we use this enum to simplify the call
    ''' </summary>
    Public Enum PortState
        Enabled = 1
        Disabled = 0
    End Enum

    Public AlbireoVLAN As Integer = 2

    ''' <summary>
    ''' This will either enable or disable a PoE port
    ''' </summary>
    ''' <param name="Username">Username</param>
    ''' <param name="Password">Password</param>
    ''' <param name="Host">The switches IP address of host name</param>
    ''' <param name="Port">The ports to be enabled/disabled</param>
    ''' <param name="PortOff">True if the port should be disabled </param>
    ''' <param name="PortOnAagin">True if the port should be enabled after being disabled</param>
    ''' <returns></returns>
    ''' 
    Function ArubaPoEPort(Username As String, Password As String, Host As String, Port As String(), PortOff As Boolean, PortOnAagin As Boolean) As Boolean
        'If ArubaLogin(Host, HttpUtility.UrlEncode(Username), HttpUtility.UrlEncode(Password)) = False Then Return False
        If ArubaLogin(Host, Username, Password) = False Then Return False

        Try
            For Each P In Port
                If P.Length < 3 Then
                    'We use port numbers 1 through 48, these should always be 1/1/xx format so if it's a single port assume 1/1/...
                    P = "1/1/" & P
                End If
                Bug($"Turning port {P} on switch {Host} off.")
                P = HttpUtility.UrlEncode(P)
                Dim req As HttpWebRequest = DirectCast(WebRequest.Create($"https://{Host}/rest/v10.08/system/interfaces/{P}/poe_interface"), HttpWebRequest)
                req.Method = "PUT"
                req.CookieContainer = CookieJar
                req.ContentType = "application/json"

                'authenticatedRequest.Headers.Add("Cookie", "session=" & sessionID)
                req.Proxy = Nothing
                req.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors
                Dim PortState As String = IIf(PortOff = False, "false", "true")

                Dim JSON As String = $"{{""config"": {{""admin_disable"": {PortState},""allocate_by_method"": ""usage"",""cfg_assigned_class"": ""class4"",""pd_class_override"": true,""pre_standard_detect"": false,""priority"": ""low""}},""poe_cycle_timeout"": 0}}"

                Dim dataBytes As Byte() = Encoding.UTF8.GetBytes(JSON)
                req.ContentLength = dataBytes.Length

                Using stream As Stream = req.GetRequestStream()
                    stream.Write(dataBytes, 0, dataBytes.Length)
                End Using

                Dim response As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
                Using reader As New StreamReader(response.GetResponseStream())
                    Console.WriteLine(reader.ReadToEnd())
                End Using
            Next
            'And on again
            'Threading.Thread.Sleep(200)
            For Each P In Port
                If PortOnAagin Then

                    If P.Length < 3 Then
                        'We use port numbers 1 through 48, these should always be 1/1/xx format so if it's a single port assume 1/1/...
                        P = "1/1/" & P
                    End If
                    Bug($"Turning port {P} on switch {Host} on again.")
                    P = HttpUtility.UrlEncode(P)
                    Dim req As HttpWebRequest = DirectCast(WebRequest.Create($"https://{Host}/rest/v10.08/system/interfaces/{P}/poe_interface"), HttpWebRequest)
                    req.Method = "PUT"
                    req.CookieContainer = CookieJar
                    req.ContentType = "application/json"

                    'authenticatedRequest.Headers.Add("Cookie", "session=" & sessionID)
                    req.Proxy = Nothing
                    req.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors
                    Dim PortState As String = "false"

                    Dim JSON As String = $"{{""config"": {{""admin_disable"": {PortState},""allocate_by_method"": ""usage"",""cfg_assigned_class"": ""class4"",""pd_class_override"": true,""pre_standard_detect"": false,""priority"": ""low""}},""poe_cycle_timeout"": 0}}"

                    Dim dataBytes As Byte() = Encoding.UTF8.GetBytes(JSON)
                    req.ContentLength = dataBytes.Length

                    Using stream As Stream = req.GetRequestStream()
                        stream.Write(dataBytes, 0, dataBytes.Length)
                    End Using

                    Dim response As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
                    Using reader As New StreamReader(response.GetResponseStream())
                        Console.WriteLine(reader.ReadToEnd())
                    End Using
                End If

            Next
            ArubaLogout(Host)
            Return True
        Catch ex As Exception
            Bug(ex.Message)
            ArubaLogout(Host)
            Return False
        End Try

    End Function
    ''' <summary>
    ''' Log in to the switch and put the session cookie in the CookieJar
    ''' </summary>
    ''' <param name="Host">IP addres or host name of the switch</param>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <returns>True if logon was successful, false if not.</returns>
    Function ArubaLogin(Host As String, UserName As String, Password As String) As Boolean
        UserName = HttpUtility.UrlEncode(UserName)
        Password = HttpUtility.UrlEncode(Password)

        Bug($"Logging in....{Host}")

        Dim request As HttpWebRequest
        Dim APIResponse As HttpWebResponse
        Try
            Dim url As String = $"https://{Host}/rest/v10.08/login"

            request = DirectCast(WebRequest.Create(url), HttpWebRequest)
            'Bug("10")
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.Proxy = Nothing
            request.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors
            'Bug("11")
            Dim postData As String = $"username={UserName}&password={Password}"
            Dim postDataBytes As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentLength = postDataBytes.Length
            'Bug("12")
            Using stream As Stream = request.GetRequestStream()
                'Bug("12a")
                stream.Write(postDataBytes, 0, postDataBytes.Length)
                'Bug("12b")
            End Using
            'Bug("13")
            'Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
            APIResponse = DirectCast(request.GetResponse(), HttpWebResponse)

            Dim sessionID As String = ""
            'Bug("14")
            If APIResponse.Headers("Set-Cookie") <> Nothing Then
                CookieJar.SetCookies(New Uri("https://" & Host), APIResponse.Headers("Set-Cookie"))
                sessionID = APIResponse.Headers("Set-Cookie")
            End If
            'Bug("15")
            Return True
        Catch ex2 As WebException
            Bug("Web exception")
            Dim errorResponse As WebResponse = ex2.Response
            Using errorStream As Stream = errorResponse.GetResponseStream()
                Dim errorReader As New StreamReader(errorStream)
                Dim errorMessage As String = errorReader.ReadToEnd()
                Bug($"ex2: {ex2.Message}, Details: {errorMessage}")
            End Using

        Catch ex As Exception
            Bug($"{ex.Message} Algemene fout tijdens inloggen Aruba switch {Host}")
            If ex.InnerException IsNot Nothing Then
                Bug(ex.InnerException.Message)
            End If
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Logout terminates and frees up the session. Failing to logout will cause subsequent logon failures.<br/>
    ''' In the putty shell you'll then need to "https-server session close all" to be able to log on again
    ''' </summary>
    ''' <param name="Host"></param>
    ''' <returns></returns>
    Function ArubaLogout(Host As String) As Boolean
        Try
            Dim url As String = $"https://{Host}/rest/v10.08/logout"

            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.Proxy = Nothing
            request.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors
            request.CookieContainer = CookieJar

            Dim postData As String = ""
            Dim postDataBytes As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentLength = postDataBytes.Length

            Using stream As Stream = request.GetRequestStream()
                stream.Write(postDataBytes, 0, postDataBytes.Length)
            End Using

            Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
            If response.StatusCode = HttpStatusCode.OK Then
                Bug("Logged out of Aruba switch " & Host)
                Return True
            Else
                Bug($"Error while logging out of Aruba switch {Host}. Return code is {response.StatusCode}")
                Return False
            End If

        Catch ex As Exception
            Bug(ex.Message & " while logging off switch " & Host)
            Return False
        End Try
    End Function
    ''' <summary>
    ''' Ask the switch all the dynamically learned mac adresses and return this as string
    ''' </summary>
    ''' <param name="Host"></param>
    ''' <returns></returns>
    Function ArubaFindMac(Host As String, Username As String, Password As String) As String
        Bug($"Log into Aruba Switch {Host}")
        If ArubaLogin(Host, Username, Password) = False Then Return ""

        If frmPoE.txtAlbireoVLAN.Text <> "" Then
            AlbireoVLAN = frmPoE.txtAlbireoVLAN.Text
        Else
            AlbireoVLAN = 2
        End If
        Bug($"Using VLAN {AlbireoVLAN}")

        Try
            Dim req = DirectCast(HttpWebRequest.Create($"https://{Host}/rest/v10.08/system/vlans/{AlbireoVLAN}/macs?attributes=port&depth=2"), HttpWebRequest)
            req.Method = "GET"
            req.CookieContainer = CookieJar
            req.ContentType = "application/json"
            'Bug("1")
            'authenticatedRequest.Headers.Add("Cookie", "session=" & sessionID)
            req.Proxy = Nothing
            req.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors
            'Bug("2")
            Dim Responce As HttpWebResponse = req.GetResponse
            Dim ResponceStream As System.IO.Stream = Responce.GetResponseStream
            'Bug("3")
            'Create new streamreader
            Dim Streamreader As New IO.StreamReader(ResponceStream)
            Dim JSONData As String = Streamreader.ReadToEnd
            'Bug("4")
            'Put the JSON data into an object and fins all the macaddress/port combo's
            Dim JSONObject = JObject.Parse(JSONData)
            'Bug("5")
            Dim SB As New StringBuilder
            For Each entry In JSONObject.Children(Of JProperty)()
                If entry.Name.StartsWith("dynamic") Then
                    Dim macAddress As String = entry.Name.Substring(8)
                    Dim portNumber As String = entry.Value("port").First.Path.Split("/"c).Last()
                    portNumber = entry.Value("port").First.ToString.Split(":").First.Replace(Chr(34), "")
                    ' Do something with the port number and MAC address (e.g. print them)
                    Bug("MAC address: " & macAddress & " - Port number: " & portNumber)
                    SB.AppendLine("MAC address: " & macAddress & " - Port number: " & portNumber)
                End If
            Next
            'Bug("6")
            Streamreader.Close()
            'Bug("7")
            ArubaLogout(Host)
            'Bug("8")
            Return SB.ToString
        Catch ex As Exception
            Bug($"{ex.Message} during REST API request https://{Host}/rest/v10.08/system/vlans/{AlbireoVLAN}/macs?attributes=port&depth=2")
            ArubaLogout(Host)
            Return ""
        End Try
        'Bug("9")

    End Function
    ''' <summary>
    ''' We need not be logged on to do this, we can use it to check if the switch is compatible with PoE Watchdog
    ''' </summary>
    ''' <param name="Host"></param>
    ''' <returns></returns>
    Function GetVersion(Host As String) As String
        Try
            Dim req = DirectCast(HttpWebRequest.Create($"https://{Host}/rest"), HttpWebRequest)
            req.Method = "GET"

            req.ContentType = "application/json"


            req.Proxy = Nothing
            req.ServerCertificateValidationCallback = Function(sender, certificate, chain, sslPolicyErrors) True ' ignore SSL certificate errors

            Dim Responce As HttpWebResponse = req.GetResponse
            Dim ResponceStream As System.IO.Stream = Responce.GetResponseStream

            'Create new streamreader
            Dim Streamreader As New IO.StreamReader(ResponceStream)
            Dim JSONData As String = Streamreader.ReadToEnd

            'Put the JSON data into an object and fins all the macaddress/port combo's
            Dim JSONObject = JObject.Parse(JSONData)

            Dim Version As String = ""
            For Each entry In JSONObject.Children(Of JProperty)()
                If entry.Name = "latest" Then
                    Version = entry.Value("version")
                    Exit For
                End If
            Next

            Return Version

        Catch ex As Exception
            Bug(ex.Message)
            Return ""
        End Try
    End Function

End Module
