﻿Module modSettings
    ''' <summary>
    ''' This is where we save the lvUnits column widths to be persistand between startups.
    ''' </summary>
    ''' <remarks></remarks>
    Public lvUnitsColumns As Integer()


    ''' <summary>
    ''' This will save or retrieve all settings from the register
    ''' </summary>
    ''' <param name="Save">If save is true, the values will be sritten to the registry, if false, then they will be retrieved and put in the controls</param>
    ''' <remarks></remarks>
    Sub AllSettings(Save As Boolean)

        For i% = 0 To frmPoE.TabControl1.TabPages.Count - 1
            For Each c As Control In frmPoE.TabControl1.TabPages(i).Controls
                If TypeOf (c) Is GroupBox Then
                    For Each c2 As Control In c.Controls
                        Debug.Print("Group: " & c.Name & " - " & c2.Name)
                        Settings(c2, Save)
                    Next
                Else
                    Debug.Print("Rest: " & c.Name)
                    Settings(c, Save)
                End If
            Next
        Next
        If Save Then
            SaveLVUnitsColumns()
            SaveSetting("PoE-NET", "", "frmPoE.Width", frmPoE.Width)
            SaveSetting("PoE-NET", "", "frmPoE.Height", frmPoE.Height)
        End If

    End Sub
    
    ''' <summary>
    ''' Simplified Get/Save settings, puts the vaue of the controle in the register, and retrieves it if neccessary. If Save is ture the setting wil be saved, othwise it wil be returned 
    ''' </summary>
    ''' <param name="WhichControl"></param>
    ''' <param name="Save"></param>
    ''' <remarks></remarks>
    Sub Settings(ByRef WhichControl As Object, Save As Boolean, Optional DefaultValue As String = "")
        If TypeOf (WhichControl) Is Windows.Forms.CheckBox Then
            'In this case we use the checked value
            If Save Then
                SaveSetting("PoE-NET", "Settings", WhichControl.Name, WhichControl.checked)
            Else
                WhichControl.checked = GetSetting("PoE-NET", "Settings", WhichControl.Name, "0")
            End If
        ElseIf TypeOf (WhichControl) Is Windows.Forms.TextBox Then
            'in this case we use the text value
            If Save Then
                SaveSetting("PoE-NET", "Settings", WhichControl.Name, WhichControl.Text)
            Else
                WhichControl.text = GetSetting("PoE-NET", "Settings", WhichControl.name, DefaultValue)
            End If
        End If
    End Sub

    Sub SaveLVUnitsColumns()
        Try
            With frmPoE.lvUnits
                For i% = 0 To 9
                    SaveSetting("POE-NET", "", "lvUnitsColom" & i, .Columns(i).Width)
                Next
            End With
        Catch ex As Exception

        End Try

    End Sub


    ''' <summary>
    ''' Gets the lvUnits saved column widths from file.
    ''' </summary>
    ''' <remarks></remarks>
    Sub GetLVUnitsColumns()
        For i = 0 To 9
            lvUnitsColumns(i) = GetSetting("POE-NET", "", "lvUnitsColom" & i, 0)
        Next
    End Sub

    ''' <summary>
    ''' This is een overridden SaveSettings, that doesn't use the registry but instead uses "app-settings.pwd"" file
    ''' </summary>
    ''' <param name="app"></param>
    ''' <param name="section"></param>
    ''' <param name="key"></param>
    ''' <param name="setting"></param>
    Sub SaveSetting(app As String, section As String, key As String, setting As String)
        Dim FileName As String = Application.StartupPath & "\" & app & "-settings.pwd"
        Dim FoundSetting As Boolean = False
        If My.Computer.FileSystem.FileExists(FileName) Then
            Dim lines As String() = System.IO.File.ReadAllLines(FileName)
            For i% = 0 To lines.Count - 1
                Dim s As String() = Split(lines(i), "þ")
                If Strings.Left(s(0), Len(key)) = key Then
                    s(1) = setting
                    lines(i) = Join(s, "þ")
                    FoundSetting = True
                    Exit For
                End If
            Next
            If FoundSetting Then
                IO.File.WriteAllLines(FileName, lines)
            Else
                ReDim Preserve lines(lines.Count)
                lines(lines.Count - 1) = key & "þ" & setting
                IO.File.WriteAllLines(FileName, lines)
            End If
        Else
            Dim line(0) As String
            line(0) = key & "þ" & setting
            IO.File.WriteAllLines(FileName, line)
        End If
    End Sub

    <DebuggerStepThrough>
    Function GetSetting(app As String, section As String, key As String, Optional [default] As String = "") As String

        Dim FileName As String = Application.StartupPath & "\" & app & "-settings.pwd"
        Dim FoundSetting As Boolean = False
        If My.Computer.FileSystem.FileExists(FileName) Then
            Dim lines As String() = System.IO.File.ReadAllLines(FileName)
            For i% = 0 To lines.Count - 1
                Dim s As String() = Split(lines(i), "þ")
                If Strings.Left(s(0), Len(key)).ToLower = key.ToLower Then
                    Return s(1)
                    FoundSetting = True
                    Exit For
                End If
            Next
            If Not FoundSetting Then
                Return [default]
            End If

        Else
            Return [default]
        End If

    End Function

End Module
