﻿Public Class frmToevoegen

    Private Sub optKindUnit1_CheckedChanged(sender As Object, e As EventArgs) Handles optKindUnit1.CheckedChanged, _
                                                                                      optKindUnit2.CheckedChanged, _
                                                                                      optKindUnit3.CheckedChanged, _
                                                                                      optKindUnit4.CheckedChanged, _
                                                                                      optKindUnit5.CheckedChanged, _
                                                                                      optKindUnit6.CheckedChanged, _
                                                                                      optKindUnit7.CheckedChanged

    End Sub

    Private Sub txtMAC_GotFocus(sender As Object, e As EventArgs) Handles txtMAC.GotFocus
        Label7.Text = "MAC adres formaat 1234-ABCD-5678"
    End Sub

    Private Sub txtMAC_LostFocus(sender As Object, e As EventArgs) Handles txtMAC.LostFocus
        Label7.Text = ""
    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click

        Dim UnitSelected As Boolean = False
        ToevoegenNieuwUnitStr = txtIP.Text & ";" & txtMAC.Text.ToUpper & ";" & txtPoE_IP.Text & ";" & txtPoE_Poort.Text

        For Each opt In Controls
            If TypeOf (opt) Is RadioButton Then
                If opt.Checked Then
                    ToevoegenNieuwUnitStr = ToevoegenNieuwUnitStr & ";" & Strings.Right(opt.Name, 1)
                    UnitSelected = True
                    Exit For
                End If
            End If
        Next

        If UnitSelected = False Then
            MsgBox("Lijkt dat je geen unit hebt gekozen. Dat kan niet.")
            Exit Sub
        End If

        ToevoegenNieuwUnitStr += ";" & txtNaam.Text
        Me.Close()

    End Sub

    Private Sub frmToevoegen_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class