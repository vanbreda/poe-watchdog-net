﻿Imports System.Data.Odbc

Module modDatabase
    Structure MAC_NAAMtype
        Dim MAC As String
        Dim NAAM As String
    End Structure

    ''' <summary>
    ''' Get the (aip) MAC - IPaddress cominations from the INEX 500 database and populates the Public ArpTable() struct.
    ''' </summary>
    ''' <remarks></remarks>
    Sub GetARPTabelFromINEXDatabase()

        Try
            ReDim ArpTable(0)
            Dim connectionString As String = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" & frmPoE.txtDatabase.Text
            'Dim myCon As New OdbcConnection("DSN=inex500")

            Dim myCon As New OdbcConnection(connectionString)
            myCon.Open()

            Dim cmdtxt As String = "SELECT albireo.address, settlement.id_technical FROM albireo INNER JOIN settlement ON albireo.id_settlement = settlement.id_settlement"

            Dim myCmd As New OdbcCommand(cmdtxt, myCon)

            Dim myAdapter As New OdbcDataAdapter(myCmd)

            Dim myData As OdbcDataReader
            myData = myCmd.ExecuteReader

            Dim UB%

            While myData.Read = True
                If Not TypeOf myData("id_technical") Is DBNull Then
                    If InStr(1, myData("id_technical"), "00256E", vbTextCompare) <> 0 Then
                        'If InStr(1, myData("id_technical"), "79EC", vbTextCompare) <> 0 Then Stop
                        UB = UBound(ArpTable) + 1
                        ReDim Preserve ArpTable(UB)

                        ArpTable(UB).MAC_Adres = Mid(myData("id_technical"), 1, 2) & ":" &
                                                 Mid(myData("id_technical"), 3, 2) & ":" &
                                                 Mid(myData("id_technical"), 5, 2) & ":" &
                                                 Mid(myData("id_technical"), 7, 2) & ":" &
                                                 Mid(myData("id_technical"), 9, 2) & ":" &
                                                 Mid(myData("id_technical"), 11, 2)

                        ArpTable(UB).IPAdres = Long2IP(myData("address"))
                        Bug(ArpTable(UB).MAC_Adres & " / " & ArpTable(UB).IPAdres)
                    End If
                End If
            End While

            myCon.Close()
        Catch ex As Exception
            Bug(Err.Description & " in GetARPTabelFromINEXDatabase")


        End Try
    End Sub

    Sub GetDatabase()

        Try
            Dim connectionString As String = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" & frmPoE.txtDatabase.Text
            Dim myCon As New OdbcConnection(connectionString)
            myCon.Open()

            Dim cmdtxt As String = "SELECT settlement.name, settlement.id_technical FROM settlement WHERE settlement.id_technical IS NOT NULL;"
            Bug(cmdtxt)
            Dim myCmd As New OdbcCommand(cmdtxt, myCon)

            Dim myAdapter As New OdbcDataAdapter(myCmd)

            Dim rs As OdbcDataReader
            rs = myCmd.ExecuteReader

            Dim Namen() As MAC_NAAMtype
            ReDim Namen(0)
            Dim ErrStr As String
            ErrStr = "uitvoeren SQL: " & cmdtxt
            While rs.Read = True
                If Not TypeOf rs("id_technical") Is DBNull Then

                    ErrStr = "ophalen van id_technical: " & rs("id_technical")
                    Namen(UBound(Namen)).MAC = rs("id_technical")
                    Namen(UBound(Namen)).NAAM = rs("name")
                    ReDim Preserve Namen(UBound(Namen) + 1)
                    Bug(rs("id_technical") & "/" & rs("name"))
                End If
            End While
            rs.Close()

            cmdtxt = "SELECT albireo_extension.id_technical, albireo_extension.name FROM albireo_extension WHERE albireo_extension.id_technical IS NOT NULL;"
            Bug(cmdtxt)
            ErrStr = "uitvoeren SQL: " & cmdtxt
            myCmd.CommandText = cmdtxt
            rs = myCmd.ExecuteReader

            While rs.Read
                If Not TypeOf rs("id_technical") Is DBNull Then
                    ErrStr = "ophalen van id_technical: " & rs("id_technical")
                    Namen(UBound(Namen)).MAC = rs("id_technical")
                    Namen(UBound(Namen)).NAAM = rs("name")
                    ReDim Preserve Namen(UBound(Namen) + 1)
                    Bug(rs("id_technical") & "/" & rs("name"))
                End If
            End While
            'We have one too many array members, remove the last one
            ReDim Preserve Namen(UBound(Namen) - 1)

            myCon.Close()

            Dim i%, f%

            ErrStr = "...door lijst poe units op MAC zoeken en naam toevoegen."
            Bug(ErrStr)
            For i = 0 To UBound(POE.Unit)
                If POE.Unit(i).KIND <> 0 Then
                    For f = 0 To UBound(Namen)
                        If ChangeMAC(POE.Unit(i).MACaddress) = Namen(f).MAC Then
                            POE.Unit(i).NAAM = Namen(f).NAAM
                            Exit For
                        End If
                    Next f
                End If
            Next i

        Catch ex As Exception
            Bug(Err.Description)
        End Try

    End Sub

    Function ChangeMAC(MAC_Address As String) As String
        ChangeMAC = UCase(Replace(MAC_Address, "-", ""))
    End Function
    ''' <summary>
    ''' Converts a Long made by the INEX in the database to a dotted quad IP string.
    ''' </summary>
    ''' <param name="IP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Long2IP(ByVal IP As Long) As String

        Try
            Dim IP2 As Net.IPAddress = New Net.IPAddress(ConvertInt(IP))
            Return IP2.ToString
        Catch ex As Exception
            Bug($"{ex.Message} tijdens converteren database IP ({CStr(IP)}) to string.")
            Return ""
        End Try




        'Try
        '    Dim H As String = Strings.Right(Hex(IP), 8)
        '    Dim Ant As String = Convert.ToInt32(Mid(H, 7, 2), 16) & "." & Convert.ToInt16(Mid(H, 5, 2), 16) & "." & Convert.ToInt16(Mid(H, 3, 2), 16) & "." & Convert.ToInt16(Mid(H, 1, 2), 16)
        '    Return Ant
        'Catch ex As Exception
        '    Debug.Print(ex.Message)
        '    Return ""
        'End Try

    End Function

    Function ConvertInt(signedInt As Int32) As UInt64

        Dim unsignedLong As UInt64

        ' Use BitConverter to convert the signed int to a byte array
        Dim intBytes As Byte() = BitConverter.GetBytes(signedInt)

        ' Pad the byte array to 8 bytes if needed
        Dim paddedBytes As Byte() = New Byte(7) {}
        Array.Copy(intBytes, paddedBytes, intBytes.Length)

        ' Convert the byte array to UInt64
        unsignedLong = BitConverter.ToUInt64(paddedBytes, 0)
        Return unsignedLong
    End Function

End Module
