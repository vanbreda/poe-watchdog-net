﻿Public Module modPWD
    Structure DHCPOptionType
        Dim Optie As String
        Dim Waarde As String
    End Structure

    Public Function DHCPAnalise(Str As String, FromIPAdres As String)

        Try
            Dim DataBytes() As Byte = System.Text.Encoding.Default.GetBytes(Str)

            Dim i%
            Dim BStr As String = ""
            Dim Macadres As String = ""
            Dim IPAdres As String = ""
            Dim BeginOptions As Integer, DHCPtype(8) As String
            Dim DHCPMSGType As String = ""
            Dim HostName As String = ""

            DHCPtype(1) = "DHCP DSC"
            DHCPtype(2) = "DHCP OFF"
            DHCPtype(3) = "DHCP REQ"
            DHCPtype(5) = "DHCP ACK"
            DHCPtype(8) = "DHCP INF"

            '    For i = 1 To Len(Str)
            '        'If Asc(Mid(Str, Teller, 1)) <> 0 Then BStr = BStr & "(" & Asc(Mid(Str, Teller, 1)) & ") "
            '        BStr = BStr & Asc(Mid(Str, i, 1)) & " "
            '    Next i

            Dim DHCPopt() As DHCPOptionType
            'DHCPopt = GetDHCPOption(Str)



            BeginOptions = InStr(1, Str, Chr(99) & Chr(130) & Chr(83) & Chr(99)) 'This is the magic cookie 'DHCP'



            If BeginOptions <> 0 Then


                BeginOptions = BeginOptions + 4

                DHCPopt = GetDHCPOption(Mid(Str, BeginOptions, Len(Str)))

                For i = 0 To UBound(DHCPopt)

                    With DHCPopt(i)

                        Select Case .Optie

                            Case "53"

                                DHCPMSGType = DHCPtype(CInt(Asc(.Waarde)))

                            Case "12"

                                HostName = .Waarde

                            Case "50"

                                IPAdres = Asc(Mid(.Waarde, 1, 1)) & "." & Asc(Mid(.Waarde, 2, 1)) & "." & Asc(Mid(.Waarde, 3, 1)) & "." & Asc(Mid(.Waarde, 4, 1))


                        End Select
                    End With

                Next i
            Else

                DHCPMSGType = "DHCP ? "
            End If



            Dim d() As String
            Dim IPStr As String = ""
            Dim IPStrTemp As String = ""

            d = Split(BStr, " ")

            Dim yiaddr As String = ""
            Dim ciaddr As String = ""


            For i = 13 To 16
                ciaddr = ciaddr & Asc(Mid(Str, i, 1))
                If i <> 16 Then ciaddr = ciaddr & "."
            Next i

            For i = 17 To 20
                yiaddr = yiaddr & Asc(Mid(Str, i, 1))
                If i <> 20 Then yiaddr = yiaddr & "."
            Next i



            Dim MAC As String = ""
            For i = 29 To 34
                MAC += Right("00" & Hex(Asc(Mid(Str, i, 1))), 2) & ":"
            Next i

            MAC = Mid(MAC, 1, Len(MAC) - 1)

            Debug.Print(": ")

            If DHCPMSGType = "DHCP INF" Then IPAdres = FromIPAdres
            If DHCPMSGType = "DHCP OFF" Then IPAdres = yiaddr

            If IPAdres = "" Then
                If ciaddr <> "0.0.0.0" Then
                    IPAdres = ciaddr
                ElseIf yiaddr <> "0.0.0.0" Then
                    IPAdres = yiaddr
                End If
            End If

            If HostName <> "" Then
                Return (DHCPMSGType & " " & MAC & " (" & HostName & ")  " & IPAdres) ' & " (" & HostName & ") - (" & GetName(IPAdres) & ")")
            Else
                Return (DHCPMSGType & " " & MAC & "  " & IPAdres) ' & " (" & GetName(IPAdres) & ")")
            End If
            Bug("From the sub itsself!!")
        Catch ex As Exception
            Bug("Fout in DHCP analise: " & ex.Message)
        End Try


    End Function

    Public Function GetDHCPOption(DHCPStr As String) As DHCPOptionType()
        Dim Begin As Integer, Optie As String, Lengte As Integer, Waarde As String, DHCPOptie() As DHCPOptionType


        Begin = 1
        ReDim DHCPOptie(0)

        Do

            Optie = Asc(Mid(DHCPStr, Begin, 1))
            If Optie = 255 Then Exit Do 'end of options
            ReDim Preserve DHCPOptie(UBound(DHCPOptie) + 1)
            Lengte = Asc(Mid(DHCPStr, Begin + 1, 1))
            Waarde = Mid(DHCPStr, Begin + 2, Lengte)

            DHCPOptie(UBound(DHCPOptie)).Optie = Optie
            DHCPOptie(UBound(DHCPOptie)).Waarde = Waarde


            Begin = Begin + Lengte + 2
            If Begin >= Len(DHCPStr) Then Exit Do
        Loop
        GetDHCPOption = DHCPOptie

    End Function


    'Sub Bug(Wat As String)
    '    Debug.Print(Wat)
    'End Sub
End Module
