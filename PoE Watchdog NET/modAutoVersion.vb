﻿Module AutoVersion
    Sub AutoVersionIncrease()
        If System.Diagnostics.Debugger.IsAttached Then 'We assume that we're running in the IDE, and this will warrant a Version number increase
            Dim OldVersion As Integer = System.IO.File.ReadAllText(System.IO.Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName & "\Version.txt")
            OldVersion += 1
            System.IO.File.WriteAllText(System.IO.Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName & "\Version.txt", OldVersion)

            Dim F As String = System.IO.Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName & "\AutoVersion.vb"

            Dim S As String = "Imports System.Reflection" & vbCrLf & vbCrLf
            S += "<Assembly: AssemblyVersion(""10.4.@@"")>" & vbCrLf
            S += "<Assembly: AssemblyFileVersion(""10.4.@@"")>" & vbCrLf
            S = Replace(S, "@@", OldVersion)
            System.IO.File.WriteAllText(F, S)
        End If
    End Sub
End Module
