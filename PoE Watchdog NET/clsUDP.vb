﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Text

''' <summary>
''' Dont forget to dispose this class in the unload!!!!!
''' </summary>
''' <remarks></remarks>
Public Class clsUDP
    Implements IDisposable
    Public listenClient As UdpClient
    Public RemoteIP As String
    Public RemotePort As Integer
    Property Started As Boolean
    Public Abort As Boolean = False
    Property Stopped As Boolean = False

    Public _LocalPort As Integer
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="datagram"></param>
    ''' <param name="RemoteEndPoint"></param>
    ''' <param name="remoteipaddress">This is the value of the remote IP adres, doesn't tend to change when things get busy</param>
    ''' <remarks></remarks>
    Public Event Data_Arrival(sender As clsUDP, datagram As String, ByVal RemoteEndPoint As Net.IPEndPoint, ByVal remoteipaddress As String, Data As Byte()) 'ByVal the remote IP address because by the time the event handles this it could be changed... 

    Public Event sckError(ByVal sender As clsUDP, ByVal ex As Exception)

    Private listenThread As Thread
    'Public Event RawDatagram(sender As clsUDP, datagram As Byte(), ByRef handled As Boolean)

    Sub New(RemoteIPAddress As String)
        RemoteIP = RemoteIPAddress
    End Sub

    Sub New(LocalPort As Integer)
        _LocalPort = LocalPort
    End Sub

    Sub New(RemoteIPAddress As String, RemotePortAdsress As Integer)
        RemotePort = RemotePortAdsress
        RemoteIP = RemoteIPAddress
    End Sub
    Sub New()

    End Sub
    ''' <summary>
    ''' This starts the UDP client listening on the local port
    ''' </summary>
    ''' <param name="LocalPort"></param>
    ''' <remarks></remarks>
    Sub Bind(LocalPort As Integer)
        Try
            _LocalPort = LocalPort
            Start()
        Catch ex As Exception
            Bug("Fout tijdens clsUDP.Bind op " & LocalPort & ": " & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' This starts tyhe DP client listening on port 0 
    ''' </summary>
    ''' <remarks></remarks>
    Sub Start()
        Try
            Started = True
            listenThread = New Thread(New ThreadStart(AddressOf ReceiveData))
            listenThread.Start()
        Catch ex As Exception
            Throw ex 'New Exception(ex.Message)
        End Try

    End Sub

    Private Sub ReceiveData()
        Try


            'Debug.Print(",,,,,,,,,,,, Overall listener thread started.")
            Dim EndPoint As IPEndPoint = New IPEndPoint(IPAddress.Any, _LocalPort) '0 is the first free availble port

            listenClient = New UdpClient(EndPoint)
            'Debug.Print(",,,,,,,,,,,, listen client started.")

            While True
                ' Debug.Print("-------listen client listening----------")
                If Me.Abort = True Then Exit While
                Try
                    Dim data() As Byte = listenClient.Receive(EndPoint)
                    Dim message As String = Encoding.ASCII.GetString(data)
                    If message IsNot Nothing Then
                        RemoteIP = EndPoint.Address.ToString
                        RemotePort = EndPoint.Port
                        Dim _tmpremoteIP As String = EndPoint.Address.ToString
                        Task.Factory.StartNew(Sub() RelayDatagram(message, EndPoint, _tmpremoteIP, data)) ', RemoteEndpoint))
                    End If

                    'Debug.Print(("Listener heard: " + message))

                Catch ex As SocketException
                    If (ex.ErrorCode <> 10060) Then
                        Debug.Print(ex.Message)
                    Else
                        Debug.Print("expected timeout error")
                    End If

                End Try

                Thread.Sleep(10)
                ' tune for your situation, can usually be omitted
            End While
            Debug.Print("Ended UDP capture")
            Stopped = True

        Catch ex As Exception
            RaiseEvent sckError(Me, ex)
        End Try
    End Sub

    Private Sub RelayDatagram(dataGram As String, RemoteEndPoint As Net.IPEndPoint, ByVal remoteip As String, Data As Byte())
        Dim handled As Boolean = False

        Try
            RaiseEvent Data_Arrival(Me, dataGram, RemoteEndPoint, remoteip, Data)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    Public Sub SendData(strData As String)
        Dim Data As Byte() = System.Text.Encoding.ASCII.GetBytes(strData)
        If RemoteIP = "" Then
            Throw New System.Exception("RemoteIP address cannot be empty if you want to send data.")
            Exit Sub
        End If
        If RemotePort = 0 Then
            Throw New System.Exception("Remote port cannot be 0 if you want to send data.")
            Exit Sub
        End If
        listenClient.Send(Data, Data.Count, RemoteIP, RemotePort)
    End Sub

    Public Sub SendData(Data As Byte())

        If RemoteIP = "" Then
            Throw New System.Exception("RemoteIP address cannot be empty if you want to send data.")
            Exit Sub
        End If
        If RemotePort = 0 Then
            Throw New System.Exception("Remote port cannot be 0 if you want to send data.")
            Exit Sub
        End If
        listenClient.Send(Data, Data.Count, RemoteIP, RemotePort)
    End Sub

    'Public Sub Dispose()
    '    CleanUp()
    'End Sub

    'Private Sub OnDisable()
    '    CleanUp()
    'End Sub

    ' be certain to catch ALL possibilities of exit in your environment,
    ' or else the thread will typically live on beyond the app quitting.
    Public Sub CleanUp()
        Debug.Print("Cleanup for listener...")
        Abort = True


        If listenClient IsNot Nothing Then
            listenClient.Close()
        End If

        'Debug.Print(",,,,, listen client correctly stopped")
        If listenThread IsNot Nothing Then
            'Give the thread a moment to shut down
            Dim nu As Date = Now
            Do While Stopped = False
                If DateDiff(DateInterval.Second, nu, Now) > 2 Then
                    Exit Do
                End If
            Loop

            listenThread.Abort()
            listenThread.Join(5000)
            listenThread = Nothing
        End If

        ' note, consider carefully that it may not be running

        'Debug.Print(",,,,, listener thread correctly stopped")
    End Sub


#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                CleanUp()
            End If
            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
