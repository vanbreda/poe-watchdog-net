﻿Public Module modPoortResets
    ''' <summary>
    ''' This will connect to a switch and turn PoE off (and if requested back on again) for all switch ports in the Port array
    ''' </summary>
    ''' <param name="IPAdres"></param>
    ''' <param name="PoE_Poort"></param>
    ''' <param name="WeerAanzetten"></param>
    ''' <remarks></remarks>
    Sub ResetPoort(IPAdres As String, PoE_Poort As String(), Optional WeerAanzetten As Boolean = True)

        If IsNothing(IPAdres) Then
            Bug("Er is geen switch IP adres opgegeven. Hier kan ik niets mee.")
            Exit Sub
        End If

        Dim PortString As String = Join(PoE_Poort, ",")

        If PortString = "" Then
            Bug("Er zijn geen poorten te resetten.")
            Exit Sub
        End If

        Bug("Resetting port(s) " & PortString)

        'For een of ander reden komp hier ipadres/kind binnen - de kind moet eraf (dit zou nu opgelost moeten zijn)
        Dim Str() As String
        If InStr(1, IPAdres, "/") Then
            Str = Split(IPAdres, "/")
            IPAdres = Str(0)
        End If

        'Kijk even of de te resetten poort op een CISCO switch zit.
        Dim i%



        ''Kijk of de te resetten poort op een HP switch zit
        'For i = 0 To UBound(POE.Switch)
        '    'Bug "Check switch IP " & POE.Switch(i).IP & " against " & IPAdres
        '    If IPAdres = POE.Switch(i).IP Then
        '        'Bug "Check switch KIND " & POE.Switch(i).KIND & " against " & KIND_SW_CISCO
        '        If POE.Switch(i).KIND = KIND_SW_HP Then

        '            frmPoE.SetTab(3)
        '            Dim InlogNaam As String = frmPoE.txtSwitchInlogNaam.Text
        '            Dim PassWord As String = frmPoE.txtSwitchInlogWachtwoord.Text
        '            Threading.ThreadPool.QueueUserWorkItem(Sub() ResetHPPort(IPAdres, InlogNaam, PassWord, PoE_Poort, True, WeerAanzetten))

        '            'ResetHPPort(IPAdres, frmPoE.txtSwitchInlogNaam.Text, frmPoE.txtSwitchInlogWachtwoord.Text, PoE_Poort, True, WeerAanzetten)
        '            Exit Sub
        '        End If
        '    End If
        'Next i

        For i = 0 To UBound(POE.Switch)
            'Bug "Check switch IP " & POE.Switch(i).IP & " against " & IPAdres
            If IPAdres = POE.Switch(i).IP Then
                'Bug "Check switch KIND " & POE.Switch(i).KIND & " against " & KIND_SW_CISCO
                frmPoE.SetTab(3)

                Dim InlogNaam As String = frmPoE.txtSwitchInlogNaam.Text
                Dim PassWord As String = frmPoE.txtSwitchInlogWachtwoord1.Text
                Dim PassWord2 As String = frmPoE.txtSwitchInlogWachtwoord2.Text

                Select Case POE.Switch(i).KIND
                    Case KIND_SW_CISCO, KIND_SW_CISCO2
                        Threading.ThreadPool.QueueUserWorkItem(Sub() ResetCiscoPort(IPAdres, InlogNaam, PassWord, PassWord2, PoE_Poort, True, WeerAanzetten))
                        Exit Sub
                    Case KIND_SW_HP
                        Threading.ThreadPool.QueueUserWorkItem(Sub() ResetHPPort(IPAdres, InlogNaam, PassWord, PoE_Poort, True, WeerAanzetten))
                        'ResetHPPort(IPAdres, frmPoE.txtSwitchInlogNaam.Text, frmPoE.txtSwitchInlogWachtwoord.Text, PoE_Poort, True, WeerAanzetten)
                        Exit Sub
                    Case KIND_SW_ARUBA
                        Threading.ThreadPool.QueueUserWorkItem(Sub() ArubaPoEPort(InlogNaam, PassWord, IPAdres, PoE_Poort, True, WeerAanzetten))
                        Exit Sub
                End Select
            End If
        Next i

    End Sub
End Module
