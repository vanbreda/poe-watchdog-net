﻿Public Class frmAanpassen


    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click

        With POE.Unit(CInt(Replace(lblKey.Text, "R", "")))
            .IPAddress = txtIP.Text
            .MACaddress = txtMAC.Text.ToUpper
            .NAAM = txtNaam.Text
            .PoE_IP_Address = txtPoE_IP.Text
            .PoE_Poort = txtPoE_Poort.Text


            For Each opt In Controls
                If TypeOf (opt) Is RadioButton Then
                    If opt.Checked Then
                        .KIND = Strings.Right(opt.Name, 1)
                        Exit For
                    End If
                End If

            Next
        End With
        frmPoE.FillListUnits()
        frmPoE.UpdateMap()
        Me.Close()



    End Sub

    Private Sub frmAanpassen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Icon = frmPoE.Icon
    End Sub
End Class