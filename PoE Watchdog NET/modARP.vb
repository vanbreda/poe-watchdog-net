﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.ComponentModel

Public Module modARP

    Private Const MAXLEN_PHYSADDR As Integer = 6

    Private Declare Function GetIpNetTable Lib "Iphlpapi" ( _
      ByVal pIpNetTable As IntPtr, _
      ByRef pdwSize As Integer, ByVal bOrder As Boolean) As Integer

    Private Declare Function SendARP Lib "iphlpapi.dll" _
          (ByVal DestIP As Long, _
           ByVal SrcIP As Long, _
           pMacAddr As Long, _
           PhyAddrLen As Long) As Long

    Const NO_ERROR = 0
    Private Structure MIB_IPNETROW
        Dim dwIndex As Integer
        Dim dwPhysAddrLen As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAXLEN_PHYSADDR)> Dim dwPhysAddr As Byte()
        Dim dwAddr As Integer
        Dim dwStructure As Integer
    End Structure 'MIB_IPNETROW'

    Private Function ConvertMacAddress(ByVal byteArray() As Byte) As String
        Dim builder As New StringBuilder
        For Each byteCurrent As Byte In byteArray
            builder.Append(byteCurrent.ToString("X2")).Append(":"c)
        Next byteCurrent
        builder.Length -= 1
        Return builder.ToString

    End Function 'ConvertMacAddress'


    Public Function GetRemoteMACAddress(ByRef RemoteIPAddress As String) As String
        Const ERROR_INSUFFICIENT_BUFFER As Integer = &H7A

        ' The number of bytes needed.
        Dim bytesNeeded As Integer = 0
        ' The result from the API call.
        Dim result As Integer = GetIpNetTable(IntPtr.Zero, bytesNeeded, False)
        ' Call the function, expecting an insufficient buffer.
        If result <> ERROR_INSUFFICIENT_BUFFER Then
            Throw New Win32Exception(result)
        End If
        ' Allocate the memory, do it in a try/finally block, to ensure that it is released.
        Dim Buffer As IntPtr = IntPtr.Zero
        Try
            ' Allocate the memory.
            Buffer = Marshal.AllocCoTaskMem(bytesNeeded)
            ' Make the call again. If it did not succeed, then raise an error.
            result = GetIpNetTable(Buffer, bytesNeeded, False)
            ' If the result is not 0 (no error), then throw an exception.
            If (result <> 0) Then
                ' Throw an exception.
                Throw New Win32Exception(result)
            Else
                ' Now we have the buffer, we have to marshal it. We can read() the first 4 bytes to get the length of the buffer.
                Dim entries As Integer = Marshal.ReadInt32(Buffer)
                ' Increment the memory pointer by the size of the int.
                Dim currentBuffer As New IntPtr(Buffer.ToInt64() + Marshal.SizeOf(GetType(Integer)))
                ' Allocate an array of entries.
                Dim table(entries) As MIB_IPNETROW
                ' Cycle through the entries.
                For index As Integer = 0 To entries - 1
                    table(index) = CType(Marshal.PtrToStructure(currentBuffer, GetType(MIB_IPNETROW)), MIB_IPNETROW)
                    Dim macAddress As String = ConvertMacAddress(table(index).dwPhysAddr)
                    Dim ipAddress As New System.Net.IPAddress(BitConverter.GetBytes(table(index).dwAddr))
                    'Debug.Print(ipAddress.ToString & " - " & macAddress)

                    If ipAddress.ToString = RemoteIPAddress Then
                        Return macAddress
                    End If
                    currentBuffer = New IntPtr(currentBuffer.ToInt64 + Marshal.SizeOf(GetType(MIB_IPNETROW)))
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            ' Release the memory.
            Marshal.FreeCoTaskMem(Buffer)
        End Try
        Return ""
    End Function 'LoadTableEntries'

    'useage:
    'Dim sRemoteMacAddress$ (wordt via ByRef aangepast)
    'if GetRemoteMACAddress(Text1.Text, sRemoteMacAddress, "-") = true then....
    'Answer = sRemoteMacAddress
    '
    'If the PoE Watchdog and the albireos are not on the same network the sendARP function fails
    'and we then use the chkMACuitDatabase to get the info from the I500 database.

    Public Function FindMAC(IPAdres As String) As String


        Dim i As Int32
        If frmPoE.chkMACuitDatabase.Checked Then
            For i = 0 To UBound(ArpTable)
                If ArpTable(i).IPAdres = IPAdres Then
                    Return ArpTable(i).MAC_Adres
                End If
            Next i
        Else
            If Ping(IPAdres) = True Then
                Dim sRemoteMacAddress As String = GetRemoteMACAddress(IPAdres) ' (wordt via ByRef aangepast)
                If sRemoteMacAddress <> "" Then
                    Return sRemoteMacAddress
                End If
            End If


        End If

        Return ""

    End Function
End Module
