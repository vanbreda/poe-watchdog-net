﻿Module modOUI
    Dim OUI() As String

    Sub GetOUI()
        Dim Str() As String, sOUI As String, Regel() As String, i As Long, FF As Integer
        ReDim OUI(0)
        Dim Bestand$
        Bestand = Application.StartupPath & "\oui.txt"

        If Dir(Bestand) <> "" Then
            FF = FreeFile
            FileOpen(FF, "oui.txt", OpenMode.Binary)
            sOUI = Space(LOF(1))
            FileGet(FF, sOUI)
            Regel = Split(sOUI, vbLf)
            For i = 6 To UBound(Regel)
                Str = Split(Regel(i), vbTab)
                If Regel(i) <> "" Then
                    If InStr(1, Str(0), "-") = 0 And Trim(Left(Str(0), 6)) <> "" Then
                        If InStr(1, Str(2), "samsung", vbTextCompare) <> 0 Then
                            ReDim Preserve OUI(UBound(OUI) + 1)
                            OUI(UBound(OUI)) = Trim(Left(Str(0), 8)) & "ÿ" & Str(2)
                        End If
                    End If
                End If
            Next i
            FileClose(FF)
        Else
            Bug("OUI.TXT niet gevonden, kan niet dedecteren of we met tablets te maken hebben.", , True)
        End If

    End Sub

    Function IsSamsung(Macadres As String) As Boolean
        Dim i As Int16
        Dim S() As String
        For i = 1 To UBound(OUI)
            S = Split(OUI(i), "ÿ")

            If Left(Replace(Macadres, "-", ""), 6) = S(0) Then
                Return True
            End If
        Next i
        Return False
    End Function

End Module
