﻿Module modPoE
    Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

    Public PoESwitchLogonName As String
    Public PoESwitchLogonPassword As String

    Public PacketXInstalled As Boolean
    Public Afsluiten As Boolean

    Public HuidigeAT As Integer 'to keep track of the AT whose name we're modifying
    Structure INIType
        Dim ScreenActive As Integer
        Dim TIMETOLISTEN As Integer
    End Structure

    Structure AlbireoTouchType
        Dim NAAM As String
        Dim IPAdres As String
        Dim INI As INIType
    End Structure

    Structure BestandType
        Dim BestandNaam As String
        Dim Pad As String
    End Structure

    Structure AIP_LIST_Type
        Dim IPAdres As String
        Dim Tijd As Date
        Dim Versie As String
    End Structure
    Public AIP_List() As AIP_LIST_Type

    'Type ATVSType
    '   PoE_IPAdres As String
    '  AT(24) As AlbireoTouchType
    ' VideoServerResetTime As Long 'in seconds
    'optVSPoort As Integer
    'End Type

    'Global ATVS As ATVSType
    'Global VideoResetTime As Long
    Public DontAsk As Boolean

    'OLDPoEType==================================================
    Structure OldUnitType
        Dim NAAM As String
        Dim PoE_IP_Address As String
        Dim PoE_Poort As String
        Dim IPAddress As String
        Dim MACaddress As String
        Dim KIND As Integer
        Dim Key As String
        Dim ScreenActive As Boolean
    End Structure
    '============================================================

    'POE Type
    Structure UnitType
        Dim NAAM As String
        Dim PoE_IP_Address As String
        Dim PoE_Poort As String
        Dim IPAddress As String
        Dim MACaddress As String
        Dim KIND As Integer
        Dim Key As String
        Dim ScreenActive As Boolean
        Dim AttachedMAC As String
        Dim LastSYSLogMSG As String
        Dim SoftwareVersionString As String
    End Structure

    Structure SwitchType
        Dim KIND As Integer
        Dim IP As String
        Dim MAC As String
    End Structure

    Structure PoEType
        Dim Unit() As UnitType
        Dim Switch() As SwitchType
    End Structure

    'OLDPoEType==================================================
    Structure OldPoEType
        Dim Unit() As OldUnitType
        Dim Switch() As SwitchType
    End Structure
    '=============================================

    Public POE As PoEType


    'AIP HOSTAP===================
    Structure AIPtype
        Dim HostAPChannel As Integer
        Dim IPAddress As String
        Dim MACaddress As String
    End Structure
    Public AIP() As AIPtype

    'MAP type==============
    ''' <summary>
    ''' Used to keep track of where a unit is in the List View list. <br></br>This links an Albireos index in the List View
    ''' <br></br>with the Unit's index number in the PoE.Unit(x) structure
    ''' </summary>
    ''' <remarks></remarks>
    Structure MapType
        ''' <summary>
        ''' This is the index of the PoE unit in the PoE Unit UDT
        ''' </summary>
        ''' <remarks></remarks>
        Dim PoEUDTnr As Integer
        ''' <summary>
        ''' This is the index of the PoE Unit in the LV list
        ''' </summary>
        ''' <remarks></remarks>
        Dim LVindex As Integer
    End Structure

    ''' <summary>
    ''' Assuming that all Albireos are in a /16 network, Unit with IP adres x.x.2.34 would be in Map(2,34). 
    ''' </summary>
    ''' <remarks></remarks>
    Public Map(255, 255) As MapType  'a device with 10.23.1.2 resides in MAPAIP(1, 2) - assuming the first two are always the same
    '==============================

    Public ResetBezig As Boolean
    Declare Function GetTickCount Lib "kernel32" () As Long

    Public Const Overige As String = "0"
    Public Const ATVS_Client As Integer = 1
    Public Const ATVS_VideoServer As Integer = 2
    Public Const Albireo_IP As Integer = 3
    Public Const KindDiverse As Integer = 4
    Public Const KindBellatrix As Integer = 5
    Public Const KindEVAapp As Integer = 6
    Public Const KindEVAVideoServer As Integer = 7

    Public Const KIND_SW_3COM As Integer = 1
    Public Const KIND_SW_CISCO As Integer = 2
    Public Const KIND_SW_HP As Integer = 3
    Public Const KIND_SW_CISCO2 As Integer = 4
    Public Const KIND_SW_TPLINK As Integer = 5
    Public Const KIND_SW_ARUBA As Integer = 6

    Public UnitKind(7) As String
    ''' <summary>
    ''' This is the number of swtich kinds we have
    ''' </summary>
    Public SwitchKind(6) As String

    'Public PoEToolTip As New clsToolTip
    Public DoTheMagicBezig As Boolean
    Public gTimeOut As Boolean

    Public AboutFormLoaded As Boolean

    Structure BoundsType
        Dim BeginCijfer As Integer
        Dim EindCijfer As Integer
    End Structure

    Public KindSelectedByChangeKind As Integer 'Als "frmKind" een andere kind kiest, wordt deze variable gebruikt om dit tussen forms te onthouden

    Structure ArpTableType
        Dim IPAdres As String
        Dim MAC_Adres As String
    End Structure

    Public ArpTable() As ArpTableType
    Public Aantalfouten As Long
    'Public SwitchResetActieHP As Boolean 'Voordat een HP switch poort reset wordt, zetten we deze op false. De Reset actie zet hem dan al dan niet op true
    Declare Function Putfocus Lib "user32" Alias "SetFocus" (ByVal hWnd As Long) As Long
    'Deze Public gebruik ik om PoE.Unit info tussen de frmToevoegen en de frmPOE.cmdToevoegen uit te wisselen
    Public ToevoegenNieuwUnitStr As String

    'Declare Function InitCommonControls Lib "comctl32.dll" () As Long
    Function MakePath(Pad As String) As Boolean
        Try
            My.Computer.FileSystem.CreateDirectory(Pad)
            Return True
        Catch ex As Exception
            Bug("Fout tijdens aanmaken " & Pad & ": " & ex.Message, , , True)
            Return False
        End Try


    End Function

    ''' <summary>
    ''' This puts -1 (empty) into all the members of the map, because the LV = 0 based, and we need to know which map members are empty
    ''' </summary>
    ''' <remarks></remarks>
    Sub InitMap()
        Dim i As Integer, ii As Integer
        For i = 0 To 255
            For ii = 0 To 255
                Map(i, ii).LVindex = -1
                Map(i, ii).PoEUDTnr = -1
            Next
        Next
    End Sub
    <DebuggerStepThrough>
    Sub b(Wat)
        If frmPoE.chkDebugNaarDeZaak.Checked Then
            'frmPoE.UDPZaak.SendData Wat
        End If

    End Sub


    Function DeAnsi(Wat As String) As String
        Dim i&, ii&
        Dim Str As String = ""

        For i = 1 To Len(Wat)
            If Asc(Mid(Wat, i, 1)) = 27 Then 'We hebben een escape
                'zoek de eerst volgende letter op
                For ii = i + 1 To Len(Wat)
                    Select Case Mid(Wat, ii, 1)
                        Case "c", "n", "R", "h", "l", "(", ")", "H", "A", "B", "C", "D", "f", "s", "u", "7", "8", "r", "D", "M", "g", "K", "J", "i", "p", "m"
                            If Asc(Mid(Wat, ii, 1)) = Asc("8") Or Asc(Mid(Wat, ii, 1)) = Asc("7") Then
                                If Mid(Wat, i + 1, 1) <> "[" Then
                                    i = ii
                                    Exit For
                                End If
                            Else
                                i = ii
                                Exit For
                            End If
                    End Select
                Next ii
            Else
                Str += Mid(Wat, i, 1)
                'If InStr(1, Str, "continue") <> 0 Then Stop
            End If
        Next i
        DeAnsi = Str
    End Function


    Sub IPReeks(IPAdressSegment As String, NetmaskSegment As String, ByRef Bounds As BoundsType)

        Dim Aantal%
        Select Case CInt(NetmaskSegment)
            Case Is = 255
                Aantal = 1
            Case Is = 254
                Aantal = 2
            Case Is = 253
                Aantal = 4
            Case Is = 248
                Aantal = 8
            Case Is = 240
                Aantal = 16
            Case Is = 224
                Aantal = 32
            Case Is = 192
                Aantal = 64
            Case Is = 128
                Aantal = 128
            Case Else
                Aantal = 128
                MsgBox("Netmask segmenten kunnen 255,254,253,248,240,224,192 of 128 zijn. Ik ga de ingevoerde " & NetmaskSegment & " aanpassing in 128!")
        End Select

        Bounds.BeginCijfer = CInt(IPAdressSegment) - (CInt(IPAdressSegment) Mod Aantal)
        Bounds.EindCijfer = Bounds.BeginCijfer + (Aantal - 1)


    End Sub

    Function IsIPAddress(IPAdres As String) As Boolean
        Dim S() As String, i%

        On Error GoTo Fout
        If Trim(IPAdres) = "" Then
            IsIPAddress = False
            Exit Function
        End If

        S = Split(IPAdres, ".")
        If UBound(S) <> 3 Then
            IsIPAddress = False
            Exit Function
        End If

        For i = 0 To 3
            If CInt(S(i)) > 255 Then
                IsIPAddress = False
                Exit Function
            End If
        Next i
        IsIPAddress = True
        Exit Function

Fout:
        IsIPAddress = False

    End Function

    Sub StartTimeoutTimer(Milliseconds As Long)
        gTimeOut = False
        frmPoE.tmrgTimeOut.Enabled = False
        frmPoE.tmrgTimeOut.Interval = Milliseconds
        frmPoE.tmrgTimeOut.Enabled = True
    End Sub

    Sub StopTimeoutTimer()
        frmPoE.tmrgTimeOut.Enabled = False
    End Sub

    Sub ResetLog(Wat As String)
        Dim Pad$
        Pad = Application.StartupPath
        If Right(Pad, 1) <> "\" Then
            Pad = Pad & "\"
        End If

        HTMLog(Wat, FontStyle.Regular, Color.Red, , , Pad & "logs\PoE Reset", True)
        Bug(Format(Now, "hh:mm") & Wat)
    End Sub

    Function GetPath(PathAndFilename As String) As BestandType
        Dim Teller% ', LaatstBackSlash
        'First find last \
        For Teller = Len(PathAndFilename) To 1 Step -1
            If Mid(PathAndFilename, Teller, 1) = "\" Then Exit For
        Next Teller

        GetPath.BestandNaam = UCase(Mid(PathAndFilename, Teller + 1, Len(PathAndFilename)))
        GetPath.Pad = UCase(Mid(PathAndFilename, 1, Teller))

    End Function

    Function GetIP(Wat As String) As String
        If InStr(1, Wat, "(") = 0 Then
            GetIP = ""
        Else
            GetIP = Trim(Mid(Wat, InStr(1, Wat, "(") + 1, Len(Wat) - InStr(1, Wat, "(") - 1))
        End If
    End Function
    Function IPFormat(IPAdres As String, Optional AIPLIst As Boolean = False)

        '"AIPlist" is the formatting for the AIP list of alive units, we don't want all those spaces in the IP adresses in this list,
        'and usually the 1st three octets are the same, or at least the same length.
        Dim SPLT() As String
        Dim S As String = ""
        If IPAdres <> "" Then
            SPLT = Split(IPAdres, ".")
            If UBound(SPLT) = 3 Then
                If AIPLIst Then
                    S += Format(CInt(SPLT(0)), "0") & "." & Format(CInt(SPLT(1)), "0") & "." & Format(CInt(SPLT(2)), "0") & "." & Format(CInt(SPLT(3)), "000")
                Else
                    S += Format(CInt(SPLT(0)), "000") & "." & Format(CInt(SPLT(1)), "000") & "." & Format(CInt(SPLT(2)), "000") & "." & Format(CInt(SPLT(3)), "000")
                End If
            End If
        End If

        Return S
    End Function
    ''' <summary>
    ''' Split the IPAdres string (.), return the "<paramref name="WelkeOctet"/> array member
    ''' </summary>
    ''' <param name="IPAdres">IPAdres as dotted string</param>
    ''' <param name="WelkeOctet">1,2,3 or 4</param>
    ''' <returns></returns>
    Function GetOctetFromIPAdres(IPAdres As String, WelkeOctet As Integer) As Integer

        If IPAdres <> "" Then
            'Dit haalt een octet (1 t/m 4) uit het IP adres en geeft het als int terug. Voor de MAP(x,x)
            Dim S() As String
            S = Split(IPAdres, ".")
            Return S(WelkeOctet - 1) 'WelkeOctet is 1 t/m 4, de Split maakt 0 t/m 3 - van daar de "-1"
        Else
            Return 0
        End If

    End Function

End Module
