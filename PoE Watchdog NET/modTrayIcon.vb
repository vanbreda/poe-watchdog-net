﻿Public Module modNotifyIcon

    Dim WithEvents TI As New NotifyIcon
    Public Sub LoadNotifyIcon()
        TI.ContextMenuStrip = frmNotifyMenu.ContextMenuStrip1
        TI.Icon = frmToevoegen.Icon
        TI.Text = "PoE Watchdog NET"
        TI.Visible = True
    End Sub

    Public Sub UnloadNotifyIcon()
        TI.Dispose()
    End Sub

    Private Sub TI_Click(sender As Object, e As EventArgs) Handles TI.Click
        frmPoE.Show()
    End Sub

End Module
