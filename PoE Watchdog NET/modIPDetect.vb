﻿Imports System.Net
Module modIPDetect

    Public Sub FindMyIP(ByRef Addresses() As String)
        ReDim  Addresses(0)
        Dim HostName As String = System.Net.Dns.GetHostName()

        For Each adapter As Net.NetworkInformation.NetworkInterface In Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
            'Debug.Print(adapter.Description)
            If adapter.OperationalStatus = NetworkInformation.OperationalStatus.Up Then
                If InStr(adapter.Description, "Hyper-V", CompareMethod.Text) = 0 Then 'we don't want adapter mirror devices
                    If adapter.NetworkInterfaceType = NetworkInformation.NetworkInterfaceType.Ethernet Then
                        For Each unicastIPAddressInformation As Net.NetworkInformation.UnicastIPAddressInformation In adapter.GetIPProperties().UnicastAddresses
                            If unicastIPAddressInformation.Address.AddressFamily = Net.Sockets.AddressFamily.InterNetwork Then
                                Debug.Print(adapter.Description & " - " & unicastIPAddressInformation.Address.ToString & "/" & unicastIPAddressInformation.IPv4Mask.ToString())

                                Addresses(Addresses.Length - 1) = unicastIPAddressInformation.Address.ToString & " / " & unicastIPAddressInformation.IPv4Mask.ToString()
                                ReDim Preserve Addresses(Addresses.Length)

                            End If
                        Next
                    End If

                End If

            End If

        Next
        ReDim Preserve Addresses(Addresses.Length - 2)
    End Sub

End Module
