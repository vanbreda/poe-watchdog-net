﻿Imports SnmpSharpNet
Module modSNMP
    'Public ReqSNMP As Byte()

    Function CreateGetRequest() As Byte()

        'Dim requestOid() As String
        'Dim result As Dictionary(Of Oid, AsnType)
        'requestOid = New String() {"1.3.6.1.2.1.1.1.0", "1.3.6.1.2.1.1.2.0"}
        'Dim snmp As SimpleSnmp = New SimpleSnmp("127.0.0.1", 45030, frmPoE.txtSNMPCommunity.Text, 50, 0)

        'If Not snmp.Valid Then
        '    Console.WriteLine("Invalid hostname/community.")
        '    Exit Function
        'End If
        'result = snmp.Get(SnmpVersion.Ver1, requestOid)
        'If result IsNot Nothing Then
        '    Dim kvp As KeyValuePair(Of Oid, AsnType)
        '    For Each kvp In result
        '        Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
        '                          SnmpConstants.GetTypeName(kvp.Value.Type), _
        '                          kvp.Value.ToString())
        '    Next
        'Else
        '    Console.WriteLine("No results received.")
        'End If



        Dim tmpReqSNMP As Byte()
        Dim CommunityString As Byte() = System.Text.Encoding.ASCII.GetBytes(frmPoE.txtSNMPCommunity.Text)
        Dim CommunityStringLength As Integer = CommunityString.Length
        Dim i As Integer
        ReDim tmpReqSNMP(7 + CommunityStringLength + 27)

        tmpReqSNMP(0) = &H30 'ASN.1
        tmpReqSNMP(1) = tmpReqSNMP.Length - 2 'Length PDU
        tmpReqSNMP(2) = &H2    'version
        tmpReqSNMP(3) = &H1    'version
        tmpReqSNMP(4) = &H0    'version
        tmpReqSNMP(5) = &H4    'com name
        tmpReqSNMP(6) = CommunityStringLength
        For i = 0 To CommunityString.Length - 1
            tmpReqSNMP(7 + i) = CommunityString(i)
        Next
        'Get Request
        tmpReqSNMP(7 + CommunityStringLength + 0) = &HA0
        tmpReqSNMP(7 + CommunityStringLength + 1) = &H1A
        'REquest ID
        tmpReqSNMP(7 + CommunityStringLength + 2) = &H2
        tmpReqSNMP(7 + CommunityStringLength + 3) = &H2
        tmpReqSNMP(7 + CommunityStringLength + 4) = &H19
        'Error Status
        tmpReqSNMP(7 + CommunityStringLength + 5) = &HCB
        tmpReqSNMP(7 + CommunityStringLength + 6) = &H2
        tmpReqSNMP(7 + CommunityStringLength + 7) = &H1
        'Index
        tmpReqSNMP(7 + CommunityStringLength + 8) = &H0
        tmpReqSNMP(7 + CommunityStringLength + 9) = &H2
        tmpReqSNMP(7 + CommunityStringLength + 10) = &H1
        tmpReqSNMP(7 + CommunityStringLength + 11) = &H0

        tmpReqSNMP(7 + CommunityStringLength + 12) = &H30
        tmpReqSNMP(7 + CommunityStringLength + 13) = &HE
        tmpReqSNMP(7 + CommunityStringLength + 14) = &H30
        tmpReqSNMP(7 + CommunityStringLength + 15) = &HC
        tmpReqSNMP(7 + CommunityStringLength + 16) = &H6
        tmpReqSNMP(7 + CommunityStringLength + 17) = &H8
        tmpReqSNMP(7 + CommunityStringLength + 18) = &H2B
        tmpReqSNMP(7 + CommunityStringLength + 19) = &H6
        tmpReqSNMP(7 + CommunityStringLength + 20) = &H1
        tmpReqSNMP(7 + CommunityStringLength + 21) = &H2
        tmpReqSNMP(7 + CommunityStringLength + 22) = &H1
        tmpReqSNMP(7 + CommunityStringLength + 23) = &H1
        tmpReqSNMP(7 + CommunityStringLength + 24) = &H5
        tmpReqSNMP(7 + CommunityStringLength + 25) = &H0
        tmpReqSNMP(7 + CommunityStringLength + 26) = &H5
        tmpReqSNMP(7 + CommunityStringLength + 27) = &H0


        Dim ReqSNMP2 As Byte() = {&H30, &H37, &H2, &H1, &H0, &H4, _
                            &H6, &H70, &H75, &H62, &H6C, &H69, &H63, &HA0, _
                            &H2A, &H2, &H4, &H12, &H5E, &HC8, &H19, &H2, _
                            &H1, &H0, &H2, &H1, &H0, &H30, &H1C, &H30, _
                            &HC, &H6, &H8, &H2B, &H6, &H1, &H2, &H1, _
                            &H1, &H1, &H0, &H5, &H0, &H30, &HC, &H6, _
                            &H8, &H2B, &H6, &H1, &H2, &H1, &H1, &H2, _
                            &H0, &H5, &H0}



        Dim packet_bytes As Byte() = {&H30, &H3A, &H2, &H1, &H0, &H4, &H19, &H69,
                                      &H50, &H76, &H67, &H64, &H4A, &H6E, &H36, &H42,
                                      &H34, &H64, &H36, &H6D, &H4B, &H32, &H76, &H57,
                                      &H49, &H54, &H6D, &H6C, &H75, &H70, &H73, &H78,
                                      &HA0, &H1A, &H2, &H2, &H3E, &H5C, &H2, &H1,
                                      &H0, &H2, &H1, &H0, &H30, &HE, &H30, &HC,
                                      &H6, &H8, &H2B, &H6, &H1, &H2, &H1, &H1,
                                      &H5, &H0, &H5, &H0}






        'Dim S As String = ""
        'For i = 0 To tmpReqSNMP.Count - 1
        '    S += Hex(tmpReqSNMP(i)) & " "
        'Next
        'Debug.Print(S)
        'S = ""
        'For i = 0 To ReqSNMP2.Count - 1
        '    S += ReqSNMP2(i) & " "
        'Next
        'Debug.Print(S)
        

        Return tmpReqSNMP
    End Function


    Function GetVersion(Host As String) As String
        Dim requestOid() As String
        Dim result As Dictionary(Of Oid, AsnType)
        Dim ReturnStr As String = ""

        requestOid = New String() {"1.3.6.1.2.1.1.1.0", "1.3.6.1.2.1.1.2.0"}
        Dim snmp As SimpleSnmp = New SimpleSnmp(Host, "public")
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Return ""
        End If

        result = snmp.Get(SnmpVersion.Ver1, requestOid)

        If result IsNot Nothing Then
            Dim kvp As KeyValuePair(Of Oid, AsnType)
            For Each kvp In result
                Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                  SnmpConstants.GetTypeName(kvp.Value.Type), _
                                  kvp.Value.ToString())

                ReturnStr += kvp.Value.ToString
            Next
            Return ReturnStr
        Else
            Return ""
        End If

    End Function

    Function GetHPInfoSNMP(host As String, community As String, OIDText As String) As String
        ' Dictionary to store values returned by GetBulk calls
        Dim result As Dictionary(Of Oid, AsnType)


        'Dim Allresult As New Dictionary(Of Oid, AsnType)
        Dim Allresult As New List(Of KeyValuePair(Of Oid, AsnType))


        ' Root Oid for the request (ifTable.ifEntry.ifDescr in this example)
        'Dim rootOid As Oid = New Oid("1.3.6.1.2.1.2.2.1.2")


        Dim rootOid As Oid = New Oid(OIDText)


        Dim nextOid As Oid = rootOid
        Dim keepGoing As Boolean = True
        ' Initialize SimpleSnmp class
        Dim snmp As SimpleSnmp = New SimpleSnmp(host, community)
        If Not snmp.Valid Then
            Console.WriteLine("Invalid hostname/community.")
            Return ""
        End If
        ' Set NonRepeaters and MaxRepetitions for the GetBulk request (optional)
        snmp.Timeout = 500
        snmp.NonRepeaters = 0
        snmp.MaxRepetitions = 20000

        Dim SB As New System.Text.StringBuilder

        While keepGoing
            ' Make a request
            result = snmp.GetBulk(New String() {nextOid.ToString()})

            ' Check SNMP agent returned valid results
            If result IsNot Nothing Then
                Dim kvp As KeyValuePair(Of Oid, AsnType)
                ' Loop through returned values
                For Each kvp In result
                    ' Check that returned Oid is part of the original root Oid
                    If rootOid.IsRootOf(kvp.Key) Then
                        Console.WriteLine("{0}: ({1}) {2}", kvp.Key.ToString(), _
                                              SnmpConstants.GetTypeName(kvp.Value.Type), _
                                              kvp.Value.ToString())

                        SB.AppendLine(kvp.Key.ToString() & ";" & kvp.Value.ToString())

                        ''''''''RTF.AppendText(String.Format("{0}: ({1}) {2}", kvp.Key.ToString(), SnmpConstants.GetTypeName(kvp.Value.Type), kvp.Value.ToString()) & vbCrLf)

                        '''''''''Application.DoEvents()
                        ' Store last valid Oid to use in additional GetBulk requests (if required)

                        'RTF.AppendText(kvp.Key.ToString() & " - " & SnmpConstants.GetTypeName(kvp.Value.Type) & " " & kvp.Value.ToString() & vbCrLf)
                        nextOid = kvp.Key
                        Allresult.Add(kvp)
                    Else
                        ' We found a value outside of the root Oid tree. Do not perform additional GetBulk ops
                        keepGoing = False
                    End If
                Next
            Else
                Console.WriteLine("No results received.")
                keepGoing = False
            End If
            Application.DoEvents()
        End While

        Return SB.ToString

    End Function
End Module
