﻿Public Class clsMyInputBox
    Inherits frmInputBox
    Property Cancel As Boolean
    Private Property OkPressed As Boolean = False
    Private Property CancelPressed As Boolean = False

    Function ShowModal(text As String, Caption As String) As String
        Me.Label1.Text = text
        Me.Text = Caption
        Me.CenterToParent()
        Me.ShowDialog()
        If CancelPressed Then
            Return ""
        Else
            Return Me.TextBox1.Text
        End If

    End Function

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        OkPressed = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        CancelPressed = True
        Me.Close()
    End Sub

End Class
