﻿Namespace My
    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.

    'This 'ApplicationEvents.vb' file is autogenerated when you click on the "View application events" button next to the "Single instance"  checkbox in the projects properties
    Partial Friend Class MyApplication
        Sub a() Handles Me.StartupNextInstance
            frmPoE.WindowState = FormWindowState.Normal
            frmPoE.Visible = True
        End Sub

        Private Sub MyApplication_Shutdown(sender As Object, e As EventArgs) Handles Me.Shutdown
            'frmPoE.NotifyIcon1.Dispose()
        End Sub
    End Class


End Namespace

