﻿Module modOverrideNow

    ''' <summary>
    ''' This overrides the standard Now function to return UTC (to prevent DST issues)
    ''' </summary>
    ''' <returns></returns>
    <DebuggerStepThrough>
    Public Function Now() As Date
        Return Microsoft.VisualBasic.DateAndTime.Now.ToUniversalTime
    End Function
End Module
