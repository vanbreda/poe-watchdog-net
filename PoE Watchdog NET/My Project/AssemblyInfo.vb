﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PoE Watchdog NET")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Van Breda B.V.")> 
<Assembly: AssemblyProduct("PoE Watchdog NET")> 
<Assembly: AssemblyCopyright("Copyright © Van Breda B.V. 2021")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("286545c2-4898-456c-a28f-0ebb339544c9")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("10.2.1.*")> 
'Version numbers are now controlled in the AutoVersionIncrease sub
