﻿Public Module modVersion
    ''' <summary>
    ''' A simpler wat to show only the Major, Minor and Build of the version in dotted notation.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Ver() As String
        Dim S As String() = Split(Application.ProductVersion, ".")
        Return S(0) & "." & S(1) & "." & S(2)
    End Function
End Module
