﻿'Imports System
'Imports System.Collections.Generic
'Imports PcapDotNet.Core
'Imports PcapDotNet.Packets
'Imports PcapDotNet.Packets.IpV4
'Imports PcapDotNet.Packets.Transport




'Public Module ProgramTest

'    Sub TestCap2()
'        Dim allDevices As IList(Of LivePacketDevice) = LivePacketDevice.AllLocalMachine

'        If allDevices.Count = 0 Then
'            Console.WriteLine("No interfaces found! Make sure WinPcap is installed.")
'            Return
'        End If

'        Dim i As Integer = 0

'        While i <> allDevices.Count
'            Dim device As LivePacketDevice = allDevices(i)
'            Console.Write((i + 1) & ". " & device.Name)

'            If device.Description IsNot Nothing Then
'                Console.WriteLine(" (" & device.Description & ")")
'            Else
'                Console.WriteLine(" (No description available)")
'            End If

'            i += 1
'        End While

'        Dim selectedDevice As PacketDevice = allDevices(1)

'        Using communicator As PacketCommunicator = selectedDevice.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000)
'            Console.WriteLine("Listening on " & selectedDevice.Description & "...")
'            communicator.SetFilter("dst port 67 or 68")
'            Dim packet As Packet

'            Do
'                Dim result As PacketCommunicatorReceiveResult = communicator.ReceivePacket(packet)

'                Select Case result
'                    Case PacketCommunicatorReceiveResult.Timeout
'                        Continue Do
'                    Case PacketCommunicatorReceiveResult.Ok
'                        Console.WriteLine(packet.Timestamp.ToString("yyyy-MM-dd hh:mm:ss.fff") & " length:" + packet.Length.ToString)
'                    Case Else
'                        Throw New InvalidOperationException("The result " & result & " should never be reached here")
'                End Select
'            Loop While True
'        End Using
'    End Sub


'    Sub TestCap()
'        Dim allDevices As IList(Of LivePacketDevice) = LivePacketDevice.AllLocalMachine

'        If allDevices.Count = 0 Then
'            Console.WriteLine("No interfaces found! Make sure WinPcap is installed.")
'            Return
'        End If

'        Dim i As Integer = 0

'        While i <> allDevices.Count
'            Dim device As LivePacketDevice = allDevices(i)
'            Console.Write((i + 1) & ". " & device.Name)

'            If device.Description IsNot Nothing Then
'                Console.WriteLine(" (" & device.Description & ")")
'            Else
'                Console.WriteLine(" (No description available)")
'            End If

'            i += 1
'        End While

'        Dim deviceIndex As Integer = 0

'        Do
'            Console.WriteLine("Enter the interface number (1-" & allDevices.Count & "):")
'            Dim deviceIndexString As String = Console.ReadLine()

'            If Not Integer.TryParse(deviceIndexString, deviceIndex) OrElse deviceIndex < 1 OrElse deviceIndex > allDevices.Count Then
'                deviceIndex = 0
'            End If
'        Loop While deviceIndex = 0

'        Dim selectedDevice As PacketDevice = allDevices(deviceIndex - 1)

'        Using communicator As PacketCommunicator = selectedDevice.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000)
'            Console.WriteLine("Listening on " & selectedDevice.Description & "...")
'            communicator.ReceivePackets(0, AddressOf PacketHandler)
'        End Using
'    End Sub

'    Private Sub PacketHandler(ByVal packet As Packet)
'        Console.WriteLine(packet.Timestamp.ToString("yyyy-MM-dd hh:mm:ss.fff") & " length:" + packet.Length.ToString)
'    End Sub
'End Module

