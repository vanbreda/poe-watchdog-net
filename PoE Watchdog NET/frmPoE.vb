﻿Imports PcapDotNet.Core
Imports PcapDotNet.Packets
Imports PcapDotNet.Packets.IpV4
Imports PcapDotNet.Packets.Transport
Imports Renci.SshNet.Security.Org.BouncyCastle.Math.EC
Imports SnmpSharpNet
Imports System.Dynamic
Imports System.IO
Imports System.Security.Permissions
Imports System.Web.ModelBinding

Public Class frmPoE
    Shared WithEvents RTF As New Windows.Forms.RichTextBox
    'We might need this if the designer thows another fit.
    Friend WithEvents lvUnits As ffListView



#Region "DeclareSection"

    Dim Chapter As String, Init As Boolean, Bezig As Boolean
    Dim ChapterCisco As String
    Dim Ch(100) As String, TCP_SWstr(100) As String, POORTINFO(100) As String 'voor de multi TCP lees actie
    Dim TCP_str_CISCO As String
    Dim TCP_str_HP As String
    Dim TCP_str_TPLink As String

    Dim TCPStr$
    Dim PoEPoort As Integer, chkPrevState(24) As Integer 'Als chkPrevState(11) = True then.....
    Dim glbWelkePoort As Integer
    Dim ResetAlbireo(24) As Integer, ResetVideoServer As Integer
    Dim MSCurrentFile As String
    Dim ReqPoESNMP As String


    Dim AbortPoEDetect As Boolean
    Dim AbortIPReoDetect As Boolean
    Dim HuidigeRequest%
    Dim SwitchToAsk%
    Dim AIPTimeOut As Integer
    Dim AnaliseStr(30) As String
    Dim HoeveelsteKeer As Integer 'houdt bij hoe vaak we door de switchlist gaan bij eenzelfde IP adres (eindeloos lus
    'tegen gaan bij een unit die daadwerkelijk niet op een poe poort zit.)
    Dim VorigeTab As Integer

    ' Constants voor PacketX WinPCap
    Const PktXPacketTypePromiscuous = &H20
    Const PktXLinkType802_3 = 1
    Const PktXLinkType802_5 = 2
    Const PktXLinkTypeFddi = 3
    Const PktXLinkTypeWan = 4
    Const PktXLinkTypeLocalTalk = 5
    Const PktXLinkTypeDix = 6
    Const PktXLinkTypeArcnetRaw = 7
    Const PktXLinkTypeArcnet878_2 = 8
    Const PktXLinkTypeAtm = 9
    Const PktXLinkTypeWirelessWan = 10
    Const PktXModeCapture = 1

    Dim gTFTPStr As String, TransferComplete As Boolean

    Dim UDPATVS_IPAdres$
    Dim UDPATVSAan As Boolean, UDPATVS_Status%
    Dim EvaLog$
    Dim ResetTimerDelay As Integer, ResetDelayMaxTime As Integer

    Dim EVAMAC() As String
    Dim StartingUp As Boolean
    Dim BusyUpdatingAIPList As Boolean

    ''' <summary>
    ''' When you rightclick with shift on one of the tabs this will be set to True and the app.will close discriminating between the apps "X" box and ShiftRightClick
    ''' </summary>
    ''' <remarks></remarks>
    Dim ShiftRightClick As Boolean = False

    Public WinPCapAdapter As Int16
    ''' <summary>
    ''' Public list of adapters that can be used by the capture thread. Populated by the GetNetworkAdapters thread.
    ''' </summary>
    ''' <remarks></remarks>
    Public allNetworkDevices As IList(Of LivePacketDevice)

    ''' <summary>
    ''' We can oly start capturing packets if the allNetworkDevices list is initialised, the capture thread waits for this, the GetNetworkAdapters thread initialsed it.
    ''' </summary>
    ''' <remarks></remarks>
    Public allNetworkDevicesInitialised As Boolean

    ''' <summary>
    ''' Declare the UDPaip need to receive pakkets from Albireo's
    ''' </summary>
    ''' <remarks></remarks>
    Dim WithEvents UDPaip As New clsUDP(993)

    ''' <summary>
    '''This port is used to send the SNMP discover request to all network devices 
    ''' </summary>
    Dim WithEvents UDP161 As New clsUDP()

    ''' <summary>
    ''' This is only used to send "FindAIP" messages to Albireos, the Albireo's always sends it's answers to the INEX poort 45200, so we capture these using the WinPCap
    ''' </summary>
    ''' <remarks></remarks>
    Dim WithEvents UDPFindAIP As New clsUDP(997)

    Dim StopCapture As Boolean = False


    ''' <summary>
    ''' If the button is presses to disable auto restarts voor x minutes, this timer will be set to the value of x minutes and count down to 0, where autorestart will be enabled
    ''' </summary>
    ''' <remarks></remarks>
    Dim DisableAutorestartTime As Int32

    '''' <summary>
    '''' This is filled the first time an entry is written to the logfile, and is used when right click on the debug button
    '''' to show the log file in te default browser.03
    '''' 
    '''' </summary>
    'Dim LogFile As String

#End Region

    ''' <summary>
    ''' This adds the RTF as SHARED so than it can be accessed by diferent threads to show SSH debug info. Try this in the designer and it keeps getting set to Friend ;)
    ''' Also adds the lvUnits as doublebuffered ListView. The designer didn't like it when I put it there.
    ''' </summary>
    ''' <remarks></remarks>
    Sub frmInit()
        Me.tbDebug.Controls.Add(frmPoE.RTF)
        frmPoE.RTF.Location = New System.Drawing.Point(0, 38)
        frmPoE.RTF.Name = "RTF"
        frmPoE.RTF.Size = New System.Drawing.Size(Me.TabControl1.Width - 5, 546)
        frmPoE.RTF.TabIndex = 8
        frmPoE.RTF.ScrollBars = RichTextBoxScrollBars.Both
        frmPoE.RTF.Text = ""
        frmPoE.RTF.Font = New Font(Me.Font.FontFamily, 11, FontStyle.Regular)
        frmPoE.RTF.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)


        Me.lvUnits = New ffListView()
        Me.tbUnits.Controls.Add(Me.lvUnits)
        '
        Me.lvUnits.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvUnits.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvUnits.ContextMenuStrip = Me.mnuAIP
        Me.lvUnits.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvUnits.HideSelection = False
        Me.lvUnits.Location = New System.Drawing.Point(3, 39)
        Me.lvUnits.Name = "lvUnits"
        Me.lvUnits.Size = New System.Drawing.Size(976, 551)
        Me.lvUnits.Sorting = System.Windows.Forms.SortOrder.Descending
        Me.lvUnits.TabIndex = 0
        Me.lvUnits.UseCompatibleStateImageBehavior = False

    End Sub

    'Public Sub Bug(Wat As String)
    '    'RTF.SelectedText = Wat

    '    If Me.txt.InvokeRequired Then
    '        Dim Args As String() = {Wat}
    '        Me.txt.Invoke(New Action(Of String)(AddressOf Bug), Args)
    '        Return
    '    End If
    '    txt.AppendText(Wat & vbCrLf)
    '    Me.Text = Wat
    '    Debug.Print(Wat)

    'End Sub

    Sub lstAdaptersAdd(Wat As String)
        If Me.lstAdapters.InvokeRequired Then
            Dim Args As String() = {Wat}
            Me.lstAdapters.Invoke(New Action(Of String)(AddressOf lstAdaptersAdd), Args)
            Return
        End If

        lstAdapters.Items.Add(Wat)

    End Sub

    Private Sub frmPoE_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Bug("PoE Watchdog NET (SSH) afgesloten!")
        frmAbout.Close()
        UnloadNotifyIcon()
        'NotifyIcon1.Visible = False
        'NotifyIcon1.Dispose()
        AutoVersionIncrease()

    End Sub

    Private Sub frmPoE_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Bug("QueryUnload called, UnloadMode = " & e.CloseReason & ", cancel is " & e.Cancel & " and ShiftRightClick = " & ShiftRightClick)
        Dim ReallyShutDown As Boolean = False

        SaveCFGFile(True)
        AllSettings(True)

        If e.CloseReason = CloseReason.UserClosing Then 'Both X and ShiftRightClick
            If ShiftRightClick = True Then
                ReallyShutDown = True
            End If
        Else 'Close request came from a different source (windows reboot, kill task etc.)

        End If

        If ReallyShutDown Then
            frmAbout.Show()
            Bug("Afsluiten - dit duurt eventjes....")
            UDP161.Abort = True
            UDPaip.Abort = True
            UDPZaak.Abort = True
            UDPFindAIP.Abort = True
            Bug("stopcapture")
            StopCapture = True
            Bug("tmrAdd disable")
            tmrAIPlstADD.Enabled = False
            Bug("DoEvents")
            Application.DoEvents()


            Bug("161 dispose")
            UDP161.Dispose()
            Bug("aip dispose")
            UDPaip.Dispose()
            Bug("udp zaak dispose")
            UDPZaak.Dispose()
            'Bug("Findaip dispose")
            UDPFindAIP.Dispose()


        Else
            Me.Visible = False
            e.Cancel = True
            'Me.WindowState = FormWindowState.Minimized

        End If

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Put the load logic in a seperate sub so as to be able to delay this, trying to find the
        'issue where the forms init reruns at runtime. Fixed it differently.

        FormLoadDelayed()
    End Sub


    Sub FormLoadDelayed()
        'Aruba_6000_API.GetVersion("10.26.1.165")
        Bug($"Starting PoE Watchdog.NET version {Application.ProductVersion}.")
        InitMSK()

        ToolTip1.SetToolTip(btnTab3, "Toont de debug scherm. Rechtermuis toont het logbestand in de browser.")
        ToolTip1.SetToolTip(chkDebugNaarDeZaak, "Debug info naar poort 44445 sturen.")

        ReDim lvUnitsColumns(9)
        frmInit()
        If Environ("COMPUTERNAME") <> "WS-000040" Then
            Button1.Visible = False
        End If

        For Each t As TabPage In TabControl1.TabPages
            t.Text = ""
            t.Height = 0
        Next
        TabControl1.ItemSize = New Size(20, 1)

        Me.lblInfo.Text = ""
        'Exit Sub
        AllSettings(False)
        Me.Width = GetSetting("PoE-Net", "", "frmPoE.Width", 1147)
        Me.Height = GetSetting("PoE-Net", "", "frmPoE.Height", 698)

        If txtLogPath.Text = "" Then
            txtLogPath.Text = Application.StartupPath & "\Logs"
        End If


        'Start thDebugUDP incase we're senden data to the debugger
        UDPZaak._LocalPort = 45030
        UDPZaak.Start()


        'ShowInTaakBalk Me.hWnd, False
        StartingUp = True
        frmAbout.Show()
        frmAbout.Refresh()
        'DoEvents

        If PreviouseInstance() Then
            MsgBox("PoE Watchdog moet je activeren met het ikoon In de taakbalk.")
            End
        End If

        ReDim AIP_List(0)

        lblAantalFouten.Text = ""

        Bug("Oude logbestanden opruimen...")

        CleanupOldLogs()

        'This version doesn't do tablets, so we also dont need the OUI
        'Bug("OUI info ophalen...")
        'GetOUI()

        '
        lblStatus2.Text = "PoE Watchdog - Versie " & Ver()
        'INIT





        UnitKind(KindDiverse) = "DIV"
        UnitKind(ATVS_Client) = "SCH"
        UnitKind(ATVS_VideoServer) = "SRV"
        UnitKind(Albireo_IP) = "AIP"
        UnitKind(KindBellatrix) = "BTX"
        UnitKind(KindEVAapp) = "EVA"
        UnitKind(KindEVAVideoServer) = "EVS"

        SwitchKind(KIND_SW_3COM) = "3 Com switch"
        SwitchKind(KIND_SW_CISCO) = "Cisco switch"
        SwitchKind(KIND_SW_CISCO2) = "Cisco (2) switch"
        SwitchKind(KIND_SW_HP) = "HP switch"
        SwitchKind(KIND_SW_TPLINK) = "TP link switch"
        SwitchKind(KIND_SW_ARUBA) = "Aruba switch"

        'instellingen ophalen=========================================
        ResetDelayMaxTime = 20

        'Debug info naar de zaak voor peter.



        If Dir("C:\VBZ\EVAlog", vbDirectory) = "" Then
            MkDir("C:\VBZ\EVAlog")
        End If

        If GetSetting("PoE-NET", "Settings", "AIPTimeout", "") = "" Then
            SaveSetting("PoE-NET", "Settings", "AIPTimeout", "120")
            AIPTimeOut = 120
        Else
            AIPTimeOut = GetSetting("PoE-NET", "Settings", "AIPTimeout")
        End If

        'Dim Teller As Long, Offset As Integer, Links As Integer
        'Offset = 300
        'Links = 4530

        '==================Open de units config waar de unit informatie in zit
        'Eerst kijken of er een upgrade nodig is
        If Dir(Application.StartupPath & "\PoE-NET.cfg") <> "" Then
            FileOpen(1, Application.StartupPath & "\PoE-NET.cfg", OpenMode.Binary)
            FileGet(1, POE)
            FileClose(1)
            SetTab(0)
        Else
            ReDim POE.Unit(0)
            ReDim POE.Switch(0)
            SetTab(2)
        End If


        '==================================Load the Units in de List View
        Try
            If UBound(POE.Unit) >= 0 Then
                FillListUnits()
            End If
        Catch ex As Exception
            Bug(Err.Description & " tijdens het laden van de PoE.Unit lijst (uit poe.cfg). Dit bestand is corrupt. Zet een backup terug of doe de magic opnieuw.", , True)
            ReDim POE.Unit(0)
            ReDim POE.Switch(0)
        End Try

        '===========================================Load PoE IP addresses
        For Teller = 0 To UBound(POE.Switch)
            If POE.Switch(Teller).IP <> "" Then lstPoESwitches.Items.Add(POE.Switch(Teller).IP & Space(500) & "/" & POE.Switch(Teller).KIND)
        Next Teller

        '=======================================Center Screen
        Me.CenterToScreen()

        frmAbout.Close()

        InitEnzi()
        UpdateMap()
        'SetTab 6

        lblSelectedNetworkAdapterText("Netwerkadapters zoeken")

        StartingUp = False
        tmrDelayBinding.Enabled = True

        'Startup the differnt listerners / threads 

        WinPCapAdapter = GetSetting("PoE-NET", "Settings", "WinPcapAdapter", 0)

        'This get all the available adapters
        Threading.ThreadPool.QueueUserWorkItem(AddressOf GetNetworkAdapterList)


        StartPCap() 'We also want to start WinPCap when changing network adapters in the list, that's whuy it's on its own sub 

        Bug("Start the UDP listeners")

        'Start listening for alive messages
        Try
            UDPaip.Start()
        Catch ex As Exception
            Bug("Fout tijden opstarten UDPaip: " & ex.Message)

        End Try

        tmrAIPlstADD.Enabled = True

        SetColor(btnTab2)
        SetTab(3)
        Me.TabControl1.Location = New Point(142, -4)

        LoadNotifyIcon()

        'AddToAIPList(" Alive H:8 B:1.7 L:4.5.2.0 A:4.6.0.0", "10.26.2.61")
        'AddToAIPList(" Alive H:8 B:1.7 L:4.5.2.0 A:4.6.0.0", "10.26.2.62")
        'AddToAIPList(" Alive H:8 B:1.7 L:4.5.2.0 A:4.6.0.0", "10.26.2.63")
        'AddToAIPList(" Alive H:8 B:1.7 L:4.5.2.0 A:4.6.0.0", "10.26.2.61")

        'txtSNMPCommunity.Text = "iPvgdJn6B4d6mK2vWITmlupsx"
    End Sub
    Sub FillListUnits()
        Try
            Dim Teller%
            GetLVUnitsColumns()
            'The first time POE Watchdog is started there wil be no values in the settings file, leaving lvUnitsColumns(0) 0
            If lvUnitsColumns(0) = 0 Then
                lvUnitsColumns(0) = 1580 \ 15
                lvUnitsColumns(1) = 2250 \ 15
                lvUnitsColumns(2) = 1560 \ 15
                lvUnitsColumns(3) = 700 \ 15
                lvUnitsColumns(4) = 700 \ 15
                lvUnitsColumns(5) = 4500 \ 15
                lvUnitsColumns(6) = 2000 \ 15
                lvUnitsColumns(7) = 2000 \ 15
                lvUnitsColumns(8) = 2000 \ 15
                lvUnitsColumns(9) = 0 \ 15
            End If

            With lvUnits
                If lvUnits.Columns.Count = 0 Then

                    lvUnits.Columns.Add("IP Unit", lvUnitsColumns(0), HorizontalAlignment.Left) '0
                    lvUnits.Columns.Add("MAC", lvUnitsColumns(1))        '1
                    lvUnits.Columns.Add("PoE IP", lvUnitsColumns(2))      '2
                    lvUnits.Columns.Add("Poort", lvUnitsColumns(3))     '3
                    lvUnits.Columns.Add("Type", lvUnitsColumns(4))    '4
                    lvUnits.Columns.Add("Naam", lvUnitsColumns(5))   '5
                    lvUnits.Columns.Add("Software", lvUnitsColumns(6))  '6
                    lvUnits.Columns.Add("AP Mac", lvUnitsColumns(7)) '7
                    lvUnits.Columns.Add("Leeft..", lvUnitsColumns(8)) '8
                    lvUnits.Columns.Add("IPAddressSoort", 0)  '9


                Else
                    'in case the user has changed vwidths

                    .Columns(0).Width = 1580 \ 15
                    .Columns(1).Width = 2250 \ 15
                    .Columns(2).Width = 1560 \ 15
                    .Columns(3).Width = 700 \ 15
                    .Columns(4).Width = 700 \ 15
                    .Columns(5).Width = 4500 \ 15
                    .Columns(6).Width = 2000 \ 15
                    .Columns(7).Width = 2000 \ 15
                    .Columns(8).Width = 2000 \ 15
                    .Columns(9).Width = 0
                End If
            End With
            lvUnits.Items.Clear()


            For Teller = 0 To UBound(POE.Unit)

                Dim LVI As New ListViewItem(POE.Unit(Teller).IPAddress, "R" & Teller)
                LVI.SubItems.Add(POE.Unit(Teller).MACaddress)
                LVI.SubItems.Add(POE.Unit(Teller).PoE_IP_Address)

                'Cicso switches support stacking, and therefore we need to use the full port path instead of just the port number here
                If POE.Unit(Teller).PoE_Poort IsNot Nothing Then
                    If POE.Unit(Teller).PoE_Poort.Contains("/") Then
                        LVI.SubItems.Add(POE.Unit(Teller).PoE_Poort)
                    Else
                        LVI.SubItems.Add(Strings.Right("00" & POE.Unit(Teller).PoE_Poort, 2))
                    End If
                Else
                    LVI.SubItems.Add(Strings.Right("00" & POE.Unit(Teller).PoE_Poort, 2))
                End If


                'Not sure why I made this change below. Trying now with two digit ports
                'LVI.SubItems.Add(POE.Unit(Teller).PoE_Poort)
                LVI.SubItems.Add(UnitKind(POE.Unit(Teller).KIND))
                LVI.SubItems.Add(POE.Unit(Teller).NAAM)
                LVI.SubItems.Add(POE.Unit(Teller).SoftwareVersionString)
                LVI.SubItems.Add(POE.Unit(Teller).AttachedMAC)
                Try
                    LVI.SubItems.Add(DateDiff("h", POE.Unit(Teller).LastSYSLogMSG, Now))
                Catch ex As Exception
                    Bug("Error adding " & POE.Unit(Teller).LastSYSLogMSG & " for unit " & POE.Unit(Teller).IPAddress)
                End Try

                LVI.SubItems.Add(IPFormat(POE.Unit(Teller).IPAddress))
                LVI.Tag = "R" & Teller
                lvUnits.Items.Add(LVI)

                POE.Unit(Teller).Key = "R" & Teller

                'With lvUnits.Items.Add("1", "R" & Teller, POE.Unit(Teller).IPAddress)  'add evt. here pictures



                '    .SubItems(1) = New ListViewItem.ListViewSubItem(POE.Unit(Teller).MACaddress)
                '    .SubItems(2) = POE.Unit(Teller).PoE_IP_Address
                '    .SubItems(3) = Format(POE.Unit(Teller).PoE_Poort, "@@")
                '    .SubItems(4) = UnitKind(POE.Unit(Teller).KIND)
                '    .SubItems(5) = POE.Unit(Teller).NAAM
                '    .SubItems(6) = POE.Unit(Teller).SoftwareVersionString
                '    .SubItems(7) = POE.Unit(Teller).AttachedMAC

                '    On Error Resume Next 'Kan zijn dat lastsyslogmsg geen datum is, dan doen we niets
                '    .SubItems(8) = DateDiff("h", POE.Unit(Teller).LastSYSLogMSG, Now)
                '    On Error GoTo 0
                '    .SubItems(9) = IPFormat(POE.Unit(Teller).IPAddress)

                '    POE.Unit(Teller).Key = "R" & Teller
                'End With


                'Debug.Print("Key: " & lvUnits.Items(1).Items(1).Key)

                Map(GetOctetFromIPAdres(POE.Unit(Teller).IPAddress, 3), GetOctetFromIPAdres(POE.Unit(Teller).IPAddress, 4)).PoEUDTnr = Teller

            Next Teller

            'lvUnits.ListItems(20).ForeColor = vbRed
            'lvUnits.ListItems(20).Bold = True
            lvUnits.View = View.Details
            lvUnits.FullRowSelect = True
        Catch ex As Exception
            Bug(ex.Message & " tijdens FillListUnits.")
        End Try


    End Sub

    ''' <summary>
    ''' This starts the PCapture thread
    ''' </summary>
    ''' <remarks></remarks>
    Sub StartPCap()
        Bug("Starting up the pCAP")
        StopCapture = False
        'This starts the PCap
        Threading.ThreadPool.QueueUserWorkItem(AddressOf PCapture)
    End Sub
    ''' <summary>
    ''' This should be running on it's own thread, dont call it directly
    ''' </summary>
    ''' <remarks></remarks>
    Sub GetNetworkAdapterList()
        Bug("Get all network adapters - this can take a few seconds....")

        allNetworkDevices = LivePacketDevice.AllLocalMachine

        If allNetworkDevices.Count = 0 Then
            Bug("No interfaces found! Make sure WinPcap is installed.")
            Return
        End If

        Dim i As Integer = 0

        While i <> allNetworkDevices.Count
            Dim device As LivePacketDevice = allNetworkDevices(i)
            Console.Write((i + 1) & ". " & device.Name)
            If device.Description IsNot Nothing Then
                If device.Attributes <> DeviceAttributes.Loopback Then
                    For Each adres As PcapDotNet.Core.DeviceAddress In device.Addresses
                        If adres.Address.Family = SocketAddressFamily.Internet Then
                            If InStr(adres.Address.ToString, "0.0.0.0", CompareMethod.Binary) = 0 Then
                                Bug("Gevonden: (" & i & "), " & device.Description & " - " & Replace(adres.Address.ToString, "Internet", "", 1, , CompareMethod.Text))
                                lstAdaptersAdd(device.Description & " (" & Replace(adres.Address.ToString, "Internet ", "", 1, , CompareMethod.Text) & ")" & Space(200) & i)
                            End If
                        End If
                    Next
                End If
            End If
            i += 1
        End While
        allNetworkDevicesInitialised = True
    End Sub
    ''' <summary>
    ''' This should be running on it's own thread, dont call it directly
    ''' </summary>
    ''' <remarks></remarks>
    Sub PCapture()

        'AllNetworkDevices is needed here, and is populated by GetNetworkAdapters thread. We need to wait for this to be initialised before we can use it.
        Bug("Wachten tot de lijst van adapters gevuld is...")


        Do While allNetworkDevicesInitialised = False

        Loop


        Try
            Bug("Probeer adapter " & WinPCapAdapter & " te openen.")
            Dim selectedDevice As PacketDevice = allNetworkDevices(WinPCapAdapter)

            'Loop methode
            Using communicator As PacketCommunicator = selectedDevice.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000)

                If communicator.DataLink.Kind <> DataLinkKind.Ethernet Then
                    Bug("This program works only on Ethernet networks.")
                    Return
                End If

                Bug("Listening on " & selectedDevice.Description & "...")
                For Each ad In selectedDevice.Addresses
                    If ad.Address.Family = SocketAddressFamily.Internet Then
                        lblSelectedNetworkAdapterText(selectedDevice.Description & "(" & Replace(ad.Address.ToString, "internet ", "", , , CompareMethod.Text) & ") geselecteerd.")
                        Bug(lblSelectedNetworkAdapter.Text)
                        Exit For
                    End If
                Next

                communicator.SetFilter("udp port 67 or udp port 45200")

                Dim packet As Packet

                Do

                    Dim result As PacketCommunicatorReceiveResult = communicator.ReceivePacket(packet)

                    Select Case result
                        Case PacketCommunicatorReceiveResult.Timeout
                            Continue Do
                        Case PacketCommunicatorReceiveResult.Ok

                            Dim ip As IpV4Datagram = packet.Ethernet.IpV4
                            Dim udp As UdpDatagram = ip.Udp
                            'Me.Bug(ip.Source.ToString & ":" + udp.SourcePort.ToString & " -> " + ip.Destination.ToString & ":" + udp.DestinationPort.ToString)

                            'Me.txt.AppendText("TEst")
                            'Dim Inhoud As Datagram = udp.Payload
                            Dim S As String = ""


                            Dim Data() As Byte = udp.Payload.ToArray
                            For i% = 0 To Data.Length - 1
                                If Data(i) <> 0 Then
                                    S += Chr(Data(i))
                                Else
                                    S += Chr(0)
                                End If
                            Next

                            Try
                                If udp.SourcePort = 997 Or udp.SourcePort = 45200 Then
                                    AddAIP(Data, ip.Source.ToString, udp.SourcePort) 'This is for the Find Albireo's button.(https://vanbreda.atlassian.net/wiki/x/QYDEQg)
                                ElseIf udp.SourcePort = 67 Or udp.SourcePort = 68 Then
                                    Bug(DHCPAnalise(S, ip.Source.ToString))
                                End If

                            Catch ex As Exception
                                MsgBox("PCapture error: " & ex.Message)
                            End Try



                            'Me.Bug(S & vbCrLf)

                        Case Else
                            Throw New InvalidOperationException("The result " & result & " should never be reached here")
                    End Select
                Loop While StopCapture = False
            End Using
        Catch ex As Exception
            Bug("Error tijdens opzetten WinPCap op Adapter " & WinPCapAdapter & ": " & ex.Message)
        End Try
        Bug("Capture stopped...")

    End Sub

    Sub AddAIP(Data As Byte(), SourceIP As String, SourcePort As Integer)
        'All AIP traffic between the Albireo's and the INEX are captured here. We only want to react to ENZI command 27
        'This is used to add Albireo's to the Do The Magic that are not in the same network, i.e. the Alilve messages do not arrive here.
        'These messages shloud only be triggered by the "Find Albireo" command button.

        Dim IncStr As String = ""
        Dim i As Int16
        For Each b As Byte In Data
            IncStr += Hex(b) & " "
        Next

        If Data(3) <> &H27 Then Exit Sub

        Bug(IncStr & " van " & SourceIP)

        Try
            If SourcePort <> 997 Then
                'Bug(IncStr)
                Data = DeStuff(Data)
                Dim MacAddress As String = ""
                For i = 6 To 11
                    MacAddress += Strings.Right("00" & Hex(Data(i)), 2)
                    Select Case i
                        Case Is = 7, 9
                            MacAddress += "-"
                    End Select
                Next
                Dim SW As String = ""
                For i = 12 To 15
                    SW += Data(i) & "."
                Next
                SW = Mid(SW, 1, Len(SW) - 1)

                SW += " / "
                For i = 16 To 19
                    SW += Data(i) & "."
                Next
                SW = Mid(SW, 1, Len(SW) - 1)

                SW += " / "
                For i = 20 To 23
                    SW += Data(i) & "."
                Next
                SW = Mid(SW, 1, Len(SW) - 1)

                SW += " Processor: " & Data(24)

                'We want to add this info to the AIP_List, we need to generate this format:
                'Dim TestStr As String = " Alive H:8 B:1.6 L:4.5.2.0 A:4.6.0.0"
                'but only if the Albireo "Alive" list is empty. This feature is either / or - have two lists of Albireo's creates a do the magic with double Albireo's
                If GroupBox6.Enabled = True Then
                    Add_lstFoundAlbireos(SourceIP, MacAddress, Data(24))
                    Bug("Albireo " & MacAddress & " - IP: " & SourceIP & " SW: " & SW & " gevonden.")
                End If

            End If
        Catch ex As Exception
            MsgBox("Add AIP error: " & ex.Message)
        End Try

    End Sub

    Sub Add_lstFoundAlbireos(IP As String, MacAdres As String, ProcessorType As String)
        If lstFoundAlbireos.InvokeRequired Then
            lstFoundAlbireos.Invoke(Sub() Add_lstFoundAlbireos(IP, MacAdres, ProcessorType))
            Return

        End If

        'Add the found Albireo to the FoundList if it isn't already there.
        Dim i As Integer, S As String(), Found As Boolean
        For i = 0 To lstFoundAlbireos.Items.Count - 1
            S = Split(lstFoundAlbireos.Items(i).ToString, " / ")
            If S(0) = IP Then
                Found = True
                Exit For
            End If
        Next

        If Not Found Then
            lstFoundAlbireos.Items.Add(IP & " / " & MacAdres & " / " & ProcessorType)
        End If
        lblFoundAlbireos.Text = "Gevonden albireo's (" & lstFoundAlbireos.Items.Count & ")"

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        'StopCapture = True
        UDPaip.CleanUp()
        UDPaip.Dispose()
        tmrAIPlstADD.Enabled = False
        'PacketCap.Abort()
    End Sub
    ''' <summary>
    ''' This shows all messages in the log and screen excluding the "Alive" messages.
    ''' and passes the the string and senders IP address to AddToAIPList() 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="datagram"></param>
    ''' <param name="RemoteEndPoint"></param>
    ''' <param name="remoteip"></param>
    ''' <param name="Data"></param>
    Private Sub UDPaip_Data_Arrival(sender As clsUDP, datagram As String, RemoteEndPoint As Net.IPEndPoint, remoteip As String, Data As Byte()) Handles UDPaip.Data_Arrival

        Dim Str As String
        Dim IP$

        Try
            Str = datagram
            IP = RemoteEndPoint.Address.ToString

            'We want to bug everything except all the "Alive" messages
            If Microsoft.VisualBasic.Strings.Left(Trim(Str), 5) <> "Alive" Then
                Bug(IP & ": " & Str)
            End If


            If DoTheMagicBezig Then Exit Sub 'Als we nog bezig zijn met the magic dan even niet meer detecteren.

            Try
                'Debug.Print(IP & " - " & Str)
                AddToAIPList(Str, IP)
            Catch ex As Exception
                Bug("Error during addtoAIPList" & ex.Message)
            End Try


        Catch ex As Exception

            Bug("Fout: " & ex.Message & " tijdens frmPoE.UDPaip_Data_Arrival")
        End Try

    End Sub

    Sub AddToAIPList(Str As String, IP As String)
        If Me.InvokeRequired Then

            Me.Invoke(Sub() AddToAIPList(Str, IP))
            Return
        End If

        ' If IP = "10.26.2.74" Then Stop

        Dim i%
        Dim Negeer() As String
        Dim Found%
        Dim NewUnit As Boolean
        Dim PoEUnitNr As Integer, LV As Integer
        'Try

        'Kijken of deze AIP in de negeerlijst voorkomt, zo ja niets verder doen.
        If txtNegeerAIPAdressen.Text <> "" Then
            Negeer = Split(txtNegeerAIPAdressen.Text, " ")
            For i = 0 To UBound(Negeer)
                If Strings.Left(IP, Len(Negeer(i))) = Negeer(i) Then
                    Str = ""
                    Exit Sub
                End If
            Next i
        End If

        'Loop door de AIP lijst. Als deze AIP al in de lijst staat vernieuw deze dan met de huidige tijd / datum
        For i = 0 To UBound(AIP_List)
            If AIP_List(i).IPAdres = IP Then  'ATV client is already in the list, renew it.
                AIP_List(i).Tijd = Now()
                AIP_List(i).Versie = Replace(Str, "Alive ", "", , , vbTextCompare)
                Found = True
                Exit For
            End If
        Next i

        'Als deze AIP nog niet in de lijst staat - dan gewoon toevoegen
        If Not Found Then
            ReDim Preserve AIP_List(UBound(AIP_List) + 1)
            'Omdat we hier een AIP toevoegen maken we newunit true (om de list box te updaten)
            NewUnit = True
            With AIP_List(UBound(AIP_List))
                .IPAdres = IP
                .Tijd = Now()
                .Versie = Replace(Str, "Alive ", "", , , vbTextCompare)
            End With
        End If
        Found = False

        'Find the IP address from this Alive message in the map, Get its Position in the PoE UDT and ListView.

        'If GetPOEnr returns a value from the MAP that is incorrect then this AIP probably hasn't been added to the PoE.Unit list
        'In that case, make the number -1
        PoEUnitNr = GetPOEnr(IP)
        If PoEUnitNr <> -1 Then
            If POE.Unit.Length > 0 Then 'this happens when all units have been deleted.
                If POE.Unit(PoEUnitNr).IPAddress <> IP Then
                    PoEUnitNr = -1
                End If
            End If
        End If


            LV = GetLV(IP)
        'If LV is 0 then the MAP has not been initialised, or this unit does not reside in the LV
        'We kan leave it 0, and test on this because lvUnits() is 1 based, NOT zero based.

        'Update the Software  version and Time fields
        UpdateUDT(IP, Str)

        'Make this ListView line black.
        If LV <> 0 Then
            ChangeLVColor(GetLV(IP), False)
        End If

        'Als we een "reset" string krijgen in de Alive bericht dan AIP gelijk onderuit trappen.

        If InStr(1, Str, "reset", vbTextCompare) <> 0 Then
            Bug("AIP " & IP & " heeft een reset aangevraagd. ff kijken of ik een switch en poort kan vinden.")

            If PoEUnitNr <> -1 Then
                With POE.Unit(PoEUnitNr)
                    Bug("AIP: " & .NAAM & " (" & .IPAddress & ") ga ik onderuit trappen... (op verzoek van de AIP zelf hoor).")
                    Dim Port(0) As String
                    Port(0) = .PoE_Poort
                    ResetPoort(.PoE_IP_Address, Port, True)
                End With
            Else
                Bug(IP & " vraagt een reset aan, maar deze zit nog niet in de Units lijst.", , True)
            End If

        End If

        'Als deze AIP niet in de POE.Unit lijst voorkomt - dan een melding geven (als de chk aan staat)
        If chkKomtWelBinnen.Checked Then
            If PoEUnitNr = -1 Then 'als de AIP niet in de lv / unit lijst voorkomt - melden
                If InStr(1, Str, "Alive") <> 0 Then
                    Bug("AIP " & IP & " Komt wel binnen maar staat niet in de bewakingslijst.")
                    ReplaceUnit(UDPaip.RemoteIP)
                    KomtWelBinnen(UDPaip.RemoteIP)
                    ' Stop
                End If
            End If
        End If

        'Hoeveel AIPs zitten in de lijst?:
        UpdateNrOfAIPs("Active AIP units: " & UBound(AIP_List))

        'Als dit een nieuwe unit is dan moeten we even de listbox refreshen via de timer event
        'If NewUnit Then tmrAIPlstADD_Tick()

        If PoEUnitNr = -1 Then Debug.Print(IP & " geen POENr")
        If LV = -1 Then Debug.Print(IP & " geen LV")

        'Catch ex As Exception
        'Debug.Print(ex.Message & "tijdens AddToAIPList.")
        'End Try

    End Sub
    ''' <summary>
    ''' Update the number of Albireo's lable, via Invoke if needed
    ''' </summary>
    ''' <param name="Wat"></param>
    Sub UpdateNrOfAIPs(Wat As String)
        If lblNumberOfAIPs.InvokeRequired Then
            Dim args() As String = {Wat}
            Me.Invoke(New Action(Of Object)(AddressOf UpdateNrOfAIPs), args)
            Return
        End If
        lblNumberOfAIPs.Text = Wat
    End Sub

    'Sub tmrAIPlstADD_Tick(sender As Object, e As EventArgs) Handles tmrAIPlstADD.Tick
    Sub tmrAIPlstADD_Tick() Handles tmrAIPlstADD.Tick

        Application.DoEvents()
        If BusyUpdatingAIPList Then
            Debug.Print("________________________OVERSLAAN____________________!!")
            Exit Sub
        End If
        If DoTheMagicBezig Then Exit Sub

        'BusyUpdatingAIPList = True

        Dim SPLT() As String, Found As Boolean, i%, ii%


        For ii = 1 To UBound(AIP_List)
            'Check if we have the is AIP
            For i = 0 To lstAIP.Items.Count - 1
                SPLT = Split(lstAIP.Items(i), ",")
                If SPLT(0) = IPFormat(AIP_List(ii).IPAdres, True) Then
                    lstAIP.Items(i) = IPFormat(AIP_List(ii).IPAdres, True) & "," & AIP_List(ii).Tijd.ToLocalTime & "," & AIP_List(ii).Versie
                    Found = True
                    Exit For
                End If
            Next i

            If Not Found Then
                lstAIP.Items.Add(IPFormat(AIP_List(ii).IPAdres, True) & "," & AIP_List(ii).Tijd.ToLocalTime & "," & AIP_List(ii).Versie)
            End If
            Found = False
        Next ii
        'Temp haal dit weg - denk dat het te veel tijd kost.

        Dim iUnit%, LV%
        For iUnit = 0 To UBound(POE.Unit)
            If POE.Unit(iUnit).IPAddress <> "" Then LV = GetLV(POE.Unit(iUnit).IPAddress) 'IPadres = "" if the PoE.Unit UDT has not yet been filled
            UpdateLV(LV, iUnit)

        Next iUnit


        tmrAIPlstADD.Enabled = False 'Als het goed is zal dit de timer resetten omdat deze proc
        tmrAIPlstADD.Enabled = True  'ook buiten de timer event om aangeroepen wordt, en zo geen twee keer deze evnt snel achter elkaar
        'Debug.Print(Now & "tmrAIPlstADD")
        Me.Label6.Text = "Units (" & POE.Unit.Count & "):"
        BusyUpdatingAIPList = False
    End Sub

    Sub UpdateLV(LVindex As Integer, PoEUnitNr As Integer)
        'lvUnits.BeginUpdate()
        With lvUnits.Items(LVindex)
            If Not POE.Unit Is Nothing Then
                If POE.Unit(PoEUnitNr).SoftwareVersionString <> .SubItems(6).Text Then .SubItems(6).Text = POE.Unit(PoEUnitNr).SoftwareVersionString

                If Not POE.Unit(PoEUnitNr).LastSYSLogMSG Is Nothing Then

                    If POE.Unit(PoEUnitNr).AttachedMAC <> .SubItems(7).Text Then .SubItems(7).Text = POE.Unit(PoEUnitNr).AttachedMAC
                    Try

                        If POE.Unit(PoEUnitNr).LastSYSLogMSG <> .SubItems(8).Text Then .SubItems(8).Text = CDate(POE.Unit(PoEUnitNr).LastSYSLogMSG).ToLocalTime
                    Catch ex As Exception
                        Bug($"error updating LV index for {POE.Unit(PoEUnitNr).LastSYSLogMSG} nummer {POE.Unit(PoEUnitNr).IPAddress}",, True)
                    End Try
                End If
            End If

        End With
        'lvUnits.EndUpdate()
    End Sub

    Sub KomtWelBinnen(IP_Adres As String)

        Dim i%
        Dim Found As Boolean
        For i = 0 To lstKomtWelBinnen.Items.Count - 1
            If lstKomtWelBinnen.Items(i) = IP_Adres Then
                Found = True
                Exit For
            End If
        Next i
        If Not Found Then lstKomtWelBinnen.Items.Add(IP_Adres)
    End Sub

    Sub ReplaceUnit(WelkeIPAdres As String)

        If chkAutoVervangUnits.Checked Then
            ToevoegenNieuweUnit(WelkeIPAdres)

            Dim i%
            Dim f%
            Dim NieuweUnit As Integer

            'welke unit is net toegevoegd?
            For i = 0 To UBound(POE.Unit)
                If POE.Unit(i).IPAddress = WelkeIPAdres Then
                    NieuweUnit = i
                    Exit For
                End If
            Next i

            'Dan kijken of we dubbele switch/poort hebben
            Dim tmpPoe As PoEType
            For i = 0 To UBound(POE.Unit)
                If i <> NieuweUnit Then
                    If POE.Unit(i).PoE_IP_Address = POE.Unit(NieuweUnit).PoE_IP_Address Then
                        If POE.Unit(i).PoE_Poort = POE.Unit(NieuweUnit).PoE_Poort Then
                            'we hebben een dubbel - deze moet weg.

                            tmpPoe = POE
                            ReDim tmpPoe.Unit(0)
                            For f = 0 To UBound(POE.Unit)
                                If i <> f Then
                                    tmpPoe.Unit(UBound(tmpPoe.Unit)) = POE.Unit(f)
                                    ReDim Preserve tmpPoe.Unit(UBound(tmpPoe.Unit) + 1)
                                End If
                            Next f
                            ReDim Preserve tmpPoe.Unit(UBound(tmpPoe.Unit) - 1)
                        End If
                    End If
                End If
            Next i
            POE = tmpPoe
            FillListUnits()
            UpdateMap()
        End If
    End Sub

    Sub UpdateMap()
        'Update the map - fill the lvUnits info
        Dim i%
        For i = 0 To lvUnits.Items.Count - 1
            If lvUnits.Items(i).Text <> "" Then
                Map(GetOctetFromIPAdres(lvUnits.Items(i).Text, 3), GetOctetFromIPAdres(lvUnits.Items(i).Text, 4)).LVindex = i
            End If
        Next i

    End Sub


    Sub ToevoegenNieuweUnit(Optional NieuweIPAdres As String = "")
        Dim Str$
        Dim MAC$
        Dim KIND%
        Dim i%
        Dim Found As Boolean
        Dim NW() As String

        If NieuweIPAdres = "" Then

            'Str = InputBox("Voer het IP adres in van de toe te voegen unit." & vbCrLf & vbCrLf & _
            '              "Als je het IP adress invoert met een ; MACAdres ; PoE Switch IPAdres ; PoE Poort dan zal ik deze gegevens " & _
            '             "gebruiken en ze niet opzoeken in de switches." & vbCrLf & _
            '             "Bijv: 10.23.2.1;0025-6e10-1234;10.23.1.112;21")



            ToevoegenNieuwUnitStr = "" 'Deze wordt door frmToevoegen ingevuld

            frmToevoegen.ShowDialog()
            frmToevoegen.Dispose()
            Str = ToevoegenNieuwUnitStr

        Else
            Str = NieuweIPAdres
        End If


        'als, na het vragen om een IP adres, deze nog steeds leeg is, dan heeft met op cancel gedrukt, of geen ip adres ingevuld en op OK
        'Hoe dan ook, als die nog leeg is, dan kappen.
        If Str = "" Then Exit Sub

        'Handmatig invoer met ; als separator.
        If InStr(1, Str, ";") Then
            NW = Split(Str, ";")
            If UBound(NW) <> 5 Then
                MsgBox("Wat je ingevoerd hebt kan niet. Kijk goed naar het voorbeeld. We gaan hier niets mee doen.")
                Exit Sub
            End If

            If Not IsIPAddress(NW(0)) Then
                MsgBox("Ik kan alleen een unit toevoegen als je een gedige IP adres invult.")
                Exit Sub
            End If



            'Check to see if this ip addres already exists
            For i = 0 To UBound(POE.Unit)
                If NW(0) = POE.Unit(i).IPAddress Then
                    With POE.Unit(i)
                        MsgBox(.IPAddress & " staat al in de lijst met naam: " & .NAAM & " en MAC adres: " & .MACaddress & "." & vbCrLf & vbCrLf &
                               "Lijkt me niet verstandig om deze nog een keer toe te voegen.")

                        SetTab(0, True)
                        Exit Sub
                    End With
                End If
            Next i

            'Als dit de eerste keer is dat je iets toevoegd dan is Poe.unit(0).ipadres leeg, dan niet een extra unit toevoegen
            If UBound(POE.Unit) = 0 Then
                If POE.Unit(0).IPAddress <> "" Then
                    ReDim Preserve POE.Unit(UBound(POE.Unit) + 1)
                End If
            Else
                ReDim Preserve POE.Unit(UBound(POE.Unit) + 1)
            End If

            With POE.Unit(UBound(POE.Unit))
                .IPAddress = NW(0)
                .KIND = NW(4) 'InputBox("Geef aan of het een ATVS Client (1), ATVS Server (2), AIP (3), DIV (4), BTX (5), EVA (6), EVS (7) is.")
                If NW(1) = "" Then
                    .MACaddress = GetRemoteMACAddress(NW(0))
                    .MACaddress = ConvertMAC(.MACaddress)
                Else
                    .MACaddress = NW(1)
                End If

                .PoE_IP_Address = NW(2)
                .PoE_Poort = NW(3)
                .ScreenActive = False
                .NAAM = NW(5)
            End With
            FillListUnits()
            UpdateMap()
            Exit Sub
        End If



        ' Check to see if the IP address being added already exists in PoE UDT
        For i = 0 To UBound(POE.Unit)
            If Str = POE.Unit(i).IPAddress Then
                With POE.Unit(i)
                    MsgBox(.IPAddress & " staat al in de lijst met naam: " & .NAAM & " en MAC adres: " & .MACaddress & "." & vbCrLf & vbCrLf &
                           "Lijkt me niet verstandig om deze nog een keer toe te voegen.")

                    SetTab(0, True)
                    Exit Sub
                End With
            End If
        Next i


        If chkMACuitDatabase.Checked Then
            Bug("MAC adressen ophalen uit de INEX 500 database")
            GetARPTabelFromINEXDatabase() 'Zet alle mac adressen in arptabel vanuit de database
        End If


        'POE.Unit(j).MACAddress = ConvertMAC(FindMAC(POE.Unit(j).IPAddress))


        If My.Computer.Network.Ping(Str, 100) = True Then
            'Achterhalen KIND of unit aan de hand van mac adres / in welke live list ie zit
            MAC = ConvertMAC(Format(Replace(FindMAC(Str), ":", ""), "@@-@@-@@-@@-@@-@@")) 'Op een dag moeten we alle formats eruit halen en alleen werken met macadressen zonder format
            If MAC <> "" Then
                If Strings.Left(MAC, 7) = "0025-6E" Then
                    KIND = Albireo_IP
                ElseIf Strings.Left(MAC, 7) = "0005-51" Then
                    For i = 0 To lstActiveVideoServers.Items.Count - 1
                        If InStr(1, lstActiveVideoServers.Items(i), Str, vbTextCompare) Then
                            KIND = ATVS_VideoServer
                            Found = True
                            Exit For
                        End If
                    Next i
                    If Not Found Then
                        For i = 0 To lstActiveATVS.Items.Count - 1
                            If InStr(1, lstActiveATVS.Items(i), Str, vbTextCompare) Then
                                KIND = ATVS_Client
                                Found = True
                                Exit For
                            End If
                        Next i
                    End If
                    If Not Found Then
                        KIND = InputBox(Str & " staat niet in een van de ATVS lijsten, dus weet ik niet of het een server of client is. Geef hier aan of het een Server (2) of Client (1) is. 0 = annuleren")
                    End If

                ElseIf IsSamsung(MAC) Then
                    KIND = KindEVAapp

                Else
                    For i = 0 To lstEVAVideoServer.Items.Count - 1
                        If InStr(1, lstEVAVideoServer.Items(i), Str, vbTextCompare) Then
                            KIND = KindEVAVideoServer
                            Found = True
                            Exit For
                        End If
                    Next i
                    'MsgBox Str & " is nog AIP nog ATVS.!"
                    If Not Found Then KIND = KindDiverse
                End If
            Else 'GetRemoteMACAddress(Str, MAC, "-")
                MsgBox("Ik kan het MAC adres van " & Str & " niet achterhalen, ga m ook niet toevoegen!")
            End If

        Else 'If Ping(Str, 100) Then
            MAC = ""
            MsgBox(Str & " reageerd niet op een ping, ga m ook niet toevoegen!")
        End If



        If KIND <> 0 Then 'Kind = 0 als het MAC adres niet achterhaald kon worden

            'Voeg deze unit alvast toe
            Try
                'De eerste keer is Ubound(PoE.Unit) 0 en is deze leeg, dan niet ophogen maar de lege vullen
                If Not (UBound(POE.Unit) = 0 And POE.Unit(0).IPAddress = "") Then
                    ReDim Preserve POE.Unit(UBound(POE.Unit) + 1)
                End If

            Catch ex As Exception
                MsgBox("Fout tijdens toevoegen: " & Err.Description & ". " & vbCrLf &
                       "Dit wil wel eens gebeuren als de PoE Watchdog bezig is met reset acties. Zet anders even de herstart vinkje uit en probeer opnieuw toe te voegen.")
                Exit Sub
            End Try

            With POE.Unit(UBound(POE.Unit))
                .KIND = KIND
                .IPAddress = Str
                .MACaddress = MAC

                '=======================
                Dim tmpIP$
                Dim U() As String
                SetTab(3)

                'We can not use Samsung MAC to determine if it's a wifi unit (EVA) and we want to be
                'able to poe bewaak base stations, so I think I'll just ask if the user want's to ask te
                'switched for port info. Unneccessary asking costs too much time.
                If MsgBox("Is dit een WiFi unit?", vbYesNo + vbQuestion, "WiFi unit?") = vbYes Then
                    tmpIP = "0;0"
                Else
                    tmpIP = GetSwitchPoortNummers(MAC)
                End If



                'tmpIP = FindIP(List3.List(0), Str)

                U = Split(tmpIP, ";")
                If U(0) <> "0" Then ' GetSwitchPoortNummers found this unit on this switch
                    If InStr(1, U(0), "/") <> 0 Then U = Split(U(0), "/")

                    .PoE_IP_Address = U(0)
                    If InStr(1, .PoE_IP_Address, "/") <> 0 Then .PoE_IP_Address = Strings.Left(.PoE_IP_Address, InStr(1, .PoE_IP_Address, "/") - 1)
                    .PoE_Poort = U(1)

                Else ' GetSwitchPoortNummers Couldn't find this unit
                    .PoE_IP_Address = ""
                    .PoE_Poort = ""

                End If
                FillListUnits()
                UpdateMap()
                '========================
            End With
        End If
        SetTab(0)
    End Sub

    Sub SaveCFGFile(Optional Backup As Boolean = False)

        If Dir(Application.StartupPath & "\cfg-bkp", FileAttribute.Directory) = "" Then
            Try
                My.Computer.FileSystem.CreateDirectory(Application.StartupPath & "\cfg-bkp")
            Catch ex As Exception
                Bug("Fout tijdens het aanmaken van " & Application.StartupPath & "\cfg-bkp" & ": " & ex.Message)
            End Try
        End If

        If Backup Then
            Try
                FileOpen(1, Application.StartupPath & "\cfg-bkp\AutoBKP-" & Format(Now, "yy-MM-dd HH.mm.ss") & "-PoE.cfg", OpenMode.Binary)
                FilePut(1, POE)
                FileClose(1)
            Catch ex As Exception
                Bug("Fout tijdens het wegschrijven van autobkp bestand in " & Application.StartupPath & "\cfg-bkp" & ": " & ex.Message)
            End Try
        End If

        FileOpen(1, Application.StartupPath & "\PoE-NET.cfg", OpenMode.Binary)
        FilePut(1, POE)
        FileClose(1)

    End Sub

    Function GetSwitchPoortNummers(MACAddressToFind As String) As String
        Dim i%, f%, Info$
        Dim Poorten() As String, Str() As String
        'Vraag alle switches voor de getmac info (ARP table)

        GetPortInfoAllSwitches() 'Dit zet alle info (RAW) in POORTINFO()

        'Refresh de EVAMAC array
        GetEVAMacAdressen()

        'Loop door de info om de te zoeken MAC adres te vinden
        For i = 0 To lstPoESwitches.Items.Count - 1
            Bug("Lengte van de Switch info " & i & " is: " & Len(POORTINFO(i)) & "bytes.")
            'vraag de switch voor de learned mac info
            Info = POORTINFO(i)
            Bug(Info)
            Application.DoEvents()
            'Zet de binnengekomen data om in een array met unit macaddress ; poortnr.
            Poorten = Opschonen(Info)
            'De poorten array ombouwen zodat alleen poorten mer een uniek (dus maar 1) macadres overblijven

            Poorten = AlleenUniek(Poorten) 'TODO!
            'Loop door de info. Alleen als een poort < 25 dan zit de unit op deze switch
            For f = 0 To UBound(Poorten)
                If Poorten(f) <> "" Then
                    Str = Split(Poorten(f), ";")
                    If Str(1) < 25 Then
                        If LCase(MACAddressToFind) = LCase(Str(0)) Then
                            'geef het IP adres van deze switch terug en de poort waar de MAC op zit.
                            Return Trim(Strings.Left(lstPoESwitches.Items(i), 50)) & ";" & Str(1)
                            Exit Function
                        End If
                    End If
                End If
            Next f
        Next i
        'Als we hier komen dan waren er geen matches - geef 0;0 terug - kon niets vinden
        GetSwitchPoortNummers = "0;0"
    End Function

    ''' <summary>
    ''' IN String Array met MAC;Poort
    ''' </summary>
    ''' <param name="Welke"></param>
    ''' <returns>Geef terug een String array met alle poort nummers die maar een MAC adres hebben</returns>
    ''' <remarks></remarks>
    Function AlleenUniek(Welke() As String) As String()

        'Voorbeeld: 0025-6E10-22ED;28
        '           0025-6E10-21C6;27


        Bug("Alleen uniek in met " & UBound(Welke) & " ongefilterde poorten.", True)
        Dim AUK() As String
        Dim Str() As String
        Dim i%
        Dim iEva%
        Dim IsEVA_MAC As Boolean
        'LET OP - Oude PoE WD was lower bound van deze twee arrays 1, en niet 0.
        Dim SwitchPoort(0 To 52) As String 'SwitchPoort is de poort op deze switch (kan 1 t/m 28 maximaal zijn)
        Dim UniekPoort(0 To 52) As Boolean 'Als deze poort maar een macaddress heeft dan is ie uniek

        'OMdat een tablet aan een HostAP Albireo kan hangen kunnen er twee macs aan de poe poort hangen.
        'Dus eerst MAC adressen bij EVA IP's zoeken



        'ReDim SwitchPoort(UBound(Welke)) 'default false
        For i = 0 To UBound(Welke)

            If Welke(i) <> "" Then
                'If Left(Welke(i), 14) = "0025-6E10-22ED" Then Stop

                Bug("Splitting " & Welke(i) & " using the ;", True)
                'If InStr(1, Welke(i), "0395", vbTextCompare) <> 0 Then Stop
                Str = Split(Welke(i), ";")

                'If CInt(Str(1)) < 27 Then Stop

                If SwitchPoort(Str(1)) = "" Then 'Als deze poort nog geen mac adres heeft
                    'Stop
                    'IF the unit is not a MAC EVA in the EVA list then add it
                    'these are probably the wifi host aP tablets
                    For iEva = 0 To UBound(EVAMAC)
                        If Str(0) = EVAMAC(iEva) Then
                            IsEVA_MAC = True
                            Exit For
                        End If
                    Next iEva
                    If Not IsEVA_MAC Then
                        SwitchPoort(Str(1)) = Str(0)
                        UniekPoort(Str(1)) = True    'alleen als er een macadres aan hangt is ie uniek
                    End If
                    IsEVA_MAC = False
                Else 'Als er AL een mac adres aan hangt dan is ie niet uniek meer

                    For iEva = 0 To UBound(EVAMAC)
                        If Str(0) = EVAMAC(iEva) Then
                            IsEVA_MAC = True
                            Exit For
                        End If
                    Next iEva
                    If Not IsEVA_MAC Then
                        UniekPoort(Str(1)) = False
                    End If
                    IsEVA_MAC = False

                End If

            End If
        Next i

        ReDim AUK(0)
        Bug("alleen uniek midden", True)
        For i = 1 To 50
            Bug(i & " " & UniekPoort(i), True)
            'Debug.Print i & " " & UniekPoort(i)
            If UniekPoort(i) Then
                AUK(UBound(AUK)) = SwitchPoort(i) & ";" & i
                ReDim Preserve AUK(UBound(AUK) + 1)
            End If
        Next i
        If UBound(AUK) > 0 Then ReDim Preserve AUK(UBound(AUK) - 1) 'de laatste onnodig aangemaakte member verwijderen (mits die aangemaakt is)
        AlleenUniek = AUK

        Bug("alleen uniek uit, " & UBound(AUK) & " unieke poorten gevonden.", True)
    End Function
    ''' <summary>
    ''' IN String Array containing MAC;Poort
    ''' </summary>
    ''' <param name="Welke"></param>
    ''' <returns>Geef terug een String array met alle porten die maar een MAC adres hebben</returns>
    ''' <remarks>This hads been modified to accommodate for the fact that switch port contains strings</remarks>

    Function AlleenUniek2(Welke() As String) As String()

        'Voorbeeld: 0025-6E10-22ED;28
        '                0025-6E10-21C6;Gi1/0/22
        '                0025-6E10-21A3;Gi2/0/27


        Bug("Alleen uniek in met " & UBound(Welke) & " ongefilterde poorten.", True)

        Dim UniquePort As New Dictionary(Of String, String)
        Dim i As Integer
        Dim Str() As String
        Dim AUK() As String

        'Loop through the switch port strings, and add the port string to the dictionary (key) if not already there.
        'If the dictonary already has this port as key, then we change the value to "false", as this port is not unique, 
        'and we don't want to use it in the PoE units list
        For i = 0 To UBound(Welke) - 1
            Debug.Print(Welke(i))
            Str = Split(Welke(i), ";")
            If UniquePort.ContainsKey(Str(1)) Then
                UniquePort(Str(1)) = "False"
            Else
                UniquePort(Str(1)) = Str(0)
            End If
        Next

        'Because the calling function expects a string array containing MAC-ADDRESS;Port for the unique ports, we 
        'create a temp AUK array to pass back.
        ReDim AUK(0)
        For Each kvp As KeyValuePair(Of String, String) In UniquePort
            If kvp.Value <> "False" Then
                AUK(UBound(AUK)) = kvp.Value & ";" & kvp.Key
                ReDim Preserve AUK(AUK.Count)
            End If
        Next
        ReDim Preserve AUK(UBound(AUK) - 1) 'Last created AUK wasn't neccessart, remove it

        Bug("alleen uniek uit, " & UBound(AUK) + 1 & " unieke poorten gevonden.", True)

        Return AUK
    End Function
    ''' <summary>
    ''' Dit zet de informatie uit (een) switch(es) waar alle "learned mac-addresses" in zit om in een string array:  MAC-Address;PoEPoort
    ''' </summary>
    ''' <param name="Wat"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Opschonen(Wat As String) As String()

        'Als UniekOnly aan staat dan worden alleen de poe poorten met een geleerde macadres gebruikt
        Dim Str() As String, strPoort As String, strMACaddress As String
        Dim Opscnn() As String
        ReDim Opscnn(0)
        Str = Split(Wat, vbCrLf)
        Bug("We beginnen met " & UBound(Str) & " regels.")
        Bug("Opschonen switch data: " & vbCrLf & Wat)

        'Eerst de <-[42D weg halen
        Dim i%

        For i = 0 To UBound(Str)
            If Str(i) <> "" Then

                Bug("Opschonen: " & Str(i), True)
                If Strings.Left(Str(i), 8) = "1.3.6.1." Then 'This is a SNMP string (HP Switches), We have to clean up  SNMP info

                    Dim tmpPort As String() = Split(Str(i), ".")
                    strPoort = tmpPort(17)
                    Dim tmpMAC As String() = Split(Str(i), ";")
                    strMACaddress = Replace(tmpMAC(1), " ", "")
                    strMACaddress = Mid(strMACaddress, 1, 4) & "-" & Mid(strMACaddress, 5, 4) & "-" & Mid(strMACaddress, 9, 4)

                    Opscnn(UBound(Opscnn)) = strMACaddress & ";" & strPoort
                    Bug(Opscnn(UBound(Opscnn)), True)
                    ReDim Preserve Opscnn(UBound(Opscnn) + 1)

                ElseIf Strings.Left(Str(i), 11) = "MAC address" Then 'Aruba Switches via API
                    'Stop
                    Dim tmpPort As String() = (Split(Str(i), ":"))
                    tmpPort = Split(tmpPort(7), "/")
                    strPoort = tmpPort(2)
                    Dim tmpMAC As String() = Split(Str(i), "-")
                    strMACaddress = Trim(Replace(tmpMAC(0), "MAC address:", ""))
                    strMACaddress = Replace(strMACaddress, ":", "")
                    strMACaddress = Mid(strMACaddress, 1, 4) & "-" & Mid(strMACaddress, 5, 4) & "-" & Mid(strMACaddress, 9, 4)

                    Opscnn(UBound(Opscnn)) = strMACaddress & ";" & strPoort
                    Bug(Opscnn(UBound(Opscnn)), True)
                    ReDim Preserve Opscnn(UBound(Opscnn) + 1)

                    'we're going for 0025-6E10-1234;2

                Else 'This is the SSH (CLI) info from Cisco switches - we need to clean this up
                    Dim tmpPort As String() = Split(Str(i), "/")
                    strPoort = tmpPort(1) & "/" & tmpPort(2) & "/" & tmpPort(3)
                    Dim tmpMAC As String() = Split(Str(i), " ")
                    strMACaddress = Replace(tmpMAC(0), ".", "")
                    strMACaddress = Mid(strMACaddress, 1, 4) & "-" & Mid(strMACaddress, 5, 4) & "-" & Mid(strMACaddress, 9, 4)

                    Opscnn(UBound(Opscnn)) = strMACaddress.ToUpper & ";" & strPoort
                    Bug(Opscnn(UBound(Opscnn)), True)
                    ReDim Preserve Opscnn(UBound(Opscnn) + 1)
                End If

                'If InStr(1, Str(i), "/") And InStr(1, Str(i), "#") = 0 Then
                'String ziet er zo uit: 
                '1.3.6.1.4.1.11.2.14.11.5.1.9.4.2.1.2.1.88.3.251.150.176.255;58 03 FB 96 B0 FF
                '1.3.6.1.4.1.11.2.14.11.5.1.9.4.2.1.2.                                                                     = de oib
                '                                                    1.                                                                   = poort
                '                                                       88.3.251.150.176.255                                 = MAC address in decimal
                '                                                                                        ;58 03 FB 96 B0 FF     = MAC string
                'First get the port number


            End If
        Next i
        Opschonen = Opscnn
        Bug("we eindigen met " & UBound(Opscnn) & " regels.")
    End Function

    Sub GetEVAMacAdressen()

        'Dit haalt alle IP adressen uit de list1 (ATVS/EVA), kijkt wat het bijbehorden MAC address is en stopt
        'deze in een EVAMAC array (AlleenUniek moet weten of een MAC adres van een EVA is of niet)

        ReDim EVAMAC(0)
        Dim EVAIP() As String
        Dim iEva%, i%

        For iEva = 0 To lstActiveATVS.Items.Count - 1
            EVAIP = Split(lstActiveATVS.Items(iEva), ",")
            Bug("Zoekmac voor " & EVAIP(i))
            Application.DoEvents()

            EVAMAC(UBound(EVAMAC)) = ConvertMAC(FindMAC(EVAIP(0)))
            If Strings.Left(EVAMAC(UBound(EVAMAC)), 6) = "0005-5" Then
                EVAMAC(UBound(EVAMAC)) = ""
            Else

                ReDim Preserve EVAMAC(UBound(EVAMAC) + 1)
            End If
        Next iEva

        If UBound(EVAMAC) <> 0 Then ReDim Preserve EVAMAC(UBound(EVAMAC) - 1) 'laastste onnodige gemaakte weer weg halen

        Bug("GetEVAMacAdressen levert " & UBound(EVAMAC) & " adressen op.")
    End Sub

    Function GetPortInfoAllSwitches() As String

        Dim nu As Date = Now()

        'Connect to the switch(es), login and retrieve info
        'Info is put into into PoortINFO() array bij the TCP_SW Data_Arrival event

        Dim i%, BeginTijd$, EindTijd$
        Dim ARPRegels() As String
        Dim X%
        BeginTijd = nu
        'Create the neccessary TCP ports

        For i = 0 To UBound(POE.Switch) '.items.count - 1
            'Check to see if its a 3Com or Cisco
            Dim Str2() As String

            'Str = Split(List3.items(i), "/")
            'If Str(1) = KIND_SW_CISCO Then
            If POE.Switch(i).KIND = KIND_SW_CISCO Then
                Bug("Going to get Cisco poort info")
                'De info die we terug krijgen zetten we in poortinfo - global variable, zo lets emply it first.
                POORTINFO(i) = ""
                ARPRegels = CiscoGetPoortInfo(POE.Switch(i).IP)

                If Not ARPRegels Is Nothing Then
                    For X = 0 To UBound(ARPRegels)
                        Bug("analising " & ARPRegels(X))

                        If InStr(1, ARPRegels(X), "/") Then 'Lines that have info about a interface are shown as x/0/1 - we look for the / to find these lines.
                            ARPRegels(X) = LeaveOnlySingleSpaces(ARPRegels(X)) ' Replace "space space" with "space" (recursively)
                            Str2 = Split(ARPRegels(X), " ")
                            'Bug($"This string has {Str2.Count} members")
                            If Str2.Count = 5 Then
                                'We willen alleen regels met Fa -Fast Ethernet poorten, Gigabit poorten beginnen ook bij 1 nl.
                                If InStr(1, Str2(4), "fa", vbTextCompare) Then
                                    'Bug("Add fa")
                                    POORTINFO(i) = POORTINFO(i) & Str2(2) & " " & "/" & Str2(4) & vbCrLf 'ARPRegels(x) & vbCrLf 'The calling sub expects POORTINFO as 1 block of string

                                ElseIf InStr(1, Str2(4), "gi", vbTextCompare) Then ' Dit zijn de Gigabit poorten, inmiddels de poorten waar de albireo's ook aan kunnen zitten.
                                    ' Bug("Add Gi")
                                    POORTINFO(i) = POORTINFO(i) & Str2(2) & " " & "/" & Str2(4) & vbCrLf 'ARPRegels(x) & vbCrLf 'The calling sub expects POORTINFO as 1 block of string
                                End If
                            Else
                                Bug($"Not reading {ARPRegels(X)}, only lines with 5 space seperated items. Str")
                            End If

                        End If
                    Next X
                End If
                Application.DoEvents()

            ElseIf POE.Switch(i).KIND = KIND_SW_HP Then
                Bug("Going to get Poortinfo HP switch " & POE.Switch(i).IP)
                'POORTINFO(i) = GetInfoFromHPSwitch(POE.Switch(i).IP, txtSwitchInlogNaam.Text, txtSwitchInlogWachtwoord.Text)
                POORTINFO(i) = GetHPInfoSNMP(POE.Switch(i).IP, txtSNMPCommunity.Text, "1.3.6.1.4.1.11.2.14.11.5.1.9.4.2.1.2") '(deze is ook promising: 1.3.6.1.4.1.11.2.14.2.10.5.1.3.1)
            ElseIf POE.Switch(i).KIND = KIND_SW_ARUBA Then
                Bug("Going to get Poortinfo Aruba switch " & POE.Switch(i).IP)
                POORTINFO(i) = ArubaFindMac(POE.Switch(i).IP, txtSwitchInlogNaam.Text, txtSwitchInlogWachtwoord1.Text)
            End If
        Next i

        Bug("All SSH info should be in.")

        'Bug("All TCP poorts are opened, waiting for all to close.")

        'nu = Now
        'StartTimeoutTimer 60000 'Zet, na n milliseconden, de gTimeout op true
        'Do While TCP_Sw.UBound > 0  'wait til the last TCP_SW is unloaded
        '    If gTimeOut Then
        '        Bug("Timeout tijdens wachten op antwoord op uitlezen alle switches.", , True)
        '        On Error Resume Next
        '        For i = TCP_Sw.UBound To 1 Step -1
        '            Unload TCP_Sw(i)
        '            If Err Then
        '                Bug(Err.Description & " tijdens unload van TCP_SW(" & i & ")", , True)
        '                Err.Clear()
        '            End If
        '        Next i
        '        Exit Do
        '    End If

        '    If DateDiff("s", nu, Now) > 60 Then
        '        Bug("Timeout tijdens wachten op antwoord op uitlezen alle switches.", , True)
        '        On Error Resume Next
        '        For i = TCP_Sw.UBound To 1 Step -1
        '            Unload TCP_Sw(i)
        '            If Err Then
        '                Bug(Err.Description & " tijdens unload van TCP_SW(" & i & ")", , True)
        '                Err.Clear()
        '            End If
        '        Next i
        '        Exit Do
        '    End If
        '    Application.DoEvents()
        'Loop

        'On Error GoTo 0

        'Bug("All TCPs should be closed and unloaded.")
        'StopTimeoutTimer()

        'StartTimeoutTimer 30000 'Zet, na n milliseconden, de gTimeout op true
        'Do While TCP_Sw(0).State <> 0  'wait untill the TCP_SW(0) - which never unloads - closes
        '    If gTimeOut Then
        '        Bug("Timeout tijdens wachten op laatste TCP_SW poort te sluiten.", , True)
        '        Exit Function
        '    End If
        '    Application.DoEvents()
        'Loop
        'Bug("TCP(0) socket should be closed.")
        'StopTimeoutTimer()

        'In case of 3Com switches:
        'All TCP_SW connections (in the data arrival event) have put the switch ARP table
        'into POORTINFO(x) string as one big block of string lines
        'The calling Sub can now do what it wants with this data.
        EindTijd = Now()
        Bug("Dit duurde " & DateAdd("s", (DateDiff("s", BeginTijd, EindTijd)), "0:00:00"))
        Return ""
    End Function

    Function ConvertMAC(IncomingMAC As String) As String
        'Zet MAC adres formaat 00-25-6E-10-00-01 in 0025-6E10-0001
        If IncomingMAC <> "" Then
            Return Mid(IncomingMAC, 1, 2) & Mid(IncomingMAC, 4, 2) & "-" & Mid(IncomingMAC, 7, 2) & Mid(IncomingMAC, 10, 2) & "-" & Mid(IncomingMAC, 13, 2) & Mid(IncomingMAC, 16, 2)
        Else
            Return ""
        End If
    End Function

    Sub SetTab(WelkeTab As Integer, Optional TerugZetten As Boolean = False)

        If TerugZetten Then
            TabControl1.SelectTab(VorigeTab)

        Else
            VorigeTab = TabControl1.SelectedIndex
            TabControl1.SelectTab(WelkeTab)
        End If

        If WelkeTab = 3 Then 'we willen de debug tab tonen
            RTF.SelectionStart = Len(RTF.Text)
            RTF.Focus()
        End If


        Select Case WelkeTab
            Case 0
                SetColor(btnTab0)
            Case 1
                SetColor(btnTab1)
            Case 2
                SetColor(btnTab2)
            Case 3
                SetColor(btnTab3)

        End Select

    End Sub

    'Public Sub FrmBug(BugInfo As clsBug)
    '    Try
    '        If Me.InvokeRequired Then
    '            'Dim args() As clsBug = {BugInfo}
    '            'Me.Invoke(New Action(Of Object)(AddressOf FrmBug), args)
    '            Me.Invoke(Sub() FrmBug(BugInfo))
    '            Return
    '        End If

    '        Dim Pad As String
    '        Dim LogPathExists As Boolean

    '        Pad = GetSetting("PoE-NET", "Settings", "txtLogPath", "C:\vbz")

    '        'If Pad = "" Then 'The settings have not yet been applied
    '        '    Pad = "C:\vbz\"
    '        'End If

    '        If BugInfo.OnlyInRTF = False Then
    '            If Dir(Pad, vbDirectory) = "" Then
    '                LogPathExists = MakePath(Pad)
    '            Else
    '                LogPathExists = True
    '            End If
    '        End If


    '        If Strings.Right(Pad, 1) <> "\" Then Pad = Pad & "\"
    '        If LogPathExists Then LogFile = Pad & "PoE Bewaking_" & Format(Now, "dd") & ".htm"

    '        Debug.Print(BugInfo.Wat)

    '        If AboutFormLoaded Then
    '            frmAbout.lblInfo.Text = BugInfo.Wat
    '            frmAbout.lblInfo.Refresh()
    '        End If

    '        If BugInfo.VetteFout Then
    '            Aantalfouten = Aantalfouten + 1
    '            lblAantalFouten.Text = "Aantal fouten: " & Aantalfouten
    '        End If
    '        If Not chkDetail Is Nothing Then
    '            If (BugInfo.DetailInfo = True) And (chkDetail.Checked = False) Then Exit Sub
    '        End If


    '        'If we have an ESC character in the text, then we should clean it up
    '        If InStr(1, BugInfo.Wat, Chr(27)) <> 0 Then BugInfo.Wat = DeAnsi(BugInfo.Wat)

    '        If Not BugInfo.OnlyInRTF Then
    '            If LogPathExists Then
    '                If BugInfo.VetteFout Then
    '                    HTMLog("POE_FOUT: " & BugInfo.Wat, FontStyle.Bold, Color.Red, , , Pad & "PoE Bewaking", True)
    '                    HTMLog("POE_FOUT: " & BugInfo.Wat, FontStyle.Bold, Color.Red, , , Pad & "Fout", True)
    '                Else
    '                    HTMLog(BugInfo.Wat, BugInfo.Style, BugInfo.Kleur, , , Pad & "PoE Bewaking", True)
    '                End If
    '            End If
    '        End If
    '        If Not chkDebugNaarDeZaak Is Nothing Then 'This happens when bug info is send during init


    '            If Me.chkDebugNaarDeZaak.Checked Then
    '                Try
    '                    If UDPZaak.Started = False Then
    '                        UDPZaak.Start()
    '                    End If
    '                    UDPZaak.RemoteIP = txtUDPNaarDeZaak.Text
    '                    UDPZaak.RemotePort = 44445
    '                    UDPZaak.SendData(BugInfo.Wat)
    '                Catch ex As Exception
    '                    chkDebugNaarDeZaak.Checked = False
    '                    Bug(Err.Description & " toen ik een bericht naar de UDP export wou sturen. Controleer de netwerk instellingen (gateway???). Zit er een debugger te luisteren?", , True, True)

    '                End Try

    '            End If
    '        End If
    '        UpdateRTF(BugInfo.Wat, BugInfo.VetteFout, BugInfo.Kleur, BugInfo.Style)

    '        ''If Err.Number <> 0 Then
    '        '    chkDebugNaarDeZaak.Checked = False
    '        '    'Bug(Err.Description & " toen ik een bericht naar de UDP export wou sturen. Controleer de netwerk instellingen (gateway???).", , True, True) 
    '        'End If
    '        ''On Error GoTo 0

    '    Catch ex As Exception

    '    End Try


    'End Sub

    Sub UpdateRTF(Wat As String, VetteFout As Boolean, Kleur As Color, Style As FontStyle)

        If RTF.InvokeRequired Then
            RTF.Invoke(Sub() UpdateRTF(Wat, VetteFout, Kleur, Style))
            Return
        End If

        Dim SS$
        Try

            If Len(RTF.Text) > 200000 Then '300000 Then
                SS = Strings.Right(RTF.Text, 10000)
                RTF.Text = ""
                'DoEvents
                RTF.Text = SS
                RTF.AppendText(Len(RTF.Text))
            Else
                'frmPoE.Caption = "PoE Watchdog " & Len(frmPoE.RTF.Text)
            End If

            'Date / Time
            RTF.SelectionFont = New Font(RTF.SelectionFont.FontFamily, 8)
            RTF.AppendText(Now() & ": ")
            RTF.SelectionFont = New Font(RTF.SelectionFont.FontFamily, 11)

            If VetteFout Then
                RTF.SelectionColor = Color.Red
                RTF.SelectionFont = New Font(RTF.SelectionFont.FontFamily, 11, FontStyle.Bold)
            Else
                If Kleur.IsEmpty Then
                    RTF.SelectionColor = Color.Black
                Else
                    RTF.SelectionColor = Kleur
                End If

                RTF.SelectionFont = New Font(RTF.SelectionFont.FontFamily, 11, Style)
            End If

            RTF.AppendText(Wat & vbCrLf)
            RTF.ScrollToCaret()
        Catch ex As Exception
            Debug.Print("!!!!!!!!!!!!!!!!!!!!! FOUT TIJDENS UPDATE RTF!! " & ex.Message)
        End Try

    End Sub
    ''' <summary>
    ''' Pas de kleur van de Albireo in de ListView aan, false = zwart, true is rood
    ''' </summary>
    ''' <param name="LVindex"></param>
    ''' <param name="Aan"></param>
    Sub ChangeLVColor(LVindex As Integer, Aan As Boolean)

        If LVindex <> -1 Then '0 zou wel een voor kunnen komen als de MAP niet geinit is.De LV is 1 based (1 to x)
            If Aan Then
                If lvUnits.Items(LVindex).ForeColor <> Color.Red Then lvUnits.Items(LVindex).ForeColor = Color.Red
                If lvUnits.Items(LVindex).Font.Bold <> True Then lvUnits.Items(LVindex).Font = New Font(lvUnits.Items(LVindex).Font, FontStyle.Bold)
            Else
                If lvUnits.Items(LVindex).ForeColor <> Color.Black Then lvUnits.Items(LVindex).ForeColor = Color.Black
                If lvUnits.Items(LVindex).Font.Bold <> False Then lvUnits.Items(LVindex).Font = New Font(lvUnits.Items(LVindex).Font, FontStyle.Regular)
            End If
        End If

    End Sub

    '''' <summary>
    '''' This converts the four parameters that frmBug needs (because it can be Invoked) into one cls
    '''' </summary>
    '''' <param name="Wat"></param>
    '''' <param name="DetailInfo"></param>
    '''' <param name="VetteFout"></param>
    '''' <param name="OnlyInRTF"></param>
    '''' <remarks></remarks>
    '<DebuggerStepThrough>
    'Sub Bug(Wat As String, Optional DetailInfo As Boolean = False, Optional VetteFout As Boolean = False, Optional OnlyInRTF As Boolean = False, Optional Kleur As Color = Nothing, Optional Style As FontStyle = FontStyle.Regular)
    '    Try
    '        Dim BugInfo As New clsBug
    '        BugInfo.Wat = Wat
    '        BugInfo.DetailInfo = DetailInfo
    '        BugInfo.VetteFout = VetteFout
    '        BugInfo.OnlyInRTF = OnlyInRTF
    '        BugInfo.Kleur = Kleur
    '        BugInfo.Style = Style
    '        FrmBug(BugInfo)
    '    Catch ex As Exception
    '        Debug.Print("****************  Fout tijdens de 'Bug routine': " & ex.Message)
    '    End Try

    'End Sub



    Private Sub NotifyIcon1_MouseClick(sender As Object, e As MouseEventArgs)
        Me.WindowState = FormWindowState.Normal
        Me.Visible = True
    End Sub

    Private Sub mnuShutdown_Click(sender As Object, e As EventArgs) Handles mnuShutdown.Click
        Application.Exit()
    End Sub

    Private Sub Command14_Click(sender As Object, e As EventArgs) Handles Command14.Click
        SaveCFGFile(True)
        MsgBox("Opgeslagen.", vbInformation, "Configuratie")
    End Sub

    Private Sub cmdDoTheMagic_Click(sender As Object, e As EventArgs) Handles cmdDoTheMagic.Click
        b(" cmdDoTheMagic_Click ")
        'Do the magic cmdButton

        Dim Poorten() As String, f%
        Dim WeerAanzetten As Boolean 'Als de herstart vink aan stond zetten we hem na afloop weer aan

        DoTheMagicBezig = True 'Dit houdt alle alive berichten tegen totdat we klaar zijn. Ik wis de UDT, waardoor de MAP niet meer klopt.

        'Eerst kijken of er IP addressen in de switches lijst staat, en of er AIP's in de lijst staan. Dan vragen of we door willen gaan.
        If lstPoESwitches.Items.Count = 0 Then
            If MsgBox("Ik heb geen PoE switches in mijn lijst! Dit houdt in dat we geen bewaking op de Albireo's kunnen doen. Weet jij zeker dat je de magic wil doen?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Toch doorgaan?") = MsgBoxResult.No Then
                DoTheMagic(False)
                DoTheMagicBezig = False
                Exit Sub
            End If
        End If

        If MsgBox("Dit gaat ALLES opnieuw configureren." & vbCrLf & vbCrLf &
                  "Hiermee WIS ik de oude lijst en creeer ik een nieuwe met de units " &
                  "die op dit moment actief zijn." & vbCrLf & "Hiermee kan je units kwijt raken in de PoE beheer." &
                  vbCrLf & vbCrLf & "Gewoon doorgaan?", vbYesNo + vbQuestion, "Doorgaan?") = vbNo Then
            DoTheMagicBezig = False
            Exit Sub
        End If

        '    If lstAIP.items.count <> List4.items.count Then
        '        If MsgBox("De Albireo IP aktieve-units lijst is niet gelijk aan de te importeren AIP lijst." & _
        '        vbCrLf & vbCrLf & "Zal ik de Albireo IPs in de aktieve lijst importeren?", vbYesNo + vbQuestion) = vbYes Then
        '            Command13_Click
        '        End If
        '    End If

        SetTab(3)
        RTF.SelectionStart = Len(RTF.Text)
        lstKomtWelBinnen.Items.Clear()

        Bug("De magic dan maar beginnen: zet de bewaking even uit.",,,, Color.Red, FontStyle.Bold)
        If chkEnableRestart.Checked = True Then WeerAanzetten = True
        chkEnableRestart.Checked = 0

        Dim i%, j%
        Dim Str() As String

        RTF.Focus()
        Bug("Wis de oude PoE Unit lijst door de redim")
        If UBound(AIP_List) + (lstFoundAlbireos.Items.Count) < 0 Then
            ReDim POE.Unit(0)
        Else
            ReDim POE.Unit(UBound(AIP_List) + (lstFoundAlbireos.Items.Count - 1))
        End If
        Bug($"De nieuwe poe.unit is {POE.Unit.Count} leden groot. ({UBound(AIP_List)} active units + {lstFoundAlbireos.Items.Count} gevonden units.)")

        Bug("een snapshot van de AIP lijst maken (active units)...")

        For i = 1 To UBound(AIP_List) '(lstAIP.items.count - 1)
            Bug("Toevoegen van active units: " & AIP_List(i).IPAdres)
            'Str = Split(lstAIP.items(i), ",")
            'Bug "Toevoegen:  Str(0) - het ip adress"
            'Bug "Toevoegen ip:" & Str(0)
            'Bug "ubound  = " & UBound(POE.Unit)
            POE.Unit(j).IPAddress = AIP_List(i).IPAdres  'alleen het IP adres
            'Bug "toevoegen de kind 1"
            POE.Unit(j).KIND = Albireo_IP
            j = j + 1
        Next i

        Bug("een snapshot van de 'gevonden AIP lijst' maken...")
        For i = 0 To lstFoundAlbireos.Items.Count - 1 '(lstAIP.items.count - 1)
            Str = Split(lstFoundAlbireos.Items(i).ToString, "/")
            Bug($"Toevoegen vanuit gevonden units: {lstFoundAlbireos.Items(i).ToString}")
            POE.Unit(j).IPAddress = Trim(Str(0))
            POE.Unit(j).MACaddress = Trim(Str(1))
            'We don't have a hardware version in the data available here, and the differences in telnet communication is between H:8 and H:9, and processor 4 only filts in H:9 and higher, 
            'so we 're misusing this to deterine which script to send to the Albireo. All other versions are 0.0 to indicate that te data is false.
            If Trim(Str(2)) = 4 Then 'ProcessorType
                POE.Unit(j).SoftwareVersionString = " Alive H:9 B:0.0 L:0.0.0.0 A:0.0.0.0"
            Else
                POE.Unit(j).SoftwareVersionString = " Alive H:8 B:0.0 L:0.0.0.0 A:0.0.0.0"
            End If

            'Bug "toevoegen de kind 1"
            POE.Unit(j).KIND = Albireo_IP
            j = j + 1
        Next i


        Bug("Gaan we de MAC adressen opzoeken voor de IP adressen in de PoE lijst")

        If chkMACuitDatabase.Checked = True Then
            Bug("MAC adressen ophalen uit de INEX 500 database")
            GetARPTabelFromINEXDatabase() 'GetARPTable get ARP info from the INEX 500 database
        End If

        'Zoek het MAC adres op voor elke IP adres
        For j = 0 To UBound(POE.Unit)
            If POE.Unit(j).MACaddress = "" Then
                Bug("Zoek het mac-adres op voor " & POE.Unit(j).IPAddress)
                Application.DoEvents()
                POE.Unit(j).MACaddress = ConvertMAC(FindMAC(POE.Unit(j).IPAddress))
                Bug("gevonden mac-adres: " & POE.Unit(j).MACaddress)
            End If
        Next j
        Bug("")

        Bug("Loop door de lijst met swiches en haal de 'display mac-addresses' op")
        Bug("Stop de complete string in POORTINFO() - 1 per switch")
        GetPortInfoAllSwitches()
        GetEVAMacAdressen()
        'We are assuming here that "i" corresponds with the poeswitches listitem and the poortinfo array
        For i = 0 To lstPoESwitches.Items.Count - 1
            'Info = GetPortInfo(List3.items(i))
            'Bug Info
            'DoEvents
            Dim SwitchIP As String = Trim(Strings.Left(lstPoESwitches.Items(i), 50))
            Bug("== Zet de binnengekomen data om een een array met macaddress;poortnr voor switch " & Trim(Strings.Left(lstPoESwitches.Items(i), 50)) & " ==")
            Poorten = Opschonen(POORTINFO(i))
            Bug("Alleen poortnummers die 1 macadress kennen worden weergegeven van " & SwitchIP)
            'Prune "Poorten" to remove all ports that have more than one entry
            Poorten = AlleenUniek2(Poorten)

            'Bug("Loop door de poorten heen om de poorten met 1 macadres te vinden")

            'Bug("Loop door de array (mac:poort) en als die uniek is (zit maar 1 macadres op)")
            'Bug("kijk dan of er een bijbehorende MAC adres in de PoE() lijst zit.")
            Bug("We hebben " & UBound(Poorten) + 1 & $" poorten van switch {SwitchIP}:")
            For x As Integer = 0 To UBound(Poorten)
                Bug(Poorten(x))
            Next


            'Loop thought these Poorten to find the MAC address in the PoE.Units list and add the port if found
            For f = 0 To UBound(Poorten)
                If Poorten(f) <> "" Then
                    Str = Split(Poorten(f), ";")
                    'If Str(1) > 24 Then Bug("check to see if this works with 3Com switches")

                    'TODO - CHECK dat dit werkt met 3Com switches
                    'If Str(1) < 51 Then 'Dit was 25 maar de nieuwe switches hebben meer als 25 poorten

                    For j = 0 To UBound(POE.Unit)
                        If UCase(POE.Unit(j).MACaddress) = UCase(Str(0)) Then
                            POE.Unit(j).PoE_Poort = Str(1)
                            POE.Unit(j).PoE_IP_Address = Trim(GetPart(lstPoESwitches.Items(i), 1, "/"))
                            Exit For
                        End If
                    Next j
                    'End If
                    'End If
                End If
            Next f
        Next i


        Bug("Namen ophalen uit de database.")
        GetDatabase()
        Bug("Units lijst vullen")
        FillListUnits()
        UpdateMap()
        'Omdat "doe the magic" nou juist alle units toegevoegd zou moeten hebben, moet deze lijst leeg. Wordt vanzelf weer gevuld.
        lstKomtWelBinnen.Items.Clear()
        If WeerAanzetten Then chkEnableRestart.Checked = 1
        SetTab(0)
        DoTheMagicBezig = False
        Bug("Klaar")
    End Sub

    Sub DoTheMagic(WelOfNiet As Boolean)
        b(" DoTheMagic ")
        DoTheMagicBezig = WelOfNiet
        If WelOfNiet Then
            lblStatus2.Text = "Bezig met de magie..."
        Else
            lblStatus2.Text = "PoE Watchdog - Versie " & Ver()
        End If
    End Sub


    Private Sub cmdZoekPoE_Click(sender As Object, e As EventArgs) Handles cmdZoekPoE.Click

        Dim txtSwitchNetwerkText As String = ""

        'Check to see if all the settings are correct before attempting the discover ========================================
        If txtSNMPCommunity.Text = "" Then
            MsgBox("Je hebt geen SNMP Community string ingevoerd. Dit is verplicht en moet overeenkomen met wat er in de switch staat.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        If txtSNMPCommunity.Text.ToLower = "public" Then
            MsgBox("Het is niet meer mogelijk 'public' als community string te gebruiken. Dit is onveilig!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        If txtSwitchNetwerk.Text <> "" Then
            'Lets test to see if they have entered a valid Network address
            If InStr(txtSwitchNetwerk.Text, "/") = 0 Then
                MsgBox("Netwerk adres moet als volgt worden ingevoerd: netwerkadres/subnet. Bijv: 10.23.1.1/24 ", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim S As String() = Split(txtSwitchNetwerk.Text, "/")
            If S.Count > 2 Then
                MsgBox("Netwerk adres moet als volgt worden ingevoerd: netwerkadres/subnet. Bijv: 10.23.1.1/24 ", MsgBoxStyle.OkOnly)
                Exit Sub
            End If

            Try
                Dim ValidIP As System.Net.IPAddress = Net.IPAddress.Parse(S(0))
            Catch ex As Exception
                MsgBox("Helaas is " & txtSwitchNetwerk.Text & " geen valide IP adres", MsgBoxStyle.OkOnly)
                Exit Sub
            End Try
            Try
                If CInt(S(1)) > 32 Or CInt(S(1)) < 0 Then
                    MsgBox("Subnetmasker moet tussen 8 en 32 zijn", MsgBoxStyle.OkOnly)
                    Exit Sub
                End If
                txtSwitchNetworkMask.Text = MSK(S(1))
                txtSwitchNetwerkText = S(0)
            Catch ex As Exception
                MsgBox("Fout tijdens gebruik van subnetmasker: " & ex.Message, MsgBoxStyle.OkOnly)
                Exit Sub
            End Try
        End If
        '========================================================================================

        'Start the UDP161 socket
        If UDP161.Started = False Then
            UDP161.Start()
        End If

        If cmdZoekPoE.Text = "Zoek PoE" Then lstPoESwitches.Items.Clear()

        'Als er niet is opgegeven waar de PoE switches zijn, dan gaan we zelf op zoek
        Dim IPAddresses() As String
        ReDim IPAddresses(0)
        If txtSwitchNetwerk.Text = "" Then
            'Zoek uit wat het IP adres is van mijn lokale netwerk adapter(s)
            'Zet deze in de IPAddresses array: 127.0.0.1/255.255.255.0
            If MsgBox("Je hebt geen netwerk opgegeven voor de PoE Switches. Wil je een Zoek PoE uitvoeren op alle beschikbare netwerk kaarten?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                FindMyIP(IPAddresses)
                If IsNothing(IPAddresses) Then
                    MsgBox("Om een of ander reden kan ik geen actieve netwerken vinden. Dan ga ik ook geen Zoek PoE uitvoeren!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
                    Exit Sub
                Else
                    If MsgBox("Ik heb de volgende netwerken gevonden:" & vbCrLf &
                              Join(IPAddresses, "      " & vbCrLf) & vbCrLf &
                              "Zal ik op deze netwerken Zoek PoE uitvoeren?", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.No Then
                        Exit Sub
                    End If
                End If
            End If

        Else 'Zoniet, dat ervanuit gaan dat de gebruiker een afwijkend IP adres netwerk heeft waar de poe switches zitten.
            ReDim IPAddresses(1)
            IPAddresses(1) = txtSwitchNetwerkText & "/" & txtSwitchNetworkMask.Text
        End If

        'Initieer de Zoek PoE knop, Abort the search by setting 'AbortPoEDetect'
        If cmdZoekPoE.Text <> "Zoek PoE" Then
            AbortPoEDetect = True
            cmdZoekPoE.Text = "Zoek PoE"
            Exit Sub
        End If

        'Zoek voor PoE switches op elke netwerk adapter. Deze For/Next stuurt een discover uit op de winsock UDP161.
        'Via de data arrival van UDP161 komen de antwoorden binnen en wordt list3 gevuld


        'Loop through all IP addresses in the network range and send the SNMP discover
        Dim ReqSNMP() As Byte = CreateGetRequest()
        Dim AddressesRange = GetIPRange(txtSwitchNetwerk.Text)

        Dim startIPInt As UInt32 = IPAddressToUInt32(Net.IPAddress.Parse(AddressesRange.startIPAddress))
        Dim endIPInt As UInt32 = IPAddressToUInt32(Net.IPAddress.Parse(AddressesRange.endIPAddress))

        For ii As UInt32 = startIPInt To endIPInt
            Dim currentIP As Net.IPAddress = UInt32ToIPAddress(ii)
            cmdZoekPoE.Text = currentIP.ToString
            UDP161.RemoteIP = currentIP.ToString
            UDP161.RemotePort = 161
            UDP161.SendData(ReqSNMP)
            Application.DoEvents()
            If AbortPoEDetect Then
                Exit For
            End If

            'PingIP(currentIP.ToString())
        Next



        'Dim i%
        'Dim Str() As String 'Needed to split IPAdress/netmask and later the IP adres and PoE type
        'Dim Str2() As String 'To split the dotted netmask


        'lstPoESwitches.Enabled = False
        'lblInfo.Text = "SNMP devices opsporen..."



        'For i = 1 To UBound(IPAddresses) 'Bijv. 10.26.1.1/255.255.255.0
        '    Str = Split(IPAddresses(i), "/")
        '    If Str(0) <> "127.0.0.1" And Str(0) <> "0.0.0.0" Then
        '        Bug("Ik ga nu IP reeks op " & Str(0) & " doorzoeken voor PoE switches.")
        '        Dim IPPart() As String
        '        IPPart = Split(IPAddresses(i), ".")
        '        Dim Teller%, Teller2%
        '        'Kijken wat de grenzen zijn van het derde octet netmasker
        '        Dim Grens As BoundsType
        '        Str2 = Split(Str(1), ".")
        '        IPReeks(IPPart(2), Str2(2), Grens)
        '        For Teller2 = Grens.BeginCijfer To Grens.EindCijfer
        '            For Teller = 1 To 254
        '                Application.DoEvents()
        '                cmdZoekPoE.Text = CStr(IPPart(0) & "." & IPPart(1) & "." & Teller2 & "." & Teller) ' "Check " & Teller
        '                If AbortPoEDetect Then
        '                    Exit For
        '                End If


        '                Dim RemoteIP As String = CStr(IPPart(0) & "." & IPPart(1) & "." & Teller2 & "." & Teller)
        '                Debug.Print(RemoteIP)
        '                UDP161.RemoteIP = RemoteIP
        '                UDP161.RemotePort = 161
        '                UDP161.SendData(ReqSNMP)

        '            Next Teller
        '        Next Teller2
        '    End If
        'Next i

        AbortPoEDetect = False
        cmdZoekPoE.Text = "Zoek PoE"
        'List3 is populated by the data arrival on the 161 UDP socket and this info is put in the poE.Switch() UDT
        RedimPoeSwitches() 'and this info is put in the poE.Switch() UDT
    End Sub


    ''' <summary>
    ''' This will look at all the SNMP devices in the PoE Switch list and check and see if they are a type that we support 
    ''' and add them to the PoE list if supported.
    ''' </summary>
    Sub RedimPoeSwitches()
        'lstPoESwitches is populated by the UDP161 receive event. It puts all SNMP responces in the list.
        Debug.Print(Now)
        lblInfo.Text = "Gevonden devices met SNMP ondersteuning..."

        'Wacht even om alle antwoorden terug te krijgen van the UDP161
        Dim Nu As Date = Now()

        Do
            If DateDiff(DateInterval.Second, Nu, Now) > 2 Then Exit Do
            Application.DoEvents()
        Loop

        Debug.Print(Now)
        Application.DoEvents()


        If lstPoESwitches.Items.Count = 0 Then
            MsgBox("Geen SNMP devices gevonden - oops!")
            ReDim POE.Switch(0)
            lblInfo.Text = ""
            lstPoESwitches.Enabled = True
        Else
            'Put the list of IP's in SwitchIPs
            Dim SwitchIPs(lstPoESwitches.Items.Count - 1) As String
            For f% = 0 To lstPoESwitches.Items.Count - 1
                SwitchIPs(f) = lstPoESwitches.Items(f).ToString
            Next

            lstPoESwitches.Items.Clear()
            Bug(SwitchIPs.Count & " switches gevonden, deze nu ondervragen.")
            For ii% = 0 To SwitchIPs.Count - 1
                Bug("Vraag " & SwitchIPs(ii))

                Dim SwitchKind As String = GetHPInfoSNMP(SwitchIPs(ii), txtSNMPCommunity.Text, "1.3.6.1.2.1.1.1")
                Dim SwitchComment As String = GetHPInfoSNMP(SwitchIPs(ii), txtSNMPCommunity.Text, "1.3.6.1.2.1.1.4")
                Dim SwitchName As String = GetHPInfoSNMP(SwitchIPs(ii), txtSNMPCommunity.Text, "1.3.6.1.2.1.1.5")

                Bug(vbCrLf & "SwitchName: " & SwitchName & "SwitchComment: " & SwitchComment & "SwitchKind: " & SwitchKind)

                'Dim SwitchInfo As String() = Split(GetHPInfoSNMP(SwitchIPs(ii), "public", "1.3.6.1.2.1.1"), vbCrLf)
                'Dim Detail As String() = Split(SwitchInfo(0), ";")
                'Dim Detail2() As String = Split(SwitchInfo(4), ";")magic

                'SwitchInfo:
                '0 - Switch Name -  HP J9138A Switch 2520-24-PoE, revision S.15.09.0029, ROM S.14.03 (/ws/swbuildm/rel_hartford_qaoff/code/build/elmo(rel_hartford_qaoff)) (Formerly ProCurve)
                '1 - ObjectId 1.3.6.1.4.1.11.2.3.7.11.95
                '2 - TimeTicks 18d 21h 19m 46s 780ms
                '3 - System Contact -  Beheer ALLEEN door Nieuw Product PWD
                '4 - System Name - PK 6 switch 161
                '5 - System location  -  In de instructieruimte PK6
                '6 - Integer32 74
                'Detail is always(1)

                'Add the default supported switches

                Bug($"Ik ga kjken of ik {SwitchKind} ondersteun.")

                If InStr(1, SwitchKind, "C2960") Then
                    Bug("C2960 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_CISCO)

                ElseIf InStr(1, SwitchKind, "Switch 2520") Then
                    Bug("Switch 2520 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_HP)

                ElseIf InStr(1, SwitchKind, "HP J9779A 2530") Then
                    Bug("HP J9779A 2530 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_HP)

                ElseIf InStr(1, SwitchKind, "HP J9780A 2530") Then
                    Bug("HP J9780A 2530 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_HP)

                ElseIf InStr(1, SwitchKind, "cisco", CompareMethod.Text) Then
                    Bug("'cisco' gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_CISCO)

                ElseIf InStr(1, SwitchKind, "Aruba R8N85A 6000", CompareMethod.Text) Then
                    Bug("ruba R8N85A 6000 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_ARUBA)

                ElseIf InStr(1, SwitchKind, "Aruba R8N87A 6000", CompareMethod.Text) Then
                    Bug("Aruba R8N87A 6000 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_ARUBA)

                ElseIf InStr(1, SwitchComment, "PWD1", CompareMethod.Text) Then
                    Bug("PWD1 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_HP)

                ElseIf InStr(1, SwitchComment, "PWD2", CompareMethod.Text) Then
                    Bug("PWD2 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_CISCO)

                ElseIf InStr(1, SwitchComment, "PWD3", CompareMethod.Text) Then
                    Bug("PWD3 gevonden, deze toevoegen.")
                    lstPoESwitchesAdd(SwitchIPs(ii) & Space(500) & "/" & KIND_SW_ARUBA)
                End If


                'Debug.Print(datagram)

                'Debug.Print("SNMP String: " & lstPoESwitches.Items(ii) & vbCrLf & SwitchInfo(1))
            Next


            ReDim POE.Switch(lstPoESwitches.Items.Count - 1)
        End If
        lstPoESwitches.Enabled = True
        lblInfo.Text = ""

        Bug("Onthou dat als je PWD1, PWD2 of PWD3 in de comment van de switch zet, forceer je herkenning als HP, CISCO of ARUBA respectievelijk.")

        If POE.Switch.Count = 0 Then
            MsgBox("Geen ondersteunde switches gevonden. :(")
        End If

        Dim i As Int16, Str As String()

        'Hier halen we de switch/type info uit de List, setten deze in de UDT.
        For i = 0 To lstPoESwitches.Items.Count - 1
            Str = Split(lstPoESwitches.Items(i), "/")

            POE.Switch(i).IP = Trim(Str(0))

            POE.Switch(i).KIND = Trim(Str(1))
            'List3.List(i) = Str(0)
            'If GetRemoteMACAddress(POE.Switch(i).IP, POE.Switch(i).MAC, "-") = True Then
            POE.Switch(i).MAC = GetRemoteMACAddress(POE.Switch(i).IP)
            If POE.Switch(i).MAC <> "" Then
                POE.Switch(i).MAC = ConvertMAC(POE.Switch(i).MAC)
            End If
            Bug("Gevonden!! PoE " & POE.Switch(i).IP & " heeft MAC " & POE.Switch(i).MAC & " en is van type: " & POE.Switch(i).KIND)
        Next i

    End Sub

    Function DeNull(Wat As String) As String

        Dim Teller%
        Dim Str As String = ""
        For Teller = 1 To Len(Wat)
            If Asc(Mid(Wat, Teller, 1)) > 31 Then Str += Mid(Wat, Teller, 1)
        Next Teller
        Return Str

    End Function

    Private Sub UDP161_Data_Arrival(sender As clsUDP, datagram As String, RemoteEndPoint As Net.IPEndPoint, remoteip As String, Data As Byte()) Handles UDP161.Data_Arrival
        b(" Sub UDP161_DataArrival(ByVal bytesTotal As Long) ")

        'Dit is de luister poort voor de discover broadcast (Switches zoeken).

        'Als er data binnen komt op deze poort dat zal er wel SNMP op zitten. Laten we vragen wat voor'n switch het is.
        Bug("Krijg dit binnen van: " & remoteip & ": " & DeNull(datagram))

        lstPoESwitchesAdd(remoteip)

    End Sub

    Sub lstPoESwitchesAdd(Wat As String)
        If lstPoESwitches.InvokeRequired Then
            Dim args As String() = {Wat}
            Me.lstPoESwitches.Invoke(New Action(Of String)(AddressOf lstPoESwitchesAdd), args)
            Return
        End If
        lstPoESwitches.Items.Add(Wat)
    End Sub

    Private Sub lstPoESwitches_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPoESwitches.SelectedIndexChanged
        If lstPoESwitches.SelectedIndex <> -1 Then
            lblInfo.Text = "Switch: " & POE.Switch(lstPoESwitches.SelectedIndex).IP & "(" & SwitchKind(POE.Switch(lstPoESwitches.SelectedIndex).KIND) & ")"
        End If

    End Sub

    Private Sub lvUnits_Click(sender As Object, e As EventArgs) Handles lvUnits.Click
        Dim Tag As Integer = Replace(lvUnits.SelectedItems.Item(0).Tag, "R", "")
        Debug.Print(lvUnits.SelectedItems.Item(0).Tag & " - " & POE.Unit(Tag).IPAddress)

        'Debug.Print(POE.Unit(0).IPAddress)


    End Sub

    Private Sub lvUnits_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles lvUnits.ColumnClick
        If e.Column = 0 Then
            lvUnits.ListViewItemSorter = New ListViewItemComparer(9)
        Else
            lvUnits.ListViewItemSorter = New ListViewItemComparer(e.Column)
        End If
        UpdateMap()

    End Sub

    Private Sub lvUnits_DoubleClick(sender As Object, e As EventArgs) Handles lvUnits.DoubleClick

        Try
            'Shell("c:\windows\sysnative\telnet.exe " & lvUnits.SelectedItems.Item(0).Text, AppWinStyle.NormalFocus)
            Shell("putty.exe " & lvUnits.SelectedItems.Item(0).Text, AppWinStyle.NormalFocus)
        Catch ex As Exception
            MsgBox("Kan 'telnet' kennelijk niet vinden :(. Is deze wel geinstalleerd?")
        End Try
    End Sub

    Private Sub lvUnits_DrawColumnHeader(sender As Object, e As DrawListViewColumnHeaderEventArgs) Handles lvUnits.DrawColumnHeader
        e.Graphics.FillRectangle(Brushes.Gray, e.Bounds)
        e.DrawText()
    End Sub

    Private Sub lvUnits_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvUnits.SelectedIndexChanged

    End Sub

    ''' <summary>
    ''' This timer monitors the ATVS list / VIDEO server list / AIP_List and resets if neccessary. Gets enabled after 15 seconds after load via timer3 before this timer enables. Runs at 1 second intervals
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tmrCheckUnits_Tick(sender As Object, e As EventArgs) Handles tmrCheckUnits.Tick


        If DoTheMagicBezig Then Exit Sub 'Als we nog met the magic bezig zijn dan even niet meer detecteren.


        'If the herstart vink uit staat dan doen we geen check
        If chkEnableRestart.Checked = False Then Exit Sub

        Dim Teller As Int16, i As Int16
        Dim Found As Boolean

        For Teller = 0 To UBound(POE.Unit) 'Loop through the PoE.Units() structure
            If chkEnableRestart.Checked = False Then Exit Sub 'Check again, the check may have been disabled during the loop.

            If POE.Unit(Teller).KIND = Albireo_IP Then  'Gaat het om een Albireo

                'Loop door de lijst van actieve AIP units
                For i = 1 To UBound(AIP_List)
                    'Regel = Split(lstAIP.List(i), ",")

                    If POE.Unit(Teller).IPAddress = AIP_List(i).IPAdres Then ' TrimIP(Regel(0)) Then 'gevonden
                        '--------
                        Found = True
                        'kijk dan hoe oud het bericht is
                        Try


                            If DateDiff("s", AIP_List(i).Tijd.ToUniversalTime, Now.ToUniversalTime) > AIPTimeOut Then   'Duurt langer dan het mag - ressetten
                                'en zet de lijst rood
                                Markeer(POE.Unit(Teller).Key)
                                Bug(String.Format("AIP {0} weg", AIP_List(i).IPAdres))
                                If ResetBezig Then
                                    Beep()
                                    Exit For
                                End If 'reset is nog bezig
                                'lstAIP.List(i) = TrimIP(Regel(0)) & "," & Now & "," & "XXX" 'zetten we een false datum in de lijst om zo tijd te geven tot ie herstart
                                AIP_List(i).Tijd = Now.ToUniversalTime
                                AIP_List(i).Versie = "XXX"
                                Dim Port(0) As String
                                Port(0) = POE.Unit(Teller).PoE_Poort
                                ResetPoort(POE.Unit(Teller).PoE_IP_Address, Port)

                                With POE.Unit(Teller)
                                    ResetLog("Auto Resetting  IPAdres: " & .IPAddress & " (" & .MACaddress & "/ " & .NAAM & ")")
                                End With
                            End If 'datediff

                        Catch ex As Exception
                            Bug("error " & ex.Message & " tijdens tijd check op " & AIP_List(i).IPAdres)
                        End Try

                        Exit For 'Wel gevonden maar tijd is nog goed
                        '---------
                    End If
                Next i

            ElseIf POE.Unit(Teller).KIND = KindEVAapp Then
                'Eva's kunnen niet gereset worden
                Found = True
            ElseIf POE.Unit(Teller).KIND = KindDiverse Then  'Gaat het om een diversen type
                'omdat 'diversen'  type units niet bewaakt worden maak ik dit soort units maar gelijk "found"
                Found = True
            End If


            If Not Found Then 'Deze Unit (PoE.Unit(Teller)) zit niet in de lijst van actieve units, maar wel in de Unit Lijst.
                'Stop
                Debug.Print(POE.Unit(Teller).IPAddress & " - Kan 'm niet vinden in de ATVS list - zullen wel geen ATVSsen zijn (AIPS?)")
                If UCase(Strings.Left(POE.Unit(Teller).MACaddress, 7)) = "0005-51" Or UCase(Strings.Left(POE.Unit(Teller).MACaddress, 7)) = "0025-6E" Then
                    Markeer(POE.Unit(Teller).Key)

                    If POE.Unit(Teller).KIND = ATVS_Client Then
                        'Als die nog niet in de lijst zit, voeg hem dan toe met een false tijd
                        If Not Found Then lstActiveATVS.Items.Add(POE.Unit(Teller).IPAddress & "," & Now() & "," & "XXX") 'zetten we een false datum in de client lijst om zo tijd te geven tot ie herstart
                    ElseIf POE.Unit(Teller).KIND = ATVS_VideoServer Then

                        lstActiveVideoServers.Items.Add(POE.Unit(Teller).IPAddress & "," & Now() & "," & "XXX") 'zetten we een false datum in de server lijst om zo tijd te geven tot ie herstart
                    ElseIf POE.Unit(Teller).KIND = Albireo_IP Then

                        'Deze Aip in de AIP_lst toevoegen, en een false datum in de AIP lijst zetten om zo tijd te geven tot ie herstart is
                        ReDim Preserve AIP_List(UBound(AIP_List) + 1)

                        AIP_List(UBound(AIP_List)).IPAdres = POE.Unit(Teller).IPAddress
                        AIP_List(UBound(AIP_List)).Tijd = Now()
                        AIP_List(UBound(AIP_List)).Versie = "XXX"
                    End If

                    Dim Port(0) As String
                    Port(0) = POE.Unit(Teller).PoE_Poort
                    ResetPoort(POE.Unit(Teller).PoE_IP_Address, Port)

                    With POE.Unit(Teller)
                        ResetLog("Auto Resetting IPAdres: " & .IPAddress & " (" & .MACaddress & "/ " & .NAAM & ")" & " (omdat deze wel in de PoE lijst zitten maar niet in de active list(s))")
                    End With

                    Debug.Print("Moest deze resetten dus")
                Else
                    'Alleen als de PoE unit lijst niet leeg is dan doorgeven dat er wat mis is
                    If UBound(POE.Unit) <> 0 Then Bug(POE.Unit(Teller).IPAddress & ": Staat wel in de UNITS lijst, maar niet in een van de actieve units lijsten. Dus moet ik hem resetten, maar het is geen ATVS, AIP, Bellatrix of Diversen. Dit mag eigenlijk nooit voorkomen.")
                End If
            End If
            Found = False
        Next Teller

    End Sub

    ''' <summary>
    ''' This delays starting the CheckUnits timer so that all Albireos have time to fill the AIP_LST 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tmrDelayCheckUnits_Tick(sender As Object, e As EventArgs) Handles tmrDelayCheckUnits.Tick

        ResetTimerDelay = ResetTimerDelay + 1
        If ResetTimerDelay >= ResetDelayMaxTime Then
            tmrCheckUnits.Enabled = True
            tmrDelayCheckUnits.Enabled = False
            cmdDoTheMagic.Enabled = True
            cmdDoTheMagic.Text = "Do the magic"
        Else
            cmdDoTheMagic.Text = "Magic in " & ResetDelayMaxTime - ResetTimerDelay

            If Environ("username") = "peter" Then

            Else
                cmdDoTheMagic.Enabled = False
            End If

        End If
    End Sub

    Sub Markeer(Key As String, Optional UIT As Boolean = False, Optional PoEUnitID As Integer = -1)

        Dim i%
        For i = 0 To lvUnits.Items.Count - 1
            If Key = "" Then
                If lvUnits.Items(i).ForeColor <> Color.Black Then lvUnits.Items(i).ForeColor = Color.Black
                If lvUnits.Items(i).Font.Bold <> False Then lvUnits.Items(i).Font = New Font(lvUnits.Font.FontFamily.Name, lvUnits.Font.Size, FontStyle.Regular)
            Else
                If lvUnits.Items(i).Tag = Key Then
                    If UIT Then
                        If lvUnits.Items(i).ForeColor <> Color.Black Then lvUnits.Items(i).ForeColor = Color.Black
                        If lvUnits.Items(i).Font.Bold <> False Then lvUnits.Items(i).Font = New Font(lvUnits.Font.FontFamily.Name, lvUnits.Font.Size, FontStyle.Regular)
                    Else
                        If lvUnits.Items(i).ForeColor <> Color.Red Then lvUnits.Items(i).ForeColor = Color.Red
                        If lvUnits.Items(i).Font.Bold <> True Then lvUnits.Items(i).Font = New Font(lvUnits.Font.FontFamily.Name, lvUnits.Font.Size, FontStyle.Bold)
                    End If
                    If PoEUnitID <> -1 Then

                        With lvUnits.Items(i)
                            If POE.Unit(PoEUnitID).SoftwareVersionString <> .SubItems(5).Text Then .SubItems(5).Text = POE.Unit(PoEUnitID).SoftwareVersionString
                            If POE.Unit(PoEUnitID).AttachedMAC <> .SubItems(6).Text Then .SubItems(6).Text = POE.Unit(PoEUnitID).AttachedMAC
                            If POE.Unit(PoEUnitID).LastSYSLogMSG <> .SubItems(7).Text Then .SubItems(7).Text = POE.Unit(PoEUnitID).LastSYSLogMSG
                        End With

                    End If
                    Exit For
                End If
            End If
        Next i
    End Sub

    Private Sub lstAdapters_Click(sender As Object, e As EventArgs) Handles lstAdapters.Click
        If lstAdapters.SelectedIndex <> -1 Then
            WinPCapAdapter = Trim(Strings.Right(lstAdapters.Text, 2))
            SaveSetting("PoE-NET", "Settings", "WinPcapAdapter", WinPCapAdapter)
            StopCapture = True
            Application.DoEvents()
            StartPCap()
        End If

    End Sub

    Private Sub lblSelectedNetworkAdapterText(strText As String)
        If lblSelectedNetworkAdapter.InvokeRequired Then
            lblSelectedNetworkAdapter.Invoke(Sub() lblSelectedNetworkAdapterText(strText))
            Return

        End If
        lblSelectedNetworkAdapter.Text = ""
        lblSelectedNetworkAdapter.Text = strText
    End Sub

    Private Sub ResetAllePoortenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResetAllePoortenToolStripMenuItem.Click

        If lstPoESwitches.Text = "" Then
            MsgBox("Er is geen switch geselecteerd.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        For Each A In POE.Switch
            If lstPoESwitches.Text.Contains(A.IP) Then
                If A.KIND = KIND_SW_CISCO Or A.KIND = KIND_SW_CISCO2 Then
                    MsgBox("Dit wordt helaas niet op Cisco switches ondersteund.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Niet ondersteund")
                    Exit Sub
                End If
            End If

        Next
        Dim Port() As String = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
        If lstPoESwitches.Text <> "" Then
            Dim IP As String() = Split(lstPoESwitches.Text, "/")
            ResetPoort(Trim(IP(0)), Port, True)
        End If
    End Sub

    Private Sub ResetPoortToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResetPoortToolStripMenuItem.Click

        If lstPoESwitches.Text = "" Then
            MsgBox("Er is geen switch geselecteerd.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        Dim myMsgBox As New clsMyInputBox

        Dim PromptStr As String = "Geef de poort(en) op:" & vbCrLf
        PromptStr += "Voorbeelden:" & vbCrLf
        PromptStr += "   1" & vbCrLf
        PromptStr += "   1,2,5,8,12" & vbCrLf
        PromptStr += "   1-6" & vbCrLf
        PromptStr += "Bij Cisco switches moet dit het volledige poort zijn, bijv. 'Gi1/0/1'" & vbCrLf

        Dim P As String = myMsgBox.ShowModal(PromptStr, "Welke poort(en) wil je herstarten?")

        'Pressing cancel sends back an empty string, don't want to do anything without a port (range)
        If P = "" Then Exit Sub

        Try
            Dim Port As String()

            If InStr(P, "-", CompareMethod.Text) <> 0 Then
                Dim FromTo As String() = Split(P, "-")
                Dim From As Integer = Trim(FromTo(0))
                Dim [To] As Integer = Trim(FromTo(1))
                ReDim Port(0)
                For i% = From To [To]
                    Port(Port.Count - 1) = i
                    ReDim Preserve Port(Port.Count)
                Next
                ReDim Preserve Port(Port.Count - 2)
            ElseIf InStr(P, ",", CompareMethod.Text) Then
                Dim Ports As String() = Split(P, ",")
                ReDim Port(0)
                For i% = 0 To Ports.Count - 1
                    Port(Port.Count - 1) = Ports(i)
                    ReDim Preserve Port(Port.Count)
                Next
                ReDim Preserve Port(Port.Count - 2)
            Else
                ReDim Port(0)
                Port(0) = P
            End If

            If lstPoESwitches.Text <> "" Then
                Dim IP As String() = Split(lstPoESwitches.Text, "/")
                ResetPoort(Trim(IP(0)), Port, True)
            Else
                MsgBox("Er is geen switch geselecteerd.")
            End If
        Catch ex As Exception
            MsgBox(ex.Message & ". Probeer het nog eens.")
        End Try

    End Sub

    'Shared Sub oldBB(wat As String)

    '    If RTF.InvokeRequired Then
    '        RTF.Invoke(Sub() oldBB(wat))
    '        Return
    '    End If
    '    RTF.SelectionFont = New Font(RTF.SelectionFont.FontFamily, 10, FontStyle.Italic)
    '    RTF.SelectionColor = Color.Gray
    '    RTF.AppendText(wat & vbCrLf)
    '    RTF.ScrollToCaret()
    '    HTMLog(wat)
    'End Sub

    Private Sub StatusUpdate()
        Dim Status$

        If chkEnableRestart.Checked = False Then
            Status = "  Auto herstarten is uitgeschakeld!"
            lblStatus.BackColor = Color.Red
            lblStatus.ForeColor = Color.White
        Else
            lblStatus.BackColor = Color.MediumSeaGreen
            lblStatus.ForeColor = Color.Black
            If ResetTimerDelay < ResetDelayMaxTime Then
                Status = "  Auto herstarten over " & ResetDelayMaxTime - ResetTimerDelay & " seconden actief!"
                ToolTip1.SetToolTip(lblStatus, "Wachten tot alle units tijd hebben gehad om zich minstens een maal te melden.")
            Else
                Status = "  Auto herstarten actief!"
                ToolTip1.SetToolTip(lblStatus, "Units worden ge POE-BOOT als ze niet op tijd reageren!!")
            End If
        End If
        If lblStatus.Text <> Status Then lblStatus.Text = Status
        'Lets also use this to enable or disable the Zoek Albireo Function
        If lstAIP.Items.Count <> 0 Then
            'We have albireos in the "Alive" list, meaning that we are in the same network as the Albireos, then we dont want to be able to manually add albireos
            GroupBox6.Enabled = False
        End If

    End Sub


    Private Sub tmrCheckStatus_Tick(sender As Object, e As EventArgs) Handles tmrStatusUpdate.Tick
        StatusUpdate() 'This
    End Sub

    Private Sub UDPaip_sckError(sender As clsUDP, ex As Exception) Handles UDPaip.sckError
        Dim FoutMSG As String = "Fout in UDPaip socket: " & ex.Message & vbCrLf &
        "                            Lokale port: " & sender._LocalPort & ", RemoteIP: " & sender.RemoteIP & ", RemotePort: " & sender.RemotePort & vbCrLf &
        "                            Alive berichten vanaf de Albireo's zal niet werken. Ik zet bewaking uit!"
        Bug(FoutMSG, , True)

        Try
            chkEnableRestart.Checked = False
        Catch ex2 As Exception

        End Try


    End Sub

    Private Sub ResetPoE(Optional Restart As Boolean = True)
        Dim MSGText As String = ""
        Dim i%, Teller%
        'I = CInt(Mid(lvUnits.SelectedItem.Key, 2, Len(lvUnits.SelectedItem.Key)))
        'Debug.Print (lvUnits. & " / "; POE.Unit(i).IPAddress)
        'Debug.Print (lvUnits.)
        If ResetBezig Then
            MsgBox("Nog bezig met een reset, probeer straks weer.")
            Exit Sub
        End If

        If Restart = True Then
            MSGText = "Reset alle geselecteerde units?"
        Else
            MSGText = "Port uitzetten voor alle geselecteerde units?"
        End If


        If MsgBox(MSGText, vbYesNo) = vbYes Then
            Me.Cursor = Cursors.WaitCursor
            SetTab(3)
            For Teller = 0 To lvUnits.Items.Count - 1
                If lvUnits.Items(Teller).Selected Then
                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                    Debug.Print(lvUnits.Items(Teller).Tag & " / " & POE.Unit(i).IPAddress)
                    Dim Ports(0) As String
                    Ports(0) = POE.Unit(i).PoE_Poort
                    ResetPoort(POE.Unit(i).PoE_IP_Address, Ports, Restart)
                    If Restart Then
                        ResetLog("Handmatig reset (someone pressed the button in PoE): " & POE.Unit(i).IPAddress & "/ " & POE.Unit(i).MACaddress & "/ " & POE.Unit(i).NAAM)
                    Else
                        ResetLog("Poort uitgezet door iemand in de AIP lijst: " & POE.Unit(i).IPAddress & "/ " & POE.Unit(i).MACaddress & "/ " & POE.Unit(i).NAAM)
                    End If

                End If
            Next Teller
        End If

        Me.Cursor = Cursors.Default



    End Sub
    Sub ResetPoEMulti(Optional Restart As Boolean = True)
        If lvUnits.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim MSGText As String = ""
        Dim i%, Teller%

        If ResetBezig Then
            MsgBox("Nog bezig met een reset, probeer straks weer.")
            Exit Sub
        End If

        If Restart = True Then
            MSGText = "Reset alle geselecteerde units?"
        Else
            MSGText = "Port uitzetten voor alle geselecteerde units?"
        End If

        Dim PoESwitchIPAddress As String = ""

        If MsgBox(MSGText, vbYesNo) = vbYes Then
            Me.Cursor = Cursors.WaitCursor
            SetTab(3)

            For Teller = 0 To lvUnits.Items.Count - 1
                If lvUnits.Items(Teller).Selected Then
                    'Now we have a selected unit, we need to verify that all switch IP's are the same. If not - request that only units on one switch can be reset at a time
                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                    Debug.Print(lvUnits.Items(Teller).Tag & " / " & POE.Unit(i).IPAddress)

                    If PoESwitchIPAddress = "" Then
                        'first time around we add the IP address of the first selected unit's PoE Switch
                        PoESwitchIPAddress = POE.Unit(i).PoE_IP_Address
                    Else
                        'Now all the other switch IP addresses need to be the same
                        If PoESwitchIPAddress <> POE.Unit(i).PoE_IP_Address Then
                            MsgBox("Om meerdere Albireo's te kunnen resetten moeten deze allemaal op dezelfde switch zitten. Selecteer Albireo's per switch a.u.b.")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    End If
                End If
            Next Teller

            'now we know that all units are on the same PoE Switch, we can build a list of ports
            Dim Ports As String()

            For Teller = 0 To lvUnits.Items.Count - 1
                If lvUnits.Items(Teller).Selected Then

                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                    Debug.Print(lvUnits.Items(Teller).Tag & " / " & POE.Unit(i).IPAddress)
                    If Ports Is Nothing Then
                        ReDim Ports(0)
                    Else
                        ReDim Preserve Ports(Ports.Count)
                    End If


                    Ports(Ports.Count - 1) = POE.Unit(i).PoE_Poort

                End If
            Next Teller

            ResetPoort(POE.Unit(i).PoE_IP_Address, Ports, Restart)

            If Restart Then
                ResetLog("Handmatig reset (someone pressed the button in PoE): SwitchIP " & PoESwitchIPAddress & "/ " & POE.Unit(i).MACaddress & "/ " & POE.Unit(i).NAAM)
            Else
                ResetLog("Poort uitgezet door iemand in de AIP lijst: " & POE.Unit(i).IPAddress & "/ " & POE.Unit(i).MACaddress & "/ " & POE.Unit(i).NAAM)
            End If
        End If

        Me.Cursor = Cursors.Default

    End Sub
    Private Sub mnuPoortUit_Click(sender As Object, e As EventArgs) Handles mnuPoortUit.Click
        ResetPoEMulti(False)
    End Sub

    Private Sub cmdResetPoE_Click(sender As Object, e As EventArgs) Handles cmdResetPoE.Click
        ResetPoEMulti()
    End Sub

    Private Sub mnuResetPoEPoort_Click(sender As Object, e As EventArgs) Handles mnuResetPoEPoort.Click
        ResetPoEMulti()
    End Sub

    Private Sub mnuFactoryReset_Click(sender As Object, e As EventArgs) Handles mnuFactoryReset.Click

        Dim i%, Teller%
        If MsgBox("Factory Reset uitvoeren op alle geselecteerde units?", vbYesNo) = vbYes Then

            If chkEnableRestart.Checked Then
                MsgBox("Ik zet auto herstarten uit als je dit soort dingen gaat doen.")
                chkEnableRestart.Checked = False
            End If

            For Teller = 0 To lvUnits.Items.Count - 1
                If lvUnits.Items(Teller).Selected Then
                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                    Dim S = GetLinuxVersion(POE.Unit(i).SoftwareVersionString)
                    Dim Filename = $"script\factoryreset - {S}.pwd"
                    If Dir(Filename) <> "" Then
                        Bug(Shell("AIPSendCommandSSH.exe " & POE.Unit(i).IPAddress & "þ" & Filename, vbNormalFocus))
                    Else
                        MsgBox($"Kan geen factory reset uitvoeren op {POE.Unit(i).IPAddress} omdat ik geen {Filename} kan vinden in de script map.")
                    End If

                    'Bug(Shell("AIPfactoryResetSSH.exe " & POE.Unit(i).IPAddress, vbNormalFocus))

                End If
            Next Teller
        End If
    End Sub

    Private Sub lblAantalFouten_Click(sender As Object, e As EventArgs) Handles lblAantalFouten.Click
        Aantalfouten = 0
        lblAantalFouten.Text = ""
    End Sub

    Sub ShutdownPoEWD(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles tbUnits.MouseUp, tbActiveUnits.MouseUp, tbDebug.MouseUp, tbInstellingen.MouseUp

    End Sub

    Private Sub mnuBooloaderUpdate_Click(sender As Object, e As EventArgs) Handles mnuBooloaderUpdate.Click

        b(" mnuBootLoaderUpdate_Click ")
        Dim i%, Teller%
        If txtTFTPIPAdres.Text = "" Then
            MsgBox("Er is geen IP-adres ingevuld voor de TFTP server (tabblad 'instellingen'). Zonder dit adres kan er niet geupdate worden.")
            Exit Sub
        End If

        If Ping(txtTFTPIPAdres.Text) = False Then
            MsgBox("De TFTP server op '" & txtTFTPIPAdres.Text & "' reageert niet op een ping. Zonder TFTP server kan niet ge-upgreed worden.")
            Exit Sub
        End If

        If MsgBox("Alle geselecteerde units voorzien van nieuwe bootloader?", vbYesNo) = vbYes Then

            If chkEnableRestart.Checked = True Then
                MsgBox("Ik zet auto herstarten uit als je dit soort dingen gaat doen.")
                chkEnableRestart.Checked = False
            End If

            For Teller = 0 To lvUnits.Items.Count - 1
                If lvUnits.Items(Teller).Selected Then
                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                    'Bug(Shell("blupSSH.exe " & POE.Unit(i).IPAddress & " " & txtTFTPIPAdres.Text & " " & POE.Unit(i).MACaddress & " " & POE.Unit(i).NAAM, vbNormalFocus))
                    'When sending a BootLoaderUpdate we need to send the bootloader script to the albireo. Each albireo can have a different file.
                    Dim S = GetLinuxVersion(POE.Unit(i).SoftwareVersionString)

                    'S = Mid(S, InStr(S, "L"))
                    'S = Replace(Trim(Split(S, "A")(0)), "L:", "")
                    ''MsgBox(S)

                    Dim Filename = $"{Application.StartupPath}\script\blup - {S}.pwd"
                    If Dir(Filename) <> "" Then
                        'Hier we have the template file, lets add tftpserver parameters and send it to the Albireo
                        Dim Template = IO.File.ReadAllText(Filename)
                        Template = Template.Replace("%tftpip%", txtTFTPIPAdres.Text)
                        IO.File.WriteAllText(Environ("temp") & "\tmppwd", Template)
                        Bug(Shell("AIPSendCommandSSH.exe " & POE.Unit(i).IPAddress & "þ" & Environ("temp") & "\tmppwd", vbNormalFocus))
                    Else
                        Dim MSG = $"{Filename} kan ik niet vinden, kan ik ook niet uploaden. Lees help."
                        MessageBox.Show(MSG, "caption",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation,
                                        MessageBoxDefaultButton.Button1,
                                        0, "https://vanbreda.atlassian.net/wiki/spaces/TOOL/pages/743276555/PWDN+-+AIP+Send+Command+SSH",
                                        "")

                    End If
                End If
            Next Teller
        End If
    End Sub

    Private Sub cmdDBResync_Click(sender As Object, e As EventArgs) Handles cmdDBResync.Click
        GetDatabase()
        FillListUnits()
    End Sub

    Sub cmdDisableRestartTmp_Click() Handles cmdDisableRestartTmp.Click

        If cmdDisableRestartTmp.Text = "Uitzetten voor:" Then
            cmdDisableRestartTmp.Text = ""
            DisableAutorestartTime = (lstDisableAutorestartTime.TopIndex + 1) * 30 * 60 '30 minute intervals * 60 seconds
            chkEnableRestart.Checked = False
        Else
            'We're already running an auto restart enable timer, switch it off
            DisableAutorestartTime = 0
            cmdDisableRestartTmp.Text = "Uitzetten voor:"
        End If
    End Sub

    ''' <summary>
    ''' This timer runs every second, and decrements the DisableAutorestartTimer if it's not 0 and, when zero, enables the check auto restart. Used for the "disable for x seconds function
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tmrEnableAutorestart_Tick(sender As Object, e As EventArgs) Handles tmrEnableAutorestart.Tick
        If DisableAutorestartTime <> 0 Then
            DisableAutorestartTime -= 1
            Dim t As TimeSpan = TimeSpan.FromSeconds(DisableAutorestartTime)
            cmdDisableRestartTmp.Text = "Weer aan over: " & t.ToString("hh\:mm\:ss")
        Else
            If cmdDisableRestartTmp.Text <> "Uitzetten voor:" Then 'Hasn.t been set yet
                If chkEnableRestart.Checked = False Then chkEnableRestart.Checked = True
                cmdDisableRestartTmp.Text = "Uitzetten voor:"
            End If
        End If
    End Sub

    Private Sub CleanupOldLogs()
        'Throw New NotImplementedException
        Debug.Print(LOGFILE)
        Dim LogFilePath As String = txtLogPath.Text
        If Strings.Right(LogFilePath, 1) <> "\" Then LogFilePath += "\"
        Try
            For Each FoundFile As String In My.Computer.FileSystem.GetFiles(LogFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.htm")
                Dim FoundFileInfo As IO.FileInfo = My.Computer.FileSystem.GetFileInfo(FoundFile)
                If DateDiff("d", FoundFileInfo.LastWriteTime, Now) >= 29 Then
                    Bug("Oude log bestand " & FoundFile & " verwijderd.")
                    My.Computer.FileSystem.DeleteFile(FoundFile)
                End If
            Next
        Catch ex As Exception
            Bug("Fout tijdens verwijderen oude logbestanden: " & ex.Message, False, True)
        End Try
    End Sub


    Private Sub cmdToevoegen_Click(sender As Object, e As EventArgs) Handles cmdToevoegen.Click
        ToevoegenNieuweUnit()
    End Sub

    Private Sub AanpassenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuAanpassenToolStripMenuItem.Click
        b(" mnuAanpassen_Click ")

        If lvUnits.SelectedItems.Count = 0 Then
            MsgBox("Als je niets selecteerd kan ik niet veel doen!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If


        Dim i%
        i = CInt(Mid(lvUnits.SelectedItems(0).Tag, 2, Len(lvUnits.SelectedItems(0).Tag)))
        With frmAanpassen

            .txtIP.Text = POE.Unit(i).IPAddress
            If IsNothing(POE.Unit(i).MACaddress) Then
                .txtMAC.Text = ""
            Else
                .txtMAC.Text = POE.Unit(i).MACaddress.ToUpper
            End If

            .txtNaam.Text = POE.Unit(i).NAAM
            .txtPoE_IP.Text = POE.Unit(i).PoE_IP_Address
            .txtPoE_Poort.Text = POE.Unit(i).PoE_Poort

            For Each opt In frmAanpassen.Controls
                If TypeOf (opt) Is RadioButton Then
                    If Strings.Right(opt.Name, 1) = POE.Unit(i).KIND Then
                        opt.checked = True
                        Exit For
                    End If

                End If

            Next

            .lblKey.Text = POE.Unit(i).Key
        End With
        frmAanpassen.ShowDialog()

    End Sub
    ''' <summary>
    ''' Updates the list of Albireo's that are no longer sending alive messages (more than 40 seconds)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub tmrAIPDood_Tick(sender As Object, e As EventArgs) Handles tmrAIPDood.Tick
        Dim i As Integer, Str() As String
        lstAIPDood.Items.Clear()
        For i = 0 To lstAIP.Items.Count - 1
            Str = Split(lstAIP.Items(i), ",")
            Try
                If DateDiff("s", Str(1), Now) > 40 Then
                    lstAIPDood.Items.Add(lstAIP.Items(i))
                End If
            Catch ex As Exception
                Bug(ex.Message & " tmrAIPDood " & Str(1))
            End Try


        Next i
        Label16.Text = "Ooit actief geweest AIP units: " & lstAIPDood.Items.Count

    End Sub

    Private Sub TabControl1_DrawItem(sender As Object, e As DrawItemEventArgs) Handles TabControl1.DrawItem


        Dim g As Graphics = e.Graphics
        'g.FillRectangle(Brushes.Gray, 0, 0, TabControl1.Width, TabControl1.Height)
        Dim myPen As New Pen(Brushes.LightGray, 10)

        'g.DrawRectangle(myPen, 0, 0, TabControl1.Width, TabControl1.Height)

        g.DrawLine(myPen, 0, 0, TabControl1.Width, 0)
        ''TextRenderer.DrawText(e.)
        ''g.Clear(Color.Gray)



        'Dim _TextBrush As Brush

        '' Get the item from the collection.
        'Dim _TabPage As TabPage = TabControl1.TabPages(e.Index)

        '' Get the real bounds for the tab rectangle.
        'Dim _TabBounds As Rectangle = TabControl1.GetTabRect(e.Index)

        'If (e.State = DrawItemState.Selected) Then
        '    ' Draw a different background color, and don't paint a focus rectangle.
        '    _TextBrush = New SolidBrush(Color.White)
        '    g.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        '    g.CompositingQuality = Drawing2D.CompositingQuality.HighSpeed
        '    g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        '    g.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
        '    g.FillRectangle(Brushes.Blue, e.Bounds)
        'Else
        '    _TextBrush = New System.Drawing.SolidBrush(e.ForeColor)
        '    g.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        '    g.CompositingQuality = Drawing2D.CompositingQuality.HighSpeed
        '    g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        '    g.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
        '    g.FillRectangle(Brushes.White, e.Bounds)
        '    e.DrawBackground()
        'End If

        '' Use our own font.
        'Dim _TabFont As New Font("Calibri", 12, FontStyle.Bold)

        '' Draw string. Center the text.
        'Dim _StringFlags As New StringFormat()
        '_StringFlags.Alignment = StringAlignment.Center
        '_StringFlags.LineAlignment = StringAlignment.Center


        'g.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        'g.CompositingQuality = Drawing2D.CompositingQuality.HighSpeed
        'g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        'g.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
        'g.DrawString(_TabPage.Text, _TabFont, _TextBrush, _TabBounds, New StringFormat(_StringFlags))
        ''g.DrawString(_TabPage.Text, Me.Font, _TextBrush, _TabBounds, New StringFormat(_StringFlags))
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnTab0.Click
        SetTab(0)
        SetColor(sender)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnTab1.Click
        SetTab(1)
        SetColor(sender)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles btnTab2.Click
        SetTab(2)
        SetColor(sender)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles btnTab3.Click
        SetTab(3)
        SetColor(sender)
    End Sub

    Sub SetColor(sender As Windows.Forms.Button)

        sender.BackColor = Color.Blue
        sender.ForeColor = Color.White
        For Each btn In Controls
            If TypeOf btn Is Button Then

                Debug.Print(btn.name)

                If Strings.Left(btn.name, 6) = "btnTab" Then
                    If btn.name <> sender.Name Then
                        btn.BackColor = Color.White
                        btn.ForeColor = Color.Black
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub frmPoE_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Right AndAlso My.Computer.Keyboard.ShiftKeyDown Then
            ShiftRightClick = True
            Me.Close()
        End If

        If e.Button = MouseButtons.Left AndAlso My.Computer.Keyboard.ShiftKeyDown Then
            frmWeb.WB.Navigate(LOGFILE)
            frmWeb.Show()
        End If

    End Sub

    Private Sub cmdFindAIP_Click(sender As Object, e As EventArgs) Handles cmdFindAIP.Click, cmdINEX500iniSturen.Click

        If cmdFindAIP.Text <> "Zoeken..." Then
            cmdFindAIP.Text = "Zoeken..."
            Exit Sub
        End If

        If sender.name = "cmdINEX500iniSturen" Then
            If MsgBox("Dit zal een telnet sessie opbouwen met alle IP adressen tussen " & txtFindAlbireoFrom.Text & " en " & txtfindAlbireoTo.Text & ", en alle gevonden devices voorzien van een nieuwe .aipconf" & vbCrLf & vbCrLf &
                      "Hoe meer IP adressen je hier geselecteerd heb, hoe meer sessies opgezet wordt. Totdat niets meer reageert!!" & vbCrLf & vbCrLf &
                      "Doen?", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.No Then
                Exit Sub
            End If
        Else
            If MsgBox("Dit zal een AIP discover alle IP adressen tussen " & txtFindAlbireoFrom.Text & " en " & txtfindAlbireoTo.Text & " uitvoeren, en alle gevonden Albireo's toevoegen aan de gevonden Albireo units lijst." & vbCrLf & vbCrLf & "Doen?", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.No Then
                Exit Sub
            End If
            SetTab(3)
        End If


        Dim FromIP As String()
        Dim ToIP As String()

        Try
            'Test and see if the given addresses can be converted to IP addresses.
            Dim FIP As System.Net.IPAddress = Net.IPAddress.Parse(txtFindAlbireoFrom.Text)
            Dim TIP As System.Net.IPAddress = Net.IPAddress.Parse(txtfindAlbireoTo.Text)

            FromIP = Split(txtFindAlbireoFrom.Text, ".")
            If FromIP.Count <> 4 Then
                MsgBox("Ik verwacht een ip adres in het formaat 10.23.2.31, en niet " & txtFindAlbireoFrom.Text)
                Exit Sub
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " als ik het 'van' IP adres probeer te gebruiken.")
            Exit Sub
        End Try

        Try
            ToIP = Split(txtfindAlbireoTo.Text, ".")
            If ToIP.Count <> 4 Then
                MsgBox("Ik verwacht een ip adres in het formaat 10.23.2.31, en niet " & txtfindAlbireoTo.Text)
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " als ik het 'tot' IP adres probeer te gebruiken.")
            Exit Sub
        End Try


        Dim FileName As String = ""
        If sender.name = "cmdINEX500iniSturen" Then
            MsgBox("Dit gaat een telnet sessie per IP adres opzetten. De InexINI script weet niet of er Gen2 of Gen3 Albireo's antwoorden, dus wacht deze niet op antwoorden. Controleer de verschillende schermen goed.")
            FileName = Environ("temp") & "\inexhost.pwd"
            If Dir(FileName) <> "" Then Kill(FileName)

            Dim S As String
            S = "root" & vbTab & "root" & vbTab & 2 & vbCrLf
            S += "echo [INEX500] > /root/configs/.aipconfig" & vbTab & "root" & vbTab & "2" & vbCrLf
            S = S + "echo IP_address=" & txtEVALogIPAdres.Text & " >> /root/configs/.aipconfig" & vbTab & "root" & vbTab & "2" & vbCrLf
            S = S + "cat /root/configs/.aipconfig" & vbTab & "root" & vbTab & "2" & vbCrLf
            S += "reboot" & vbTab & "reboot" & vbTab & "2"

            FileOpen(1, FileName, OpenMode.Binary)
            FilePut(1, S)
            FileClose(1)
        Else
            lstFoundAlbireos.Items.Clear()
        End If


        Try

            If Not UDPFindAIP.Started Then UDPFindAIP.Start()

            Dim x%, xx%, xxx%, xxxx%
            'For each and every IP address between from and to:
            For x = FromIP(0) To ToIP(0)
                For xx = FromIP(1) To ToIP(1)
                    For xxx = FromIP(2) To ToIP(2)
                        For xxxx = FromIP(3) To ToIP(3)
                            'Set the "Search" button to "Cancel" - allowing it to also function as cancel.
                            cmdFindAIP.Text = "Annuleren...(" & x & "." & xx & "." & xxx & "." & xxxx & ")"

                            'If it's the "Send INEX ini button
                            If sender.name = "cmdINEX500iniSturen" Then
                                Bug(Shell("AIPSendCommandSSH.exe " & x & "." & xx & "." & xxx & "." & xxxx & "þ" & FileName, vbNormalFocus))
                            Else 'of if it's the Find Albireo button.
                                UDPFindAIP.RemoteIP = x & "." & xx & "." & xxx & "." & xxxx
                                UDPFindAIP.RemotePort = 45200
                                Dim ENZIStr As String = FindAIPENZI("123412341234")

                                Dim i%
                                Dim S As String = ""

                                Dim ENZIData As Byte()
                                ReDim ENZIData(Len(ENZIStr) - 1)

                                For i = 1 To Len(ENZIStr)
                                    S += Hex(Asc(Strings.Mid(ENZIStr, i, 1))) & " "
                                    ENZIData(i - 1) = CByte(Asc(Strings.Mid(ENZIStr, i, 1)))
                                Next

                                'Bug(S)

                                UDPFindAIP.SendData(ENZIData)



                            End If
                            'The command button is also the cancel button
                            If cmdFindAIP.Text = "Zoeken..." Then
                                Exit Sub
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception
            MsgBox(ex.Message & " tijdens zoeken Albireo's.", MsgBoxStyle.Critical)
        End Try

        cmdFindAIP.Text = "Zoeken..."

    End Sub

    Private Sub WiFiInstellingenSturenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuWiFiInstellingenSturenToolStripMenuItem.Click
        If txtWiFiSSID.Text = "" Or txtWiFiPassword.Text = "" Then
            MsgBox("WiFi SSID en/of wachtwoord is leeg. Dat kan niet!", vbCritical + vbExclamation + vbOKOnly)
            Exit Sub
        End If


        Dim i As Integer
        For Teller = 0 To lvUnits.Items.Count - 1
            If lvUnits.Items(Teller).Selected Then
                i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                Dim S = GetLinuxVersion(POE.Unit(i).SoftwareVersionString)
                Dim Template = IO.File.ReadAllText($"script/wifi - {S}.pwd")
                Template = Template.Replace("%txtWiFiSSID%", txtWiFiSSID.Text)
                Template = Template.Replace("%txtWiFiPassword%", txtWiFiPassword.Text)
                IO.File.WriteAllText(Environ("temp") & "\tmppwd", Template)

                Bug(Shell("AIPSendCommandSSH.exe " & POE.Unit(i).IPAddress & "þ" & Environ("temp") & "\temppwd", vbNormalFocus))
            End If
        Next Teller
    End Sub

    Private Sub INEXIniSturenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuINEXIniSturenToolStripMenuItem.Click

        Dim i As Integer

        Dim MSG As String = "Hiermee ga ik met Telnet sessies de Albireo's voorzien van een nieuwe '.aipconf', waardoor deze de INEX500 kan vinden." & vbCrLf & vbCrLf
        MSG += "(Als je niet precies weet hoe dit werkt, klik dan op 'Help'.)"

        Select Case MessageBox.Show(MSG, "caption",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button3,
            0, "https://vanbreda.atlassian.net/wiki/spaces/TOOL/pages/513343489/PoE+Watchdog.NET+-+Documentatie+-+INEX500+IP+adres+sturen",
            "")

            Case Windows.Forms.DialogResult.Yes

                For Teller = 0 To lvUnits.Items.Count - 1
                    If lvUnits.Items(Teller).Selected Then
                        i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                        Dim S = GetLinuxVersion(POE.Unit(i).SoftwareVersionString)
                        Dim Template = IO.File.ReadAllText($"script/inexini - {S}.pwd")
                        Template = Template.Replace("%INEXIP%", txtEVALogIPAdres.Text)
                        IO.File.WriteAllText(Environ("temp") & "\tmppwd", Template)
                        Bug(Shell("AIPSendCommandSSH.exe " & POE.Unit(i).IPAddress & "þ" & Environ("temp") & "\tmppwd", vbNormalFocus))
                    End If
                Next Teller
            Case Windows.Forms.DialogResult.No

        End Select

    End Sub

    Private Sub ScriptNaarAlbireoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuScriptNaarAlbireoToolStripMenuItem.Click
        Dim FileName As String = Application.StartupPath & "\script\AlgScript.pwd"

        If Dir(Application.StartupPath & "\script", vbDirectory) = "" Then
            If MsgBox($"De scriptmap '{Application.StartupPath}\script ' bestaat niet - lees de handleiding in Confluence. Zoek op 'AlgScript'. {vbCrLf}{vbCrLf}Zal ik de map aanmaken? ", MsgBoxStyle.YesNo) = vbYes Then
                Try
                    IO.Directory.CreateDirectory(Application.StartupPath & "\script")
                    Process.Start("explorer.exe", Application.StartupPath & "\script")
                Catch ex As Exception
                    MsgBox("Helaas, fout tijdens aan proberen te maken van de script map: " & ex.Message)
                End Try
            End If
            Exit Sub

        End If


        If Dir(FileName) = "" Then
            MsgBox("Het scriptbestand ' " & FileName & " ' bestaat niet - lees de handleiding in Confluence. Zoek op 'AlgScript'")
            Try
                Process.Start("explorer.exe", Application.StartupPath & "\script")
            Catch ex As Exception

            End Try
            Exit Sub
        End If

        Dim i As Integer

        Dim MSG As String = "Hiermee wordt " & Application.StartupPath & "\script\AlgScript.pwd naar alle gesecteerde Albireo's gestuurd. Kies 'help' om de handleiding in Confluence te openen."
        MSG += vbCrLf & vbCrLf & "Als je niet weet waar je mee bezig bent - en of hoe gevaarlijk dit kan zijn - doe het dan NIET!"
        MSG += vbCrLf & vbCrLf & "Wil je door gaan?"

        Select Case MessageBox.Show(MSG, "caption",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button3,
            0, "https://vanbreda.atlassian.net/wiki/spaces/TOOL/pages/512753725/PoE+Watchdog+-+Algemeen+Script",
            "")

            Case Windows.Forms.DialogResult.Yes

                For Teller = 0 To lvUnits.Items.Count - 1
                    If lvUnits.Items(Teller).Selected Then
                        i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                        Bug(Shell("AIPSendCommandSSH.exe " & POE.Unit(i).IPAddress & "þ" & FileName, vbNormalFocus))
                    End If
                Next Teller
            Case Windows.Forms.DialogResult.No

        End Select

    End Sub

    Private Sub chkEnableRestart_CheckedChanged(sender As Object, e As EventArgs) Handles chkEnableRestart.CheckedChanged
        If chkEnableRestart.Checked Then
            DisableAutorestartTime = 0
            cmdDisableRestartTmp.Text = "Uitzetten voor:"
        End If
    End Sub

    Private Sub cmdAlbireoZoekHelp_Click(sender As Object, e As EventArgs) Handles cmdAlbireoZoekHelp.Click
        Process.Start("https://vanbreda.atlassian.net/wiki/spaces/TOOL/pages/501481489/PoE+Watchdog.NET+-+Documentatie#Toevoegen-niet-mogelijk")
    End Sub

    Private Sub mnuToevoegen_Click(sender As Object, e As EventArgs) Handles mnuToevoegen.Click
        ToevoegenNieuweUnit()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click, Label26.Click

    End Sub

    Private Sub txtAlbireoVLAN_TextChanged(sender As Object, e As EventArgs) Handles txtAlbireoVLAN.TextChanged
        Dim temp As String = ""

        For Each c As Char In txtAlbireoVLAN.Text
            If Char.IsNumber(c) Then
                temp += c
            End If
        Next

        txtAlbireoVLAN.Text = temp
        txtAlbireoVLAN.SelectionStart = txtAlbireoVLAN.Text.Length
    End Sub

    Private Sub ToevoegenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToevoegenToolStripMenuItem.Click
        frmAddSwitch.ShowDialog(Me)

    End Sub



    Sub VerwijderenUnit()
        'NOT THOROUGHLY TESTED
        If lvUnits.SelectedItems.Count = 0 Then
            MsgBox("Zonder een Albireo te kiezen kan ik niet veel doen.")
            Exit Sub
        End If
        If MsgBox("Wil jij daadwerkelijk de geselecteerde units verwijderen?", vbYesNo + vbQuestion) = vbYes Then

            Dim i%, Teller%
            'lvUnits.SetFocus
            'Create a temp PoE struct and store the current PoE struct there
            Dim PoETemp As PoEType
            PoETemp = POE              'Zet alles van PoE in PoETemp
            ReDim PoETemp.Unit(0)      'Wis alleen de Units (laat de .switch intact)


            'Then add all the NOT selected units to the tempPoE (Changed to 0 as lower bound, not sure why this was 1)
            'For Teller = 1 To lvUnits.Items.Count - 1
            For Teller = 0 To lvUnits.Items.Count - 1
                i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                Debug.Print(lvUnits.Items(Teller).Tag & " / " & POE.Unit(i).IPAddress)
                If lvUnits.Items(Teller).Selected = False Then
                    i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))


                    PoETemp.Unit(UBound(PoETemp.Unit)) = POE.Unit(i)
                    'Redim for the next unit - if it isn't used we'll delete it at the end
                    ReDim Preserve PoETemp.Unit(UBound(PoETemp.Unit) + 1)
                Else
                    'delete the map entry for the units about to be deleted.
                    Map(GetOctetFromIPAdres(lvUnits.Items(Teller).Text, 3), GetOctetFromIPAdres(lvUnits.Items(Teller).Text, 4)).LVindex = -1
                    Map(GetOctetFromIPAdres(lvUnits.Items(Teller).Text, 3), GetOctetFromIPAdres(lvUnits.Items(Teller).Text, 4)).PoEUDTnr = -1
                End If
            Next Teller
            'delete the last redimmed PoE.Unit
            ReDim Preserve PoETemp.Unit(UBound(PoETemp.Unit) - 1)
            'Delete old PoE.Units
            'ReDim POE.Unit(0)

            'put PoEtemp back to PoE
            POE = PoETemp

            FillListUnits()
            'Updatemap shouldnt be neccessary here, we deleted the units in the map in the for loop above
            'Yes it is. The lvUnits listview has all different indexes - nothing is the same
            UpdateMap()
        End If

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub mnuVerwijderen2_Click(sender As Object, e As EventArgs) Handles mnuVerwijderen2.Click
        VerwijderenUnit()
    End Sub

    Private Sub txtSwitchNetwerk_TextChanged(sender As Object, e As EventArgs) Handles txtSwitchNetwerk.TextChanged

    End Sub

    Private Sub mnuTelnetToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TelnetToolStripMenuItem.Click
        If lstPoESwitches.SelectedIndex = -1 Then
            MsgBox("Selecteer een een switch voordat je dit probeert!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        Try
            Shell("c:\windows\sysnative\telnet.exe " & Trim(Strings.Left(lstPoESwitches.SelectedItem.ToString, 16)), AppWinStyle.NormalFocus)
        Catch ex As Exception
            MsgBox("Kan 'telnet' kennelijk niet vinden :( Is deze wel geinstalleerd?")
        End Try
    End Sub

    Private Sub PuttyToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PuttyToolStripMenuItem.Click
        If lstPoESwitches.SelectedIndex = -1 Then
            MsgBox("Selecteer een een switch voordat je dit probeert!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        Try
            Shell("putty.exe " & Trim(Strings.Left(lstPoESwitches.SelectedItem.ToString, 16)), AppWinStyle.NormalFocus)
        Catch ex As Exception
            MsgBox("Kan 'telnet' kennelijk niet vinden :( Is deze wel geinstalleerd?")
        End Try
    End Sub

    Private Sub TelnetToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TelnetToolStripMenuItem1.Click
        Dim i%, Teller%

        For Teller = 0 To lvUnits.Items.Count - 1
            If lvUnits.Items(Teller).Selected Then
                i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                Bug(Shell("c:\windows\sysnative\telnet.exe " & POE.Unit(i).IPAddress, vbNormalFocus))
            End If
        Next Teller


    End Sub

    Private Sub PuttyToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PuttyToolStripMenuItem1.Click
        Dim i%, Teller%

        For Teller = 0 To lvUnits.Items.Count - 1
            If lvUnits.Items(Teller).Selected Then
                i = CInt(Mid(lvUnits.Items(Teller).Tag, 2, Len(lvUnits.Items(Teller).Tag)))
                Bug(Shell("putty.exe " & POE.Unit(i).IPAddress, vbNormalFocus))
            End If
        Next Teller

    End Sub

    Private Sub VerwijderenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerwijderenToolStripMenuItem.Click
        Dim i%
        If lstPoESwitches.SelectedIndex <> -1 Then
            For i = 0 To POE.Switch.Count - 1
                If Trim(Strings.Left(lstPoESwitches.SelectedItem, 20)) = POE.Switch(i).IP Then

                    DeletePoESwitch(POE.Switch(i).IP)
                    Exit For
                End If
            Next
            lstPoESwitches.Items.RemoveAt(lstPoESwitches.SelectedIndex)
        End If

    End Sub

    Sub DeletePoESwitch(IP As String)
        Dim i%
        Dim tmpPoESwitches As SwitchType()
        ReDim tmpPoESwitches(0)
        For i = 0 To POE.Switch.Count - 1
            If IP = POE.Switch(i).IP Then

            Else
                tmpPoESwitches(tmpPoESwitches.Count - 1) = POE.Switch(i)
                ReDim Preserve tmpPoESwitches(tmpPoESwitches.Count)

            End If
        Next
        ReDim Preserve tmpPoESwitches(tmpPoESwitches.Count - 2)
        POE.Switch = tmpPoESwitches
        'RedimPoeSwitches()
    End Sub

    Private Sub mnuTray_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles mnuTray.Opening

    End Sub

    Private Sub btnTab3_MouseUp(sender As Object, e As MouseEventArgs) Handles btnTab3.MouseUp
        If e.Button = MouseButtons.Right Then
            If LOGFILE IsNot Nothing Then
                Process.Start(LOGFILE)
            End If
        End If
    End Sub

    Private Sub GroupBox2_Resize(sender As Object, e As EventArgs) Handles GroupBox2.Resize
        Dim CenterPoint = (txtSwitchInlogNaam.Width / 2) + txtSwitchInlogNaam.Left
        Debug.Print(CenterPoint)
        txtSwitchInlogWachtwoord1.Left = txtSwitchInlogNaam.Left
        txtSwitchInlogWachtwoord1.Width = (txtSwitchInlogNaam.Width / 2) - 5
        txtSwitchInlogWachtwoord2.Left = CenterPoint + 5
        txtSwitchInlogWachtwoord2.Width = txtSwitchInlogWachtwoord1.Width
    End Sub
End Class
