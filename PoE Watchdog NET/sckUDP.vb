﻿Public Class sckUDPOUD
    Implements IDisposable
    Dim Client As Net.Sockets.UdpClient
    Dim Endpoint As Net.IPEndPoint
    ''' <summary>
    ''' The RemoteEndpoint is global within the class, and is filled by the remote host when receiving data in bound mode. It is returned in dthe data received event.
    ''' </summary>
    ''' <remarks></remarks>
    Dim RemoteEndpoint As New Net.IPEndPoint(Net.IPAddress.Any, 0)

    Public Event RawDatagram(sender As sckUDPOUD, datagram As Byte(), ByRef handled As Boolean)
    Public Event NewDatagram(sender As sckUDPOUD, datagram As String, RemoteEndPoint As Net.IPEndPoint)
    Property Bound As Boolean = False
    Property Encoding As System.Text.Encoding = New System.Text.UnicodeEncoding(False, False)
    Property RemoteHostIP As String = ""

    ReadOnly Property Port As Integer
        Get
            Return Endpoint.Port
        End Get
    End Property
    ReadOnly Property LocalEndpoint As Net.IPEndPoint
        Get
            Return Endpoint
        End Get
    End Property
    ReadOnly Property LocalIP As String
        Get
            Return Endpoint.Address.ToString
        End Get
    End Property

#Region "New (init class)"
    Sub New(ip As String, port As Integer)
        Try
            Endpoint = New Net.IPEndPoint(Net.IPAddress.Parse(ip), port)
            Client = New Net.Sockets.UdpClient(Endpoint)
            RemoteHostIP = ip
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
    End Sub
    Sub New(port As Integer)
        Try
            Endpoint = New Net.IPEndPoint(Net.IPAddress.Any, port)
            Client = New Net.Sockets.UdpClient(Endpoint)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
    End Sub
    Sub New(ip As String)
        Try
            Endpoint = New Net.IPEndPoint(Net.IPAddress.Parse(ip), 0)
            Client = New Net.Sockets.UdpClient(Endpoint)
            RemoteHostIP = ip
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
    End Sub
    Sub New()
        Try
            Client = New Net.Sockets.UdpClient
            Endpoint = Client.Client.LocalEndPoint
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
    End Sub
#End Region


    ''' <summary>
    ''' This binds the UDP port to start recieving data
    ''' </summary>
    ''' <remarks></remarks>
    Sub Start()
        If Not Bound Then
            Bound = True
            BeginReceive()
        End If
    End Sub

    Private Sub BeginReceive()
        Try
            Client.BeginReceive(AddressOf ReceiveCallback, "NO STATE")
        Catch ex As Exception
            Debug.Print(ex.Message & " BeginReceive")

        End Try

    End Sub

    Private Sub ReceiveCallback(ar As IAsyncResult)
        Dim datagram() As Byte = Nothing
        Try

            datagram = Client.EndReceive(ar, RemoteEndpoint)
            Debug.Print(RemoteEndpoint.Address.ToString)
            RemoteHostIP = RemoteEndpoint.Address.ToString
            If datagram IsNot Nothing Then Task.Factory.StartNew(Sub() RelayDatagram(datagram, RemoteEndpoint))

        Catch ex As Exception
            Console.WriteLine(ex.ToString)

        End Try

        If Bound = True Then BeginReceive()
    End Sub

    Private Sub RelayDatagram(datagram As Byte(), RemoteEndPoint As Net.IPEndPoint)
        Dim handled As Boolean = False
        Try
            RaiseEvent RawDatagram(Me, datagram, handled)
            If handled = True Then Return
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try

        Try
            Dim strData As String = System.Text.Encoding.UTF8.GetString(datagram)
            RaiseEvent NewDatagram(Me, strData, RemoteEndPoint)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' Sends the string to the remote UDP host, or if not supplied, to the host that received the last packet, if one exists.
    ''' </summary>
    ''' <param name="Wat">The string to be sent on the wire</param>
    ''' <param name="RemoteHost">If supplied,  data will be sent specifically to this host.</param>
    ''' <remarks></remarks>
    Sub SendData(Wat As String, Optional RemoteHost As Net.EndPoint = Nothing)
        'If RemoteHost is specified, then the data will go there.
        'If there's no RemoteHost then we'll try to send data to the remotehost from a previous receive
        Dim Data As Byte() = System.Text.Encoding.UTF8.GetBytes(Wat)

        If RemoteHost IsNot Nothing Then
            Try
                Client.SendAsync(Data, Data.Count, RemoteHost)
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        Else
            Try
                If RemoteEndpoint.Address.ToString <> "0.0.0.0" Then

                    Client.SendAsync(Data, Data.Count, RemoteEndpoint)
                Else
                    If RemoteHostIP <> "0.0.0.0" Then
                        Client.SendAsync(Data, Data.Count, New Net.IPEndPoint(Net.IPAddress.Parse(RemoteHostIP), Port))
                    Else
                        Throw New System.Exception("Cannot send data to a 0.0.0.0 ipadres. Sukkel!")
                        Debug.Print("Can't send data to 0.0.0.0")
                    End If
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        End If
    End Sub
    ''' <summary>
    ''' Sends the Data Byte array to the remote UDP host, or if not supplied, to the host that received the last packet, if one exists.
    ''' </summary>
    ''' <param name="Data">The string to be sent on the wire</param>
    ''' <param name="RemoteHost">If supplied,  data will be sent specifically to this host.</param>
    ''' <remarks></remarks>
    Sub SendData(Data As Byte(), Optional RemoteHost As Net.EndPoint = Nothing)
        'If RemoteHost is specified, then the data will go there.
        'If there's no RemoteHost then we'll try to send data to the remotehost from a previous receive


        If RemoteHost IsNot Nothing Then
            Try
                Client.SendAsync(Data, Data.Count, RemoteHost)
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        Else
            Try
                If RemoteEndpoint.Address.ToString <> "0.0.0.0" Then

                    Client.SendAsync(Data, Data.Count, RemoteEndpoint)
                Else
                    If RemoteHostIP <> "0.0.0.0" Then
                        Client.SendAsync(Data, Data.Count, New Net.IPEndPoint(Net.IPAddress.Parse(RemoteHostIP), Port))
                    Else
                        Throw New System.Exception("Cannot send data to a 0.0.0.0 ipadres. Sukkel!")
                        Debug.Print("Can't send data to 0.0.0.0")
                    End If
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        End If
    End Sub



#Region "IDisposable Support"
    Private disposedValue As Boolean
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                Bound = False
                Try
                    Client.Close()
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                End Try
            End If
            Me.Endpoint = Nothing
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
    End Sub
#End Region
End Class
