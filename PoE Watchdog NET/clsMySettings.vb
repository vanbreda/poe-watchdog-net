﻿'Imports System
'Imports System.IO
'Imports System.Web.Script.Serialization 'Reference system.web.extentions

'Public Class clsMySettings
'    Class TestProgram
'        Private Shared Sub Main(ByVal args() As String)
'            Dim settings As MySettings = MySettings.Load
'            Console.WriteLine(("Current value of 'myInteger': " + settings.myInteger))
'            Console.WriteLine("Incrementing 'myInteger'...")
'            settings.myInteger = (settings.myInteger + 1)
'            Console.WriteLine("Saving settings...")
'            settings.Save()
'            Console.WriteLine("Done.")
'            Console.ReadKey()
'        End Sub
'    End Class
'End Class

'Public Class MySettings
'    Inherits AppSettings(Of MySettings)

'    Public myString As String = "Hello World"
'    Public myInteger As Integer = 1

'End Class

'Public Class AppSettings(Of T As {New})

'    Private Const DEFAULT_FILENAME As String = "settings.json"

'    Public Overloads Sub Save(Optional ByVal fileName As String = DEFAULT_FILENAME)
'        File.WriteAllText(fileName, New JavaScriptSerializer().Serialize(Me))
'    End Sub

'    Public Overloads Shared Sub Save(ByVal pSettings As T, Optional ByVal fileName As String = DEFAULT_FILENAME)
'        File.WriteAllText(fileName, New JavaScriptSerializer().Serialize(pSettings))
'    End Sub

'    Public Shared Function Load(Optional ByVal fileName As String = DEFAULT_FILENAME) As T
'        Dim t As T = New T
'        If File.Exists(fileName) Then
'            t = New JavaScriptSerializer().Deserialize(Of T)(File.ReadAllText(fileName))
'        End If

'        Return t
'    End Function
'End Class