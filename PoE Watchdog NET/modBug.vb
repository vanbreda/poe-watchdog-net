﻿Public Module modBug
    ''' <summary>
    ''' UDBZaak is the debug port to send debug info to a debug listener, and also receive debug info from eg. AlbireosendCommand prog.
    ''' </summary>
    ''' <remarks></remarks>
    Public WithEvents UDPZaak As New clsUDP()


    ''' <summary>
    ''' Send debug info to the RTF in the application as wel as to one or more HTML files.
    ''' </summary>
    ''' <param name="Wat">The text to be shown in the logger/debugger</param>
    ''' <param name="DetailInfo">Only shown if the the "Detail" checkbox is checked.</param>
    ''' <param name="VetteFout">Add the text to the Fout HTML as wel as the normal html log (in bold)</param>
    ''' <param name="OnlyInRTF">Does not log to HTML</param>
    ''' <param name="Kleur"></param>
    ''' <param name="Style"></param>
    <DebuggerStepThrough>
    Public Sub Bug(Wat As String, Optional DetailInfo As Boolean = False, Optional VetteFout As Boolean = False, Optional OnlyInRTF As Boolean = False, Optional Kleur As Color = Nothing, Optional Style As FontStyle = FontStyle.Regular)
        Try

            Dim Pad As String
            Dim LogPathExists As Boolean

            Pad = GetSetting("PoE-NET", "Settings", "txtLogPath", "C:\vbz")

            If OnlyInRTF = False Then
                If Dir(Pad, vbDirectory) = "" Then
                    LogPathExists = MakePath(Pad)
                Else
                    LogPathExists = True
                End If
            End If


            If Strings.Right(Pad, 1) <> "\" Then Pad = Pad & "\"
            If LogPathExists Then LOGFILE = Pad & "PoE Bewaking_" & Format(Now, "dd") & ".htm"

            Debug.Print(Wat)

            If AboutFormLoaded Then
                frmAbout.lblInfo.Text = Wat
                frmAbout.lblInfo.Refresh()
            End If

            If VetteFout Then
                Aantalfouten = Aantalfouten + 1
                frmPoE.lblAantalFouten.Text = "Aantal fouten: " & Aantalfouten
            End If

            'If Not frmPoE.chkDetail Is Nothing Then
            If (DetailInfo = True) And (frmPoE.chkDetail.Checked = False) Then Exit Sub
            'End If


            'If we have an ESC character in the text, then we should clean it up
            If InStr(1, Wat, Chr(27)) <> 0 Then Wat = DeAnsi(Wat)

            If Not OnlyInRTF Then
                If LogPathExists Then
                    If VetteFout Then
                        HTMLog("POE_FOUT: " & Wat, FontStyle.Bold, Color.Red, , , Pad & "PoE Bewaking", True)
                        HTMLog("POE_FOUT: " & Wat, FontStyle.Bold, Color.Red, , , Pad & "Fout", True)
                    Else
                        HTMLog(Wat, Style, Kleur, , , Pad & "PoE Bewaking", True)
                    End If
                End If
            End If

            'If Not frmPoE.chkDebugNaarDeZaak Is Nothing Then 'This happens when bug info is send during init

            If frmPoE.chkDebugNaarDeZaak.Checked Then
                Try
                    If UDPZaak.Started = False Then
                        UDPZaak.Start()
                    End If
                    UDPZaak.RemoteIP = frmPoE.txtUDPNaarDeZaak.Text
                    UDPZaak.RemotePort = 44445
                    UDPZaak.SendData(Wat)
                Catch ex As Exception
                    frmPoE.chkDebugNaarDeZaak.Checked = False
                    Bug(Err.Description & " toen ik een bericht naar de UDP export wou sturen. Controleer de netwerk instellingen (gateway???). Zit er een debugger te luisteren?", , True, True)

                End Try

            End If
            'End If

            frmPoE.UpdateRTF(Wat, VetteFout, Kleur, Style)

        Catch ex As Exception
            Debug.Print(ex.Message & "***************************************************")
        End Try


        'Bug(Wat)
    End Sub


    Private Sub UDPZaak_Data_Arrival(sender As clsUDP, datagram As String, RemoteEndPoint As Net.IPEndPoint, remoteipaddress As String, data As Byte()) Handles UDPZaak.Data_Arrival
        If InStr(datagram, frmPoE.txtSNMPCommunity.Text) = 0 Then
            Bug(datagram)
        Else

            'Dim R As Byte() = System.Text.Encoding.ASCII.GetBytes(datagram)
            'ReqSNMP = R

            'Dim i As Integer, S As String = ""
            'For i = 1 To Len(datagram)
            '    S += (Hex(Asc(ReqPoESNMP(i))) & " ")
            'Next
            'Bug(S)
            'Dim t As String = ""
            'For i = 0 To data.Length - 1
            '    t += (Hex(Asc(data(i))) & " ")
            'Next
            'Bug(t)
        End If


    End Sub
End Module
