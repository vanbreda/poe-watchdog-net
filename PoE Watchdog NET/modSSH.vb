﻿Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports System.Text

Module modSSH
    Dim SBOutputFile As New StringBuilder
    'Dim SBErrorFile As New StringBuilder
    Dim OutputFile As String = IO.Path.GetTempPath & "PoEWDOutput.txt"
    Dim ErrorFile As String = IO.Path.GetTempPath & "PoEWDError.txt"

    Function ResetHPPort(Host As String, Username As String, Password As String, Port As String(), PortOff As Boolean, Optional PortOnAgain As Boolean = True) As Integer
        Dim PortString As String = Join(Port, ",")
        Bug("Poort(en) " & PortString & " op switch " & Host & " opnieuw starten.")

        Dim connectionInfo = New PasswordConnectionInfo(Host, 22, Username, Password)
        connectionInfo.Timeout = TimeSpan.FromSeconds(15)

        Dim client = New SshClient(connectionInfo)
        Try
            Bug("Connect to " & Host)
            client.Connect()
        Catch ex As Exception
            Bug("SSH error: Connect went wrong to " & Host & ", " & Err.Description)
            Return -1
        End Try

        'Create a client shell stream called "CLI" (random name) with size and buffer params.
        Dim stream = client.CreateShellStream("CLI", 0, 0, 0, 0, 1024)

        If Not WaitForHP("Press any key", stream) Then
            Bug("SSH error: 'Press any key' not found")
            Return -1
        End If
        Bug("Sending <space>", , , , Color.DarkGreen, FontStyle.Italic)
        stream.WriteLine(" ")
        'Make sure the line is written to the device
        stream.Flush()

        If Not WaitForHP("#", stream) Then
            Bug("SSH error: No # found")
            Return -1
        End If

        Bug("Sending 'config'", , , , Color.DarkGreen, FontStyle.Italic)
        stream.WriteLine("config")
        stream.Flush()

        If Not WaitForHP("(config)#", stream) Then
            Bug("SSH error: timeout after sending config, waiting for '(config)#'")
            Return -1
        End If

        For i% = 0 To UBound(Port)
            If PortOff Then
                'Power off command
                Bug("Sending no interface " & Port(i) & " power-over-ethernet", , , , Color.DarkGreen, FontStyle.Italic)
                stream.WriteLine("no interface " & Port(i) & " power-over-ethernet")
                stream.Flush()

                If Not WaitForHP("(config)#", stream) Then
                    Bug("SSH error: timeout waiting for '(config)#' after sending: no interface " & Port(i) & " power-over-ethernet")
                    Return -1
                End If
            End If
        Next

        For i% = 0 To UBound(Port)


            If PortOnAgain Then
                'Power off command
                Bug("Sending interface " & Port(i) & " power-over-ethernet", , , , Color.DarkGreen, FontStyle.Italic)
                stream.WriteLine("interface " & Port(i) & " power-over-ethernet")
                stream.Flush()

                If Not WaitForHP("(config)#", stream) Then
                    Bug("SSH error: timeout waiting for '(config)#' after sending: no interface " & Port(i) & " power-over-ethernet")
                    Return -1
                End If
            End If
        Next
        Bug("Closing and disconnecting from " & Host)
        stream.Close()
        client.Disconnect()
        Return 0
    End Function

    'Function GetInfoFromHPSwitch(Host As String, Username As String, Password As String) As String

    '    Dim connectionInfo = New PasswordConnectionInfo(Host, 22, Username, Password)
    '    connectionInfo.Timeout = TimeSpan.FromSeconds(5)

    '    Dim client = New SshClient(connectionInfo)
    '    Try
    '        client.Connect()
    '    Catch ex As Exception
    '        Bug("SSH connect went wrong to " & Host & ", " & Err.Description)
    '        Return ""
    '    End Try

    '    'Create a client shell stream called "CLI" (random name) with size and buffer params.
    '    Dim stream = client.CreateShellStream("CLI", 0, 0, 0, 0, 1024)

    '    If Not WaitForHP("Press any key", stream) Then
    '        Bug("SSH error: Press any key not found")
    '        Return ""
    '    End If

    '    stream.WriteLine(" ")
    '    'Make sure the line is written to the device
    '    stream.Flush()

    '    If Not WaitForHP("#", stream) Then
    '        Bug("SSH error: No # found")
    '        Return ""
    '    End If

    '    stream.WriteLine("terminal length 500")
    '    stream.Flush()

    '    If Not WaitForHP("#", stream) Then
    '        Bug("SSH error: timeout")
    '        Return ""
    '    End If

    '    stream.WriteLine("show mac-address")
    '    stream.Flush()

    '    If Not WaitForHP("#", stream) Then
    '        Bug("SSH error: timeout")
    '        Return ""
    '    End If


    '    stream.Close()
    '    client.Disconnect()
    '    Return SBOutputFile.ToString

    'End Function


    Function WaitForHP(TextToWaitFor As String, Stream As ShellStream) As Boolean
        Dim nu As Date = Now()
        Bug("waiting for: " & TextToWaitFor)
        Do
            If Stream.DataAvailable Then
                Dim S As String = Stream.Read
                Debug.Print("**" & S)
                'frmPoE.BB(DeAnsi(S))
                Bug("From switch: " & DeAnsi(S), , , , Color.DarkBlue, FontStyle.Italic)
                'SBOutputFile.Append(DeAnsi(S) & vbCrLf)

                SBOutputFile.Append(S & vbCrLf)

                Console.WriteLine("***" & S)
                If InStr(S, TextToWaitFor, CompareMethod.Text) <> 0 Then
                    'The text to wait for has been found, return true
                    Return True
                End If
            End If

            If DateDiff(DateInterval.Second, nu, Now) > 10 Then
                'Timeout and return false
                Return False
            End If
        Loop
    End Function

    Function CiscoGetPoortInfo(IP As String) As String()
        'We don't need to 'enable' the CLI (second password) to just get the mac address table. 
        Dim connectionInfo = New PasswordConnectionInfo(IP, 22, frmPoE.txtSwitchInlogNaam.Text, frmPoE.txtSwitchInlogWachtwoord1.Text)
        connectionInfo.Timeout = TimeSpan.FromSeconds(5)
        SBOutputFile.Clear()

        Dim ReturnInfo As String()
        Dim client = New SshClient(connectionInfo)
        Bug("Verbinden met " & IP)
        Try
            client.Connect()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return ReturnInfo
        End Try

        Bug("Shell stream opzetten, wachten op '>'")
        'Dim stream = client.CreateShellStream("CLI", 0, 0, 0, 0, 1024)
        Dim stream = client.CreateShellStream("CLI", 0, 0, 0, 0, 8192)

        'Dim stream2 = client.CreateShellStream("CLI2", 0, 0, 0, 0, 0)


        Dim ResponseFromSwitch As String

        Dim Responses = {">", "#"}
        Dim Answer As String = StreamRead(stream, Responses)
        If InStr(Answer, ">", CompareMethod.Text) = 0 Then
            Bug("we didn't get a '>', lets see if it's a '#'")

            'Could be that the "en" password is diabled, in which case we get a  "#" here.
            If InStr(Answer, "#", CompareMethod.Text) = 0 Then
                Bug("Connecting switch doesn't result in a prompt")
                Return ReturnInfo
            Else
                Bug("yes, it's a '#'")
                ResponseFromSwitch = "#"
            End If
        Else
            ResponseFromSwitch = ">"
        End If

        Bug("Response from switch is: " & ResponseFromSwitch)

        If Not StreamWrite("terminal length 0", stream, ResponseFromSwitch) Then Return ReturnInfo

        Dim S As String = ""

        'For some strange rease this command will not run the first time, the second time is apparently does - need to try and figure out why.
        If Not StreamWrite("show mac address-table" & vbCrLf, stream, ResponseFromSwitch, S) Then

        End If
        'If the first run fails, S doesn't contain the info, we try too rerun

        If S.Length < 100 Then
            Bug("first time show mac address-table has no result")
            If Not StreamWrite("show mac address-table" & vbCrLf, stream, ResponseFromSwitch, S) Then
                Bug("second time show mac address-table also has no result :(")
                Return ReturnInfo
            End If
        End If

        'Stop
        ReturnInfo = Split(S, vbCrLf)
        Bug("Closing connection with " & IP)
        stream.Close()
        client.Disconnect()

        Return ReturnInfo
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Host"></param>
    ''' <param name="Username"></param>
    ''' <param name="Password"></param>
    ''' <param name="Password2"></param>
    ''' <param name="Port">This needs to be the port as shown in the switch, ie: Gi1/0/2 or Fa1/0/1 due to cicso stacking support</param>
    ''' <param name="PortOff"></param>
    ''' <param name="PortOnAgain"></param>
    ''' <returns></returns>
    Function ResetCiscoPort(Host As String, Username As String, Password As String, Password2 As String, Port As String(), PortOff As Boolean, Optional PortOnAgain As Boolean = True) As Integer
        Dim PortString As String = Join(Port, ",")
        Bug("Poort(en) " & PortString & " op switch " & Host & " opnieuw starten.")

        Dim connectionInfo = New PasswordConnectionInfo(Host, 22, Username, Password)
        connectionInfo.Timeout = TimeSpan.FromSeconds(15)

        Dim client = New SshClient(connectionInfo)

        Try
            client.Connect()
        Catch ex As Exception
            Bug("SSH error: Connect went wrong to " & Host & ", " & Err.Description)
            Return -1
        End Try

        'Create a client shell stream called "CLI" (random name) with size and buffer params.
        Dim stream = client.CreateShellStream("CLI", 0, 0, 0, 0, 1024)
        Dim Responses = {">", "#"}
        Dim AnswerFromSwitch = StreamRead(stream, Responses)
        Dim PromptFromSwitch As String

        If AnswerFromSwitch.Contains(">") Then
            PromptFromSwitch = ">"
        ElseIf AnswerFromSwitch.Contains("#") Then
            PromptFromSwitch = "#"
        Else
            Bug("SSH error: 'prompt' not found after connecting to switch.")
            Return -1
        End If

        If Not StreamWrite("terminal length 0", stream, PromptFromSwitch) Then Return -1

        'Sometimes we need a password here, sometimes it is not needed. We're going to assume that the > needs the en command, the # does not.
        If PromptFromSwitch = ">" Then
            Bug($"Because Prompt from switch is {PromptFromSwitch} we need to log in with ""en""")
            If Not StreamWrite("en", stream, "password") Then Return -1
            If Not StreamWrite(Password2, stream, "#") Then Return -1
        Else
            Bug($"Because Prompt from switch is {PromptFromSwitch} we bipass ""en"" and go straight to resetting the port.")
        End If


        ''We're connected to the switch at this point. Let's first try to determine what the ports are called (fa or Gi)
        'Dim ResultString As String = ""
        'Bug("Show interface status to see if we have fa or Gi type interfaces")
        'If Not StreamWrite("show interface status", stream, "#", ResultString) Then

        'End If

        ''Here we should have the poort type, lets try to retreive the info
        'Dim InterfaceType As String = GetCiscoSwitchPortName(ResultString)
        'If InterfaceType = "" Then
        '    Bug("Interface names returned empty, defaulting to fa 0")
        '    InterfaceType = "fa 0"
        'Else
        '    Bug($"Interface names returned {InterfaceType}")
        'End If


        'Let go into config mode and reset the port(s)
        If Not StreamWrite("conf t", stream, "(config)#") Then Return -1

        For i% = 0 To UBound(Port)
            If PortOff Then

                If Not StreamWrite("int " & Port(i), stream, "(config-if)#") Then Return -1
                If Not StreamWrite("shut", stream, "(config-if)#") Then Return -1

            End If
        Next

        For i% = 0 To UBound(Port)
            If PortOnAgain Then
                If Not StreamWrite("int " & Port(i), stream, "(config-if)#") Then Return -1
                If Not StreamWrite("no shut", stream, "(config-if)#") Then Return -1
            End If
        Next
        Bug("Closing connection with " & Host)

        stream.Close()
        client.Disconnect()
        Return 0
    End Function


    Function StreamWrite(Command As String, ByRef Stream As ShellStream, Response As String, Optional ByRef ActualResult As String = "") As Boolean
        Bug("To switch: " & Command & ", waiting for: " & Response, , , , Color.DarkGreen, FontStyle.Italic)
        'Dim i As Short
        'Dim Stmp As String = ""
        'For i = 1 To Len(Command)
        '    Stmp += Hex(Asc(Mid(Command, i, 1))) & " "
        'Next
        'Bug("Command: " & Stmp, , , , Color.DarkGreen, FontStyle.Italic)

        Stream.WriteLine(Command)
        'Stream.WriteLine(Command)
        'Stream.Write(Command & vbLf)
        Application.DoEvents()
        Stream.Flush()

        Dim Responses = {Response}
        Dim S As String = StreamRead(Stream, Responses)

        If InStr(S, Response, CompareMethod.Text) = 0 Then
            Bug("SSH error: Command = " & Command & ", waiting for: " & Response & " but actual response from switch was: " & S)
            Return False
        ElseIf S = "Timeout" Then
            Bug("Timeout waiting! From switch: " & S & ", waiting for: " & Response, , , , Color.Red, FontStyle.Bold)
            Return False
        Else
            Bug("From switch: " & S & ", waiting for: " & Response, , , , Color.DarkBlue, FontStyle.Italic)
            ActualResult = S
            Return True
        End If


    End Function
    ''' <summary>
    ''' There are situations where, by cisco switches, the switch is in exec mode at login, meaning that the connect will sometimews return a >, and sometimes a #. 
    ''' We need to accoujnt for both scenarios, therefor we allow the possiblility to pass two (or more) possible responces
    ''' </summary>
    ''' <param name="Stream"></param>
    ''' <param name="ExpectedResponses"></param>
    ''' <returns></returns>
    Function StreamRead(ByRef Stream As ShellStream, ExpectedResponses As String()) As String
        Dim S As String = ""
        Dim nu As Date = Now()
        'System.Threading.Thread.Sleep(1000)
        Do While Stream.DataAvailable = False
            'wait until the first data arrives
        Loop

        Do
            If Stream.DataAvailable = True Then
                S += Stream.Read
                Bug("**" & S & "**", , , , Color.LightGray)
                System.Threading.Thread.Sleep(100)
            End If

            For Each ExpectedResponse In ExpectedResponses
                If InStr(S, ExpectedResponse, CompareMethod.Text) <> 0 Then
                    Bug("Ontvangen van stream: " & S)
                    Return S
                End If
            Next

            If DateDiff(DateInterval.Second, nu, Now) > 5 Then
                Bug("Timeout waiting for expected response from switch", , , , Color.Red)
                Return "Timeout"
            End If
        Loop
    End Function

    Function GetCiscoSwitchPortName(Wat As String) As String
        Dim S As String() = Split(Wat, vbCrLf)
        Dim i%
        For i = 0 To S.Length - 1
            If InStr(S(i), "0/1") <> 0 Then
                Dim P As String() = Split(S(i), "/")
                Return P(0)
            End If
        Next
        Return ""

    End Function
End Module
