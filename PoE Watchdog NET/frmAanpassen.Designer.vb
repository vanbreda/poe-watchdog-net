﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAanpassen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAanpassen))
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPoE_IP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNaam = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMAC = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPoE_Poort = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.optKindUnit1 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit6 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit7 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit2 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit3 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit5 = New System.Windows.Forms.RadioButton()
        Me.optKindUnit4 = New System.Windows.Forms.RadioButton()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.lblKey = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtIP
        '
        Me.txtIP.Location = New System.Drawing.Point(14, 27)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(116, 23)
        Me.txtIP.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "IP adres"
        '
        'txtPoE_IP
        '
        Me.txtPoE_IP.Location = New System.Drawing.Point(12, 70)
        Me.txtPoE_IP.Name = "txtPoE_IP"
        Me.txtPoE_IP.Size = New System.Drawing.Size(116, 23)
        Me.txtPoE_IP.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "PoE IP-adres"
        '
        'txtNaam
        '
        Me.txtNaam.Location = New System.Drawing.Point(12, 116)
        Me.txtNaam.Name = "txtNaam"
        Me.txtNaam.Size = New System.Drawing.Size(116, 23)
        Me.txtNaam.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Naam"
        '
        'txtMAC
        '
        Me.txtMAC.Location = New System.Drawing.Point(173, 27)
        Me.txtMAC.Name = "txtMAC"
        Me.txtMAC.Size = New System.Drawing.Size(116, 23)
        Me.txtMAC.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(170, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "MAC adres"
        '
        'txtPoE_Poort
        '
        Me.txtPoE_Poort.Location = New System.Drawing.Point(173, 70)
        Me.txtPoE_Poort.Name = "txtPoE_Poort"
        Me.txtPoE_Poort.Size = New System.Drawing.Size(116, 23)
        Me.txtPoE_Poort.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(170, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "PoE Poort"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 153)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Type"
        '
        'optKindUnit1
        '
        Me.optKindUnit1.AutoSize = True
        Me.optKindUnit1.Location = New System.Drawing.Point(122, 171)
        Me.optKindUnit1.Name = "optKindUnit1"
        Me.optKindUnit1.Size = New System.Drawing.Size(84, 19)
        Me.optKindUnit1.TabIndex = 2
        Me.optKindUnit1.TabStop = True
        Me.optKindUnit1.Text = "ATVS client"
        Me.optKindUnit1.UseVisualStyleBackColor = True
        Me.optKindUnit1.Visible = False
        '
        'optKindUnit6
        '
        Me.optKindUnit6.AutoSize = True
        Me.optKindUnit6.Location = New System.Drawing.Point(122, 188)
        Me.optKindUnit6.Name = "optKindUnit6"
        Me.optKindUnit6.Size = New System.Drawing.Size(44, 19)
        Me.optKindUnit6.TabIndex = 2
        Me.optKindUnit6.TabStop = True
        Me.optKindUnit6.Text = "EVA"
        Me.optKindUnit6.UseVisualStyleBackColor = True
        Me.optKindUnit6.Visible = False
        '
        'optKindUnit7
        '
        Me.optKindUnit7.AutoSize = True
        Me.optKindUnit7.Location = New System.Drawing.Point(122, 207)
        Me.optKindUnit7.Name = "optKindUnit7"
        Me.optKindUnit7.Size = New System.Drawing.Size(112, 19)
        Me.optKindUnit7.TabIndex = 2
        Me.optKindUnit7.TabStop = True
        Me.optKindUnit7.Text = "EVA Videoserver"
        Me.optKindUnit7.UseVisualStyleBackColor = True
        Me.optKindUnit7.Visible = False
        '
        'optKindUnit2
        '
        Me.optKindUnit2.AutoSize = True
        Me.optKindUnit2.Location = New System.Drawing.Point(122, 226)
        Me.optKindUnit2.Name = "optKindUnit2"
        Me.optKindUnit2.Size = New System.Drawing.Size(117, 19)
        Me.optKindUnit2.TabIndex = 2
        Me.optKindUnit2.TabStop = True
        Me.optKindUnit2.Text = "ATVS videoserver"
        Me.optKindUnit2.UseVisualStyleBackColor = True
        Me.optKindUnit2.Visible = False
        '
        'optKindUnit3
        '
        Me.optKindUnit3.AutoSize = True
        Me.optKindUnit3.Location = New System.Drawing.Point(12, 171)
        Me.optKindUnit3.Name = "optKindUnit3"
        Me.optKindUnit3.Size = New System.Drawing.Size(79, 19)
        Me.optKindUnit3.TabIndex = 5
        Me.optKindUnit3.TabStop = True
        Me.optKindUnit3.Text = "Albireo IP"
        Me.optKindUnit3.UseVisualStyleBackColor = True
        '
        'optKindUnit5
        '
        Me.optKindUnit5.AutoSize = True
        Me.optKindUnit5.Location = New System.Drawing.Point(12, 190)
        Me.optKindUnit5.Name = "optKindUnit5"
        Me.optKindUnit5.Size = New System.Drawing.Size(72, 19)
        Me.optKindUnit5.TabIndex = 6
        Me.optKindUnit5.TabStop = True
        Me.optKindUnit5.Text = "Bellatrix"
        Me.optKindUnit5.UseVisualStyleBackColor = True
        '
        'optKindUnit4
        '
        Me.optKindUnit4.AutoSize = True
        Me.optKindUnit4.Location = New System.Drawing.Point(12, 209)
        Me.optKindUnit4.Name = "optKindUnit4"
        Me.optKindUnit4.Size = New System.Drawing.Size(73, 19)
        Me.optKindUnit4.TabIndex = 7
        Me.optKindUnit4.TabStop = True
        Me.optKindUnit4.Text = "Diversen"
        Me.optKindUnit4.UseVisualStyleBackColor = True
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(214, 281)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(75, 23)
        Me.cmdOk.TabIndex = 8
        Me.cmdOk.Text = "Ok"
        Me.cmdOk.UseVisualStyleBackColor = True
        '
        'lblKey
        '
        Me.lblKey.Location = New System.Drawing.Point(173, 106)
        Me.lblKey.Name = "lblKey"
        Me.lblKey.Size = New System.Drawing.Size(116, 42)
        Me.lblKey.TabIndex = 9
        Me.lblKey.Text = "hidden lblKey to store the ley"
        Me.lblKey.Visible = False
        '
        'frmAanpassen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(301, 308)
        Me.Controls.Add(Me.lblKey)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.optKindUnit4)
        Me.Controls.Add(Me.optKindUnit5)
        Me.Controls.Add(Me.optKindUnit3)
        Me.Controls.Add(Me.optKindUnit2)
        Me.Controls.Add(Me.optKindUnit7)
        Me.Controls.Add(Me.optKindUnit6)
        Me.Controls.Add(Me.optKindUnit1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPoE_Poort)
        Me.Controls.Add(Me.txtMAC)
        Me.Controls.Add(Me.txtNaam)
        Me.Controls.Add(Me.txtPoE_IP)
        Me.Controls.Add(Me.txtIP)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAanpassen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Unit Aanpassen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtIP As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPoE_IP As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNaam As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMAC As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPoE_Poort As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents optKindUnit1 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit6 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit7 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit2 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit3 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit5 As System.Windows.Forms.RadioButton
    Friend WithEvents optKindUnit4 As System.Windows.Forms.RadioButton
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents lblKey As System.Windows.Forms.Label
End Class
