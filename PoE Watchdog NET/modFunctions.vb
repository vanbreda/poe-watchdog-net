﻿Imports System.Net.NetworkInformation
Imports System.Web.UI

Public Module modFunctions
    Public Function Ping(IPaddress As String) As Boolean
        Dim pingSender As Ping = New Ping
        ' Create a buffer of 32 bytes of data to be transmitted.
        Dim data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        Dim buffer() As Byte = System.Text.Encoding.ASCII.GetBytes(data)
        ' Wait 10 seconds for a reply.
        Dim timeout As Integer = 100
        ' Set options for transmission:
        ' The data can go through 64 gateways or routers
        ' before it is destroyed, and the data packet
        ' cannot be fragmented.
        Dim options As PingOptions = New PingOptions(64, True)
        ' Send the request.
        Dim reply As PingReply = pingSender.Send(IPaddress, timeout, buffer, options)
        If (reply.Status = IPStatus.Success) Then
            Console.WriteLine("Address: {0}", reply.Address.ToString)
            Console.WriteLine("RoundTrip time: {0}", reply.RoundtripTime)
            Console.WriteLine("Time to live: {0}", reply.Options.Ttl)
            Console.WriteLine("Don't fragment: {0}", reply.Options.DontFragment)
            Console.WriteLine("Buffer size: {0}", reply.Buffer.Length)
            Return True
        Else
            Console.WriteLine(reply.Status)
            Return False
        End If


    End Function
    ''' <summary>
    ''' This returns the Albireo's (it's IP address) position in the List View (via the map)
    ''' </summary>
    ''' <param name="IPAdres">Dooted IP address string</param>
    ''' <returns></returns>
    Public Function GetLV(IPAdres As String) As Integer
        GetLV = Map(GetOctetFromIPAdres(IPAdres, 3), GetOctetFromIPAdres(IPAdres, 4)).LVindex
    End Function
    ''' <summary>
    ''' This returns the position of the Albireo (it's IPAdres) in the PoE.Unit() structure 
    ''' </summary>
    ''' <param name="IPAdres"></param>
    ''' <returns></returns>
    Public Function GetPOEnr(IPAdres As String) As Integer
        GetPOEnr = Map(GetOctetFromIPAdres(IPAdres, 3), GetOctetFromIPAdres(IPAdres, 4)).PoEUDTnr
    End Function
    ''' <summary>
    ''' This updates the Albireo in PoE.Unit() SoftwareVersionString,  LastSYSLogMSG (current time) and Attached MAC properties
    ''' </summary>
    ''' <param name="IP"></param>
    ''' <param name="VersionStr"></param>
    Sub UpdateUDT(IP As String, VersionStr As String)

        Dim PoEUnitNr%
        PoEUnitNr = GetPOEnr(IP)
        'Could be that the incoming IP is no longer in the "Map", and then PoEUnitNr is -1. This happens when a unit is deleted that is still alive (the map is set to -1)
        If PoEUnitNr <> -1 Then
            If POE.Unit.Length > 0 Then 'When all units have been deleted, then this is 0.
                If POE.Unit(PoEUnitNr).IPAddress <> IP Then
                    PoEUnitNr = -1
                End If
            End If
        Else
                Bug($"Incoming {IP} is no longer in the MAP, cannot update.", True,, True)
        End If
        ' If InStr(1, VersionStr, "EVA", vbTextCompare) <> 0 Then Stop
        'Update the Software version and Time fields i
        Dim AttachedMAC As String = ""
        If InStr(VersionStr, "W:", CompareMethod.Text) <> 0 Then
            AttachedMAC = Mid(VersionStr, InStr(VersionStr, "W:"), Len(VersionStr))
            AttachedMAC = Mid(AttachedMAC, InStr(AttachedMAC, "("), Len(AttachedMAC) - 1)
            AttachedMAC = Replace(Replace(AttachedMAC, "(", ""), ")", "")
        End If

        If PoEUnitNr <> -1 Then

            If POE.Unit.Length > 0 Then 'This happens when all units have been deleted
                POE.Unit(PoEUnitNr).SoftwareVersionString = Trim(Replace(Replace(VersionStr, "Alive ", ""), "I: EVASystemService:", ""))

                POE.Unit(PoEUnitNr).LastSYSLogMSG = Now()
                POE.Unit(PoEUnitNr).AttachedMAC = AttachedMAC
            End If
        End If

    End Sub
    Function LeaveOnlySingleSpaces(Wat As String) As String
        Do While InStr(1, Wat, "  ") <> 0
            Wat = Replace(Wat, "  ", " ")
        Loop
        Return Wat
    End Function
    ''' <summary>
    ''' This will return the "L:xxxx" info from the Albireos software version needed in the Sendcommand scripts
    ''' </summary>
    ''' <param name="Softwareversion">String like "H:8 B:1.7 L:4.5.2.0 A:4.5.2.0"</param>
    ''' <returns>String like "4.5.2.0"</returns>
    Function GetLinuxVersion(Softwareversion As String) As String
        Dim S = Softwareversion
        S = Mid(S, InStr(S, "L"))
        S = Replace(Trim(Split(S, "A")(0)), "L:", "")
        Return S
    End Function


    Function PreviouseInstance() As Boolean
        Dim createdNew As Boolean
        Dim mutex As Threading.Mutex

        Try
            mutex = New Threading.Mutex(False, "SINGLEINSTANCE", createdNew)
            If (createdNew = False) Then
                Return True
            End If

            ' Run your application
        Catch e As Exception
            ' Unable to open the mutex for various reasons
            Bug("Unable to open the mutex for various reasons, when checking PrevInstance")
        Finally
            ' If this instance created the mutex, ensure that
            ' it's cleaned up, otherwise we can't restart the
            ' application
            If (mutex IsNot Nothing AndAlso createdNew) Then

                Try
                    mutex.ReleaseMutex()
                Catch ex As Exception

                End Try

                mutex.Dispose()
            End If
        End Try
        Return False
    End Function

    ''' <summary>
    ''' Splits 'WaarUit' using 'Delimiter' and return the 'Hoeveelste' array member (1 based)
    ''' </summary>
    ''' <param name="WaarUit"></param>
    ''' <param name="Hoeveelste"></param>
    ''' <param name="Delimiter"></param>
    ''' <returns></returns>
    ''' <remarks>I guess we should call this "GetArrayMember" in the future</remarks>
    Function GetPart(WaarUit As String, Hoeveelste As Integer, Delimiter As String) As String
        Dim Str() As String
        Str = Split(WaarUit, Delimiter)
        Return Str(Hoeveelste - 1)
    End Function
End Module
