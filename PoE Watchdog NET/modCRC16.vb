﻿Module modCRC16
    Dim CRCTab(255) As Long '               Array for single byte CRCs loaded from table
    Dim DataByte() As Byte '                Array for byte data

    Function StrToLetters(S As String) As String
        'Dit zet een string met characters om in een leesbare hex string
        'ûÔ•• wordt 3E2B0707 bijvoorbeeld

        Dim tmpS As String = ""
        Dim i As Integer
        For i = 1 To Len(S)
            tmpS = tmpS & Right("00" & Hex(Asc(Mid(S, i, 1))), 2)
        Next i
        StrToLetters = tmpS
    End Function

    Function CRC16(S As String)

        Dim TempStr As String
        Dim DataSize As Long
        Dim X As Long
        Dim Index As Long
        Dim TempLng As Integer
        Dim CRC As Long

        InitCRCTabel()

        'TempStr = Text1.Text '                             Load text box hex bytes
        TempStr = StrToLetters(S) '                         Load text box hex bytes
        DataSize = (Len(TempStr) / 2) - 1 '                 Calc nunber of pairs of chrs
        ReDim DataByte(DataSize) '                          Resize the array
        Index = 1
        For X = 0 To DataSize '                             Load array into memory
            DataByte(X) = "&h" & Mid(TempStr, Index, 2)
            Index = Index + 2
        Next

        'CRC = CLng("&h" & Text2.Text) '                    Load initial value (normally 0xFFFF)
        CRC = &H1D0F

        For X = 0 To DataSize '                             Loop through data bytes
            TempLng = ((CRC \ 256) Xor DataByte(X)) '       Shift left (>>8) XOR with data
            CRC = ((CRC * 256) And 65535) Xor CRCTab(TempLng) ' Shift right (<<8) prevent overflow, XOR with table
        Next
        'Text3.Text = Right("0000" & Hex(CRC), 4)
        Debug.Print(Strings.Right("0000" & Hex(CRC), 4))
        Dim Byte1 As String, Byte2 As String
        Byte1 = Left(Right("0000" & Hex(CRC), 4), 2)
        Byte2 = Right(Right("0000" & Hex(CRC), 4), 2)
        Byte1 = Chr("&H" & Byte1)
        Byte2 = Chr("&H" & Byte2)
        CRC16 = Byte1 & Byte2 'Right("0000" & Hex(CRC), 4)

    End Function

    'Load single byte CRC table
    Sub InitCRCTabel()
        Dim X As Long
        Dim TempStr As String = ""

        FileOpen(1, Application.StartupPath & "\CRCTAB.txt", OpenMode.Input)
        For X = 0 To 255
            Input(1, TempStr)
            CRCTab(X) = CLng(TempStr)
        Next
        FileClose(1)
    End Sub



End Module
