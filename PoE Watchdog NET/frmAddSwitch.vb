﻿Imports SnmpSharpNet

Public Class frmAddSwitch
    Dim SelectedSwitchKind As Integer

    Private Sub frmAddSwitch_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click

        If txtIP.Text <> "" Then
            Try
                Dim IP As Net.IPAddress = Nothing
                Dim i = Net.IPAddress.TryParse(txtIP.Text, IP)
                If i = True Then
                    'ReDim Preserve POE.Switch(POE.Switch.Count)
                    'POE.Switch(POE.Switch.Count - 1).KIND = KIND_SW_CISCO
                    'POE.Switch(POE.Switch.Count - 1).IP = txtIP.Text

                    frmPoE.lstPoESwitchesAdd(txtIP.Text & Space(500) & "/" & SelectedSwitchKind)

                    ReDim POE.Switch(frmPoE.lstPoESwitches.Items.Count - 1)
                    Dim ii As Integer
                    For ii = 0 To frmPoE.lstPoESwitches.Items.Count - 1
                        Dim Str() As String
                        Str = Split(frmPoE.lstPoESwitches.Items(ii), "/")

                        POE.Switch(ii).IP = Trim(Str(0))
                        POE.Switch(ii).KIND = Trim(Str(1))

                        Bug("Toegevoegd!! PoE " & POE.Switch(ii).IP & " heeft MAC " & POE.Switch(ii).MAC & " en is van type: " & POE.Switch(ii).KIND)
                    Next ii
                    Me.Close()
                Else
                    MsgBox($"{txtIP.Text} kan ik niet als IP adres gebruiken")
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub optCisco_CheckedChanged(sender As Object, e As EventArgs) Handles optCisco.CheckedChanged
        SelectedSwitchKind = KIND_SW_CISCO
    End Sub

    Private Sub optCisco2_CheckedChanged(sender As Object, e As EventArgs) Handles optCisco2.CheckedChanged
        SelectedSwitchKind = KIND_SW_CISCO2
    End Sub

    Private Sub optHP_CheckedChanged(sender As Object, e As EventArgs) Handles optHP.CheckedChanged
        SelectedSwitchKind = KIND_SW_HP
    End Sub

    Private Sub optAruba_CheckedChanged(sender As Object, e As EventArgs) Handles optAruba.CheckedChanged
        SelectedSwitchKind = KIND_SW_ARUBA
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
End Class