﻿Module modENZI


    Public eHeader As String
    Public eFooter As String
    Public gBerichtNummer As Integer

    Public LookupTable(255) As String
    Public ResponseValue(30) As String 'Reponse cijfer to text van AIP


    Sub InitLookupTable()
        LookupTable(0) = &H0&
        LookupTable(1) = &H1021&
        LookupTable(2) = &H2042&
        LookupTable(3) = &H3063&
        LookupTable(4) = &H4084&
        LookupTable(5) = &H50A5&
        LookupTable(6) = &H60C6&
        LookupTable(7) = &H70E7&
        LookupTable(8) = &H8108&
        LookupTable(9) = &H9129&
        LookupTable(10) = &HA14A&
        LookupTable(11) = &HB16B&
        LookupTable(12) = &HC18C&
        LookupTable(13) = &HD1AD&
        LookupTable(14) = &HE1CE&
        LookupTable(15) = &HF1EF&
        LookupTable(16) = &H1231&
        LookupTable(17) = &H210&
        LookupTable(18) = &H3273&
        LookupTable(19) = &H2252&
        LookupTable(20) = &H52B5&
        LookupTable(21) = &H4294&
        LookupTable(22) = &H72F7&
        LookupTable(23) = &H62D6&
        LookupTable(24) = &H9339&
        LookupTable(25) = &H8318&
        LookupTable(26) = &HB37B&
        LookupTable(27) = &HA35A&
        LookupTable(28) = &HD3BD&
        LookupTable(29) = &HC39C&
        LookupTable(30) = &HF3FF&
        LookupTable(31) = &HE3DE&
        LookupTable(32) = &H2462&
        LookupTable(33) = &H3443&
        LookupTable(34) = &H420&
        LookupTable(35) = &H1401&
        LookupTable(36) = &H64E6&
        LookupTable(37) = &H74C7&
        LookupTable(38) = &H44A4&
        LookupTable(39) = &H5485&
        LookupTable(40) = &HA56A&
        LookupTable(41) = &HB54B&
        LookupTable(42) = &H8528&
        LookupTable(43) = &H9509&
        LookupTable(44) = &HE5EE&
        LookupTable(45) = &HF5CF&
        LookupTable(46) = &HC5AC&
        LookupTable(47) = &HD58D&
        LookupTable(48) = &H3653&
        LookupTable(49) = &H2672&
        LookupTable(50) = &H1611&
        LookupTable(51) = &H630&
        LookupTable(52) = &H76D7&
        LookupTable(53) = &H66F6&
        LookupTable(54) = &H5695&
        LookupTable(55) = &H46B4&
        LookupTable(56) = &HB75B&
        LookupTable(57) = &HA77A&
        LookupTable(58) = &H9719&
        LookupTable(59) = &H8738&
        LookupTable(60) = &HF7DF&
        LookupTable(61) = &HE7FE&
        LookupTable(62) = &HD79D&
        LookupTable(63) = &HC7BC&
        LookupTable(64) = &H48C4&
        LookupTable(65) = &H58E5&
        LookupTable(66) = &H6886&
        LookupTable(67) = &H78A7&
        LookupTable(68) = &H840&
        LookupTable(69) = &H1861&
        LookupTable(70) = &H2802&
        LookupTable(71) = &H3823&
        LookupTable(72) = &HC9CC&
        LookupTable(73) = &HD9ED&
        LookupTable(74) = &HE98E&
        LookupTable(75) = &HF9AF&
        LookupTable(76) = &H8948&
        LookupTable(77) = &H9969&
        LookupTable(78) = &HA90A&
        LookupTable(79) = &HB92B&
        LookupTable(80) = &H5AF5&
        LookupTable(81) = &H4AD4&
        LookupTable(82) = &H7AB7&
        LookupTable(83) = &H6A96&
        LookupTable(84) = &H1A71&
        LookupTable(85) = &HA50&
        LookupTable(86) = &H3A33&
        LookupTable(87) = &H2A12&
        LookupTable(88) = &HDBFD&
        LookupTable(89) = &HCBDC&
        LookupTable(90) = &HFBBF&
        LookupTable(91) = &HEB9E&
        LookupTable(92) = &H9B79&
        LookupTable(93) = &H8B58&
        LookupTable(94) = &HBB3B&
        LookupTable(95) = &HAB1A&
        LookupTable(96) = &H6CA6&
        LookupTable(97) = &H7C87&
        LookupTable(98) = &H4CE4&
        LookupTable(99) = &H5CC5&
        LookupTable(100) = &H2C22&
        LookupTable(101) = &H3C03&
        LookupTable(102) = &HC60&
        LookupTable(103) = &H1C41&
        LookupTable(104) = &HEDAE&
        LookupTable(105) = &HFD8F&
        LookupTable(106) = &HCDEC&
        LookupTable(107) = &HDDCD&
        LookupTable(108) = &HAD2A&
        LookupTable(109) = &HBD0B&
        LookupTable(110) = &H8D68&
        LookupTable(111) = &H9D49&
        LookupTable(112) = &H7E97&
        LookupTable(113) = &H6EB6&
        LookupTable(114) = &H5ED5&
        LookupTable(115) = &H4EF4&
        LookupTable(116) = &H3E13&
        LookupTable(117) = &H2E32&
        LookupTable(118) = &H1E51&
        LookupTable(119) = &HE70&
        LookupTable(120) = &HFF9F&
        LookupTable(121) = &HEFBE&
        LookupTable(122) = &HDFDD&
        LookupTable(123) = &HCFFC&
        LookupTable(124) = &HBF1B&
        LookupTable(125) = &HAF3A&
        LookupTable(126) = &H9F59&
        LookupTable(127) = &H8F78&
        LookupTable(128) = &H9188&
        LookupTable(129) = &H81A9&
        LookupTable(130) = &HB1CA&
        LookupTable(131) = &HA1EB&
        LookupTable(132) = &HD10C&
        LookupTable(133) = &HC12D&
        LookupTable(134) = &HF14E&
        LookupTable(135) = &HE16F&
        LookupTable(136) = &H1080&
        LookupTable(137) = &HA1&
        LookupTable(138) = &H30C2&
        LookupTable(139) = &H20E3&
        LookupTable(140) = &H5004&
        LookupTable(141) = &H4025&
        LookupTable(142) = &H7046&
        LookupTable(143) = &H6067&
        LookupTable(144) = &H83B9&
        LookupTable(145) = &H9398&
        LookupTable(146) = &HA3FB&
        LookupTable(147) = &HB3DA&
        LookupTable(148) = &HC33D&
        LookupTable(149) = &HD31C&
        LookupTable(150) = &HE37F&
        LookupTable(151) = &HF35E&
        LookupTable(152) = &H2B1&
        LookupTable(153) = &H1290&
        LookupTable(154) = &H22F3&
        LookupTable(155) = &H32D2&
        LookupTable(156) = &H4235&
        LookupTable(157) = &H5214&
        LookupTable(158) = &H6277&
        LookupTable(159) = &H7256&
        LookupTable(160) = &HB5EA&
        LookupTable(161) = &HA5CB&
        LookupTable(162) = &H95A8&
        LookupTable(163) = &H8589&
        LookupTable(164) = &HF56E&
        LookupTable(165) = &HE54F&
        LookupTable(166) = &HD52C&
        LookupTable(167) = &HC50D&
        LookupTable(168) = &H34E2&
        LookupTable(169) = &H24C3&
        LookupTable(170) = &H14A0&
        LookupTable(171) = &H481&
        LookupTable(172) = &H7466&
        LookupTable(173) = &H6447&
        LookupTable(174) = &H5424&
        LookupTable(175) = &H4405&
        LookupTable(176) = &HA7DB&
        LookupTable(177) = &HB7FA&
        LookupTable(178) = &H8799&
        LookupTable(179) = &H97B8&
        LookupTable(180) = &HE75F&
        LookupTable(181) = &HF77E&
        LookupTable(182) = &HC71D&
        LookupTable(183) = &HD73C&
        LookupTable(184) = &H26D3&
        LookupTable(185) = &H36F2&
        LookupTable(186) = &H691&
        LookupTable(187) = &H16B0&
        LookupTable(188) = &H6657&
        LookupTable(189) = &H7676&
        LookupTable(190) = &H4615&
        LookupTable(191) = &H5634&
        LookupTable(192) = &HD94C&
        LookupTable(193) = &HC96D&
        LookupTable(194) = &HF90E&
        LookupTable(195) = &HE92F&
        LookupTable(196) = &H99C8&
        LookupTable(197) = &H89E9&
        LookupTable(198) = &HB98A&
        LookupTable(199) = &HA9AB&
        LookupTable(200) = &H5844&
        LookupTable(201) = &H4865&
        LookupTable(202) = &H7806&
        LookupTable(203) = &H6827&
        LookupTable(204) = &H18C0&
        LookupTable(205) = &H8E1&
        LookupTable(206) = &H3882&
        LookupTable(207) = &H28A3&
        LookupTable(208) = &HCB7D&
        LookupTable(209) = &HDB5C&
        LookupTable(210) = &HEB3F&
        LookupTable(211) = &HFB1E&
        LookupTable(212) = &H8BF9&
        LookupTable(213) = &H9BD8&
        LookupTable(214) = &HABBB&
        LookupTable(215) = &HBB9A&
        LookupTable(216) = &H4A75&
        LookupTable(217) = &H5A54&
        LookupTable(218) = &H6A37&
        LookupTable(219) = &H7A16&
        LookupTable(220) = &HAF1&
        LookupTable(221) = &H1AD0&
        LookupTable(222) = &H2AB3&
        LookupTable(223) = &H3A92&
        LookupTable(224) = &HFD2E&
        LookupTable(225) = &HED0F&
        LookupTable(226) = &HDD6C&
        LookupTable(227) = &HCD4D&
        LookupTable(228) = &HBDAA&
        LookupTable(229) = &HAD8B&
        LookupTable(230) = &H9DE8&
        LookupTable(231) = &H8DC9&
        LookupTable(232) = &H7C26&
        LookupTable(233) = &H6C07&
        LookupTable(234) = &H5C64&
        LookupTable(235) = &H4C45&
        LookupTable(236) = &H3CA2&
        LookupTable(237) = &H2C83&
        LookupTable(238) = &H1CE0&
        LookupTable(239) = &HCC1&
        LookupTable(240) = &HEF1F&
        LookupTable(241) = &HFF3E&
        LookupTable(242) = &HCF5D&
        LookupTable(243) = &HDF7C&
        LookupTable(244) = &HAF9B&
        LookupTable(245) = &HBFBA&
        LookupTable(246) = &H8FD9&
        LookupTable(247) = &H9FF8&
        LookupTable(248) = &H6E17&
        LookupTable(249) = &H7E36&
        LookupTable(250) = &H4E55&
        LookupTable(251) = &H5E74&
        LookupTable(252) = &H2E93&
        LookupTable(253) = &H3EB2&
        LookupTable(254) = &HED1&
        LookupTable(255) = &H1EF0&

    End Sub

    Function BugByte(Wat As String) As String
        Dim i%
        Dim S As String = ""
        For i = 1 To Len(Wat)
            S += Right("00" & Hex(Asc(Mid(Wat, i, 1))), 2) & " "
        Next i
        BugByte = S
    End Function

    Function BugByte(Wat As Byte()) As String

        Dim S As String = ""
        For Each b As Byte In Wat
            S += Strings.Right("00" & Hex(b), 2) & " "
        Next
        Return S

    End Function

    Sub InitEnzi()
        eHeader = Chr(&H10) & Chr(&H1F) & Chr(&H20)
        eFooter = Chr(&H10) & Chr(&H1E) & Chr(&H12) & Chr(&H34)

        BugByte(eHeader)

        ResponseValue(0) = " Geen fout, het bestand wordt opgehaald"
        ResponseValue(1) = " protocol is onbekend"
        ResponseValue(2) = " er is al een download voor dit bestand bezig"
        ResponseValue(3) = " bestand is te groot"
        ResponseValue(4) = " kan URL niet parsen"
        ResponseValue(5) = " fout in scheme"
        ResponseValue(6) = " fout in username"
        ResponseValue(7) = " fout in password"
        ResponseValue(8) = " fout in domain"
        ResponseValue(9) = " fout in port"
        ResponseValue(10) = " root directory is niet geldig"
        ResponseValue(11) = " bestand staat in een subdirectory van configs"
        ResponseValue(12) = " mac adres toevoeging (in bestandsnaam) komt niet overeen met de unit"

        ResponseValue(20) = " Geen fout, het bestand is ge-deployed"
        ResponseValue(21) = " overdracht is afgebroken"
        ResponseValue(22) = " formaat van bestand klopt niet met de opgegeven grootte"
        ResponseValue(23) = " Deployment failed"

    End Sub

    Function Stuff(Wat As String) As String
        Stuff = Replace(Wat, Chr(&H10), Chr(&H10) & Chr(&H10))

    End Function

    ''' <summary>
    ''' This removes all "10 10" byte stuffed bytes
    ''' </summary>
    ''' <param name="Data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function DeStuff(Data As Byte()) As Byte()
        Dim DeStuffedBytes As Byte()
        ReDim DeStuffedBytes(0)
        Dim i%
        For i = 0 To Data.Count - 2
            'if this byte is 10and the previous byte was aso 10 then we dont add it

            Debug.Print((Data(i)) & "/" & (Data(i + 1)))
            If (Data(i) = 16 And Data(i + 1) = 16) Then
                'We have a stuffed byte , 
                'Debug.Print((Data(i)) & "/" & (Data(i - 1)))
                i += 1 'Lets step over it
            End If
            DeStuffedBytes(DeStuffedBytes.Count - 1) = Data(i)
            ReDim Preserve DeStuffedBytes(DeStuffedBytes.Count)
        Next
        DeStuffedBytes(DeStuffedBytes.Count - 1) = Data(i) 'the last byte (For loop only goes to count - 2)
        'ReDim Preserve DeStuffedBytes(DeStuffedBytes.Count - 1)
        Debug.Print(BugByte(Data))
        Debug.Print(BugByte(DeStuffedBytes))

        Return DeStuffedBytes

    End Function
    Function Bytes(Waarde As Long) As String
        'Dit zet een getal (32 bit) om in 4 bytes, achterstevoren LSB eerst dan MSB
        Dim S$
        S = Right("00000000" & Hex(Waarde), 8)
        Bytes = Chr("&H" & Mid(S, 7, 2)) & Chr("&H" & Mid(S, 5, 2)) & Chr("&H" & Mid(S, 3, 2)) & Chr("&H" & Mid(S, 1, 2))
        ' BugByte Bytes
    End Function

    ''' <summary>
    ''' Given a MAC addres this wil convert each byte in the given mac address to hex values.
    ''' </summary>
    ''' <param name="MAC_Address"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function MakeMAC(ByVal MAC_Address As String) As String
        'Ik verwacht hier MAC address in formaat 00:25:6e:10:11:12
        Dim MacPart() As String
        Dim i%
        Dim MacChr As String = ""
        If MAC_Address <> "" Then
            'Get rid of any - seperators in de MAC address
            If InStr(1, MAC_Address, "-") <> 0 Then
                MAC_Address = Replace(MAC_Address, "-", "")
            End If
            'Get rid of any : seperators in de MAC address
            If InStr(1, MAC_Address, ":") <> 0 Then
                MAC_Address = Replace(MAC_Address, ":", "")
            End If

            'Leaving us here with only the 6 pairs of the mac adress, so we add the : between each pair 
            Dim tmpS As String = ""
            For i = 1 To 12 Step 2
                tmpS += Strings.Mid(MAC_Address, i, 2) & ":"
            Next
            MAC_Address = Strings.Left(tmpS, 17)

            'Split up the MAC address pairs
            MacPart = Split(MAC_Address, ":")
            If UBound(MacPart) <> 5 Then
                Return ""
            End If

            'Convert each pair to Hex
            For i = 0 To 5
                MacChr += Chr("&H" & MacPart(i))
            Next i
        End If

        Return MacChr
    End Function

    Function BerichtNummer() As String

        If gBerichtNummer < 255 Then
            gBerichtNummer = gBerichtNummer + 1
        Else
            gBerichtNummer = 1
        End If
        BerichtNummer = Chr(gBerichtNummer)

    End Function

    'Function Herstart(MAC As String) As String
    '    '        DLE US EV  CMD  LEN MSGNr MAC
    '    Dim Payload$
    '    Dim iCRC As Byte()

    '    Payload = BerichtNummer & MakeMAC(MAC) & Chr(&H2)
    '    'Old VB6 way (doesn't work in .NET
    '    'iCRC = CRC16(eHeader & Chr(&HFF) & Chr(Len(Payload)) & Payload & Left(eFooter, 2))

    '    'New way - NOT TESTED!!!!
    '    Dim B As Byte() = New Byte() {eHeader, Val(Chr(&HFF)), Val(Chr(Len(Payload))), Payload, Left(eFooter, 2)}


    '    'Debug.Print(BugByte((eHeader & Chr(&HFF) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC)))

    '    'Herstart = eHeader & Chr(&HFF) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC

    'End Function

    Function FindAIPENZI(MAC As String) As String
        '        DLE US EV  CMD  LEN MSGNr MAC
        Dim Payload As String
        Dim iCRC

        Payload = BerichtNummer() & MakeMAC(MAC)

        iCRC = CRC16(eHeader & Chr(&H26) & Chr(Len(Payload)) & Payload & Left(eFooter, 2))

        Debug.Print(BugByte((eHeader & Chr(&H26) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC)))
        Return eHeader & Chr(&H26) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC

    End Function

    'Function FindAIPENZI(MAC As Byte()) As Byte()

    '    '        DLE US EV  CMD  LEN MSGNr MAC
    '    Dim Payload As Byte()
    '    Dim iCRC As Byte()
    '    Dim NewBerichtNummer As Integer

    '    Payload = BerichtNummer() & MakeMAC(MAC)

    '    iCRC = CRC16(eHeader & Chr(&H26) & Chr(Len(Payload)) & Payload & Left(eFooter, 2))

    '    Debug.Print(BugByte((eHeader & Chr(&H26) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC)))
    '    Return eHeader & Chr(&H26) & Stuff(Chr(Len(Payload)) & Payload) & Left(eFooter, 2) & iCRC

    'End Function

    Function DownLoad(Bestand As String, MAC As String, tftpServer As String)
        Dim Payload$, Lengte$, Data$

        '10 1F 20 CMD LEN BerNR MAC DATA 10 1E CRC

        Data = Chr(&H1) & tftpServer & Chr(0) & Chr(0) & Bestand & Chr(0)
        Lengte = Chr(Len(Data) + 7) '+7 is voor macadres en berichtnummer

        Payload = eHeader & Chr(&H84) & Lengte & BerichtNummer & Stuff(MakeMAC(MAC)) & Stuff(Data) & eFooter
        DownLoad = Payload


        Bug("Sending to AIP Download: (" & BugByte(Payload) & ")")

    End Function


    Function ResponceDownloadFinished() As String
        Dim Payload$, Lengte$, Data$
        Dim MAC$
        MAC = "0000-0000-0000"
        '10 1F 20 CMD LEN BerNR MAC DATA 10 1E CRC

        Data = "0" 'Deze 0 moet eigenlijk "" zijn, maar AIP daemon kan niet met "geen data" overweg.
        Lengte = Chr(Len(Data) + 7) '+7 is voor macadres en berichtnummer

        Payload = eHeader & Chr(&H87) & Lengte & BerichtNummer & Stuff(MakeMAC(MAC)) & eFooter
        ResponceDownloadFinished = Payload

        Bug("Sending to AIP: ResponceDownloadFinished. (" & BugByte(Payload) & ")")


    End Function

End Module
