﻿Public Class frmAbout

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub frmAbout_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        AboutFormLoaded = False
    End Sub

    Private Sub frmAbout_Load(sender As Object, e As EventArgs) Handles Me.Load
        AboutFormLoaded = True
        lblCopyright.Text = "© Van Breda B.V. " & Format(Now, "yyyy")
        lblVersion.Text = "Versie: " & Application.ProductVersion
    End Sub
End Class