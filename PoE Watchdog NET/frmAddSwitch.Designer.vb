﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddSwitch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.optHP = New System.Windows.Forms.RadioButton()
        Me.optCisco2 = New System.Windows.Forms.RadioButton()
        Me.optCisco = New System.Windows.Forms.RadioButton()
        Me.optAruba = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMAC = New System.Windows.Forms.TextBox()
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(215, 156)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(75, 23)
        Me.cmdOk.TabIndex = 28
        Me.cmdOk.Text = "Ok"
        Me.cmdOk.UseVisualStyleBackColor = True
        '
        'optHP
        '
        Me.optHP.AutoSize = True
        Me.optHP.Location = New System.Drawing.Point(15, 107)
        Me.optHP.Name = "optHP"
        Me.optHP.Size = New System.Drawing.Size(40, 17)
        Me.optHP.TabIndex = 27
        Me.optHP.TabStop = True
        Me.optHP.Text = "HP"
        Me.optHP.UseVisualStyleBackColor = True
        '
        'optCisco2
        '
        Me.optCisco2.AutoSize = True
        Me.optCisco2.Location = New System.Drawing.Point(15, 87)
        Me.optCisco2.Name = "optCisco2"
        Me.optCisco2.Size = New System.Drawing.Size(66, 17)
        Me.optCisco2.TabIndex = 26
        Me.optCisco2.TabStop = True
        Me.optCisco2.Text = "Cisco (2)"
        Me.optCisco2.UseVisualStyleBackColor = True
        '
        'optCisco
        '
        Me.optCisco.AutoSize = True
        Me.optCisco.Location = New System.Drawing.Point(15, 67)
        Me.optCisco.Name = "optCisco"
        Me.optCisco.Size = New System.Drawing.Size(51, 17)
        Me.optCisco.TabIndex = 25
        Me.optCisco.TabStop = True
        Me.optCisco.Text = "Cisco"
        Me.optCisco.UseVisualStyleBackColor = True
        '
        'optAruba
        '
        Me.optAruba.AutoSize = True
        Me.optAruba.Checked = True
        Me.optAruba.Location = New System.Drawing.Point(15, 127)
        Me.optAruba.Name = "optAruba"
        Me.optAruba.Size = New System.Drawing.Size(53, 17)
        Me.optAruba.TabIndex = 18
        Me.optAruba.TabStop = True
        Me.optAruba.Text = "Aruba"
        Me.optAruba.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(171, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 15)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "MAC adres (optioneel)"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 15)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Type"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 15)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "IP adres"
        '
        'txtMAC
        '
        Me.txtMAC.Location = New System.Drawing.Point(174, 26)
        Me.txtMAC.Name = "txtMAC"
        Me.txtMAC.Size = New System.Drawing.Size(116, 20)
        Me.txtMAC.TabIndex = 23
        '
        'txtIP
        '
        Me.txtIP.Location = New System.Drawing.Point(15, 26)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(116, 20)
        Me.txtIP.TabIndex = 10
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(134, 156)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 28
        Me.cmdCancel.Text = "Annuleren"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'frmAddSwitch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(302, 186)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.optHP)
        Me.Controls.Add(Me.optCisco2)
        Me.Controls.Add(Me.optCisco)
        Me.Controls.Add(Me.optAruba)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtMAC)
        Me.Controls.Add(Me.txtIP)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmAddSwitch"
        Me.Text = "Switch toevoegen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdOk As Button
    Friend WithEvents optHP As RadioButton
    Friend WithEvents optCisco2 As RadioButton
    Friend WithEvents optCisco As RadioButton
    Friend WithEvents optAruba As RadioButton
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtMAC As TextBox
    Friend WithEvents txtIP As TextBox
    Friend WithEvents cmdCancel As Button
End Class
